class Person{
    name: string;
    constructor(theName: string){
        this.name = theName;
    }
    greet(){
        return console.log('Hello: ', this.name);
    }
}

const races = require('./races.json');

let greet = new Person('Damian');
console.log(greet.greet());

class Pony{
    name: string;
    race: string;
    constructor(theName: string, theRace: string){
        this.name = theName;
        this.race = theRace;
    }
    greet(){
        return 'the pony is: ' + this.name + ' and race: ' + this.race;
    }
}

let ponster = new Pony('Twilight', races[Math.floor(Math.random() * races.length)].name);
console.log(ponster.greet());


function poner(ponyObj: {name: string, race: string}){
    console.log('No interface: ' + ponyObj.name + ' race: ' + ponyObj.race);
}
let sugar = {name: 'Sugar', race: 'Unicorn'};
poner(sugar);

interface ponyProps{
    name: string;
    race: string;
}
function poner2(ponyObj: ponyProps){
    console.log('Interface: ' + ponyObj.name + ' race: ' + ponyObj.race);
}
let sugar2 = {name: 'Sugar2', race: 'Pegasus'};
poner2(sugar2);