// import * as fs from 'fs';
const fs = require('fs');

const genders = require('./genders.json');
const races = require('./races.json');
const locations = require('./locations.json');
const firstName = require('./firstnames.json');
const lastName = require('./lastnames.json');
const professions = require('./jobs.json');

// type Char = id: number;

// import * as genders from './genders.json';

const names: string[] = [], chars: object[] = [];

function randomArr(array: any[] | string[]){
    return array[Math.floor(Math.random() * array.length)];
}

async function generateNames(flags: string){
    try{
        for(let x = 0; x < firstName.length; x++){
            for(let y = 0; y < lastName.length; y++){
                const nameString:string = firstName[x] + ' ' + lastName[y];
                names.push(nameString);
            }
        }
        fs.writeFileSync('names.txt', names.join(',\n'), (err: string | undefined) => {
            if(err){ throw new Error(err); }
        });
        if(flags == '-n' || flags == '-nc' || flags == '-a'){ console.log(names, 'Name combinations: ' + names.length); }
    }
    catch(err){ throw new Error(err); }
}

async function generateCharacters(flags:string, times?: number){
    try{
        const namesUsed: string[] = [], idsUsed: number[] = [], charStrings: string[] = [];
        await fs.writeFileSync('./chars.txt', '', (err: string | undefined) => {
            if(err){ throw new Error(err); }
        });
        times = times || 50;
        if(times < 1){
            console.log('Can\'t be less than 1 times. Setting to 1 automatically.');
            times = 1;
        }
        else if(times > 50){
            console.log('Can\'t be higher than 50. Setting automatically to 50.');
            times = 50;
        }
        for(let y = 0; y < times; y++){
            let id:number, name:string;
            do{
                id = Math.floor(Math.random() * 999999);
            }while(idsUsed.includes(id));
            idsUsed.push(id);
            do{
                name = randomArr(names);
            }while(namesUsed.includes(name));
            namesUsed.push(name);
            const age:number = Math.floor(Math.random() * 100) + 1;
            const gender:any = randomArr(genders);
            const race:any = randomArr(races);
            const location: string = randomArr(locations);
            const job: any = randomArr(professions);
            let ageString: string;
            if(age < 12 && gender.name == 'Stallion'){ ageString = 'Foal'; }
            else if(age < 12 && gender.name == 'Mare'){ ageString = 'Filly'; }
            else if(age >= 12 && gender.name == 'Stallion' && age < 18){ ageString = 'Colt'; }
            else if(age >= 12 && gender.name == 'Mare' && age < 18){ ageString = 'Young mare?'; }
            else if(age >= 18 && gender.name == 'Stallion' && age < 30){ ageString = 'Stallion'; }
            else if(age >= 18 && gender.name == 'Mare' && age < 30){ ageString = 'Mare'; }
            else if(age >= 30 && age < 60){ ageString = 'Middle aged '; }
            else if(age >= 60 && age < 100){ ageString = 'Elder'; }
            else if(age >= 100 && age < 200){ ageString = 'Super old'; }
            else if(age >= 200){ ageString = 'Immortal'; }
            else{ ageString = 'Undetermined'; }
            const char:any = {
                id: id,
                name: name,
                gender: gender.name,
                race: race.name,
                hometown: location,
                age: age + ` ${ageString}`,
                job: job.name,
            };
            chars.push(char);
            const charString = `Name: ${char.name} | Gender: ${char.gender} | Race: ${char.race}\nHometown: ${char.hometown} | Age: ${char.age} | Job: ${char.job}`;
            charStrings.push(charString);
            await fs.appendFileSync('./chars.txt', charString, (err: string | undefined) => {
                if(err){ throw new Error(err); }
            });
            if(flags == '-c' || flags == '-nc'){ console.log(char); }
            // console.log('we are here');
        }
        console.log(namesUsed, 'Used names: ' + namesUsed.length);
        console.log(idsUsed, 'Used ids: ' + idsUsed.length);
        console.log(chars, 'Chars genertaed: ' + chars.length);
        await fs.writeFileSync('./chars.json', JSON.stringify(chars), (err: string | undefined) => {
            if(err){ throw new Error(err); }
        });
        await fs.writeFileSync('./chars2.txt', charStrings.join('\n\n'), (err: string | undefined) => {
            if(err){ throw new Error(err); }
        });
    }
    catch(err){ throw new Error(err); }
}

async function run(){
    try{
        const args: string = process.argv[2], times: number = parseInt(process.argv[3]);
        if(args == '-a'){
            console.log(genders, 'Genders: ' + genders.length);
            console.log(races, 'Races: ' + races.length);
            await generateNames(args);
            await generateCharacters(args, times);
            console.log('It runs');
        }
        else if(args == '-n'){ return generateNames(args); }
        else if(args == '-c'){ return generateCharacters(args, times); }
        else if(args == '-nc'){
            await generateNames(args);
            await generateCharacters(args, times);
            return;
        }
        else{ return console.log('Please specify what you wish to do.\n-a for all, -n for names, -c for characters, -nc for names and characters'); }
    }
    catch(err){ throw new Error(err); }
}

run();