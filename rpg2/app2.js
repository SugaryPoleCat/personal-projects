"use strict";
class Person {
    constructor(theName) {
        this.name = theName;
    }
    greet() {
        return console.log('Hello: ', this.name);
    }
}
const races = require('./races.json');
let greet = new Person('Damian');
console.log(greet.greet());
class Pony {
    constructor(theName, theRace) {
        this.name = theName;
        this.race = theRace;
    }
    greet() {
        return 'the pony is: ' + this.name + ' and race: ' + this.race;
    }
}
let ponster = new Pony('Twilight', races[Math.floor(Math.random() * races.length)].name);
console.log(ponster.greet());
function poner(ponyObj) {
    console.log('No interface: ' + ponyObj.name + ' race: ' + ponyObj.race);
}
let sugar = { name: 'Sugar', race: 'Unicorn' };
poner(sugar);
function poner2(ponyObj) {
    console.log('Interface: ' + ponyObj.name + ' race: ' + ponyObj.race);
}
let sugar2 = { name: 'Sugar2', race: 'Pegasus' };
poner2(sugar2);
