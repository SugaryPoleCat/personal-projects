const letters = {
	'a': 'd',
	'b': 'e',
	'c': 'f',
	'd': 'g',
	'e': 'b',
	'f': 'p',
	'g': 'i',
	'h': 'm',
	'i': 'l',
	'j': 'w',
	'k': 'j',
	'l': 'y',
	'm': 't',
	'n': 'x',
	'o': 'a',
	'p': 'o',
	'q': 'h',
	'r': 'q',
	's': 'r',
	't': 'z',
	'u': 'n',
	'v': 's',
	'w': 'c',
	'x': 'u',
	'y': 'k',
	'z': 'v',
};

// INPUT
const hello = process.argv[2] ? String(process.argv[2]) : 'The quick brown fox jumps over the lazy dog!';

// SHOW INPUT
console.log('Input: ', hello);
function replacement6(theString) {
	let newString = '';
	for (let x = 0; x < theString.length; x++) {
		// it looks ugly but i tried a different way and it looked ew.
		if (Object.keys(letters).find(key => letters[key] === theString[x])) {
			newString += Object.keys(letters).find(key => letters[key] === theString[x]);
		} else if (Object.keys(letters).find(key => letters[key] === theString[x].toLowerCase())) {
			const newKey = Object.keys(letters).find(key => letters[key] === theString[x].toLowerCase());
			newString += newKey.toUpperCase();
		} else {
			newString += theString[x];
		}
	}
	return newString;
}

try {
	console.log('Output: ', replacement6(hello));
}
catch (err) {
	console.error(err);
}
