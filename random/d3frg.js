const tiers = 4;
const craft = 3;
// the start.
let needed = craft;

// Costs of gems. Its basically, marquise * 2, *3, *4, *5
// Marquise costs 100000 so. 
const imperialCostBase = 200000;
const flawImperialCostBase = 300000;
const royalCostBase = 400000;
const flawRoyalCostBase = 500000;

// the base Death's Breath costs for royal gems.
// Separated into 2, in case it changes.
const royalDbBase = 1;
const flawRoyalDbBase = 1;

// how many would you like to make.
const neededToMake = 1;

// this loop handles how many gems you need to craft, per tier.
// tier is = 0 marquise, 1 imperial, 2 flawless imperial, 3 royal gems and 4 is flawless royal gems. 
// we take tiers - 1, because otherwise it will display wrong information, because in the beginning i was thinking wrong. We can eliminate it by shifting tiers: 0 imperial, 1 flawless imperial, 2 royal gems 3 flawless royal gems.
// because in the begining i was thinking: oh tiers from the start. But now im thinking: how many tiers are we upgrading.
// We are intentionally disregarding gems that drop below level 70, as they are so cheap and dont drop past level 70, that its useless to count them. 
for (let x = 0; x < (tiers - 1); x++) {
	// for the amount of teirs we are going up, we multiply 3 * 3, as you always need 3 gems to craft a higher tier.
	needed = needed * craft;
}

// how many you want to make
needed = needed * neededToMake;

// these are just costs and how many gems you need from given tier.
// the cost is just multiplied by the amount of gems needed.
// while the needed gems are previousTier / 3, as you get 3 times less gems in new tier, from previous tier. 
const imperialCost = imperialCostBase * needed;
const needed2 = needed / 3;
const flawImperialCost = flawImperialCostBase * needed2;
const needed3 = needed2 / 3;
const royalCost = royalCostBase * needed3;
const royalDb = royalDbBase * needed3;
const needed4 = needed3 / 3;
const flawRoyalCost = flawRoyalCostBase * needed4;
const flawRoyalDb = flawRoyalDbBase * needed4;

// total costs.
const totalCost = flawRoyalCost + flawRoyalCost + imperialCost + royalCost;
// how to get cost per 1 gem? just divide the total by how many you needed.
const costFor1 = totalCost / neededToMake;
const totalDb = royalDb + flawRoyalDb;
const dbFor1 = totalDb / neededToMake;

console.log(`To create ${neededToMake} Flawless Royal Gems, you need:
${needed} Marquise Gems and ${imperialCost} Gold, to make into ${needed2} Imperial Gems
${needed2} Imperial Gems and ${flawImperialCost} Gold, to make into ${needed3} flawless Imperial Gems
${needed3} Flawless Imperial Gems and ${royalCost} Gold and ${royalDb} Death's Breath/s, to make into ${needed4} Royal Gems
${needed4} Royal Gems and ${flawRoyalCost} Gold and ${flawRoyalDb} Death's Breath/s, to make into ${neededToMake} Flawless Royal Gems
\n
Total cost: ${totalCost} Gold and ${totalDb} Death's Breath/s.
Total cost per one: ${costFor1} Gold and ${dbFor1} Death's Breath/s`);

