const letters = {
    a: 'o',
    b: 'e',
    c: 'w',
    d: 'a',
    e: 'b',
    f: 'p',
    g: 'd',
    h: 'r',
    i: 'g',
    j: 'k',
    k: 'y',
    l: 'i',
    m: 'h',
    n: 'u',
    o: 'p',
    p: 'f',
    q: 'r',
    r: 's',
    s: 'v',
    t: 'm',
    u: 'x',
    v: 'z',
    w: 'j',
    x: 'n',
    y: 'l',
    z: 't',
};

// here: left column is the RIGFHT column above
const letters2 = {
    'a': 'd',
    'b': 'e',
    'c': 'f',
    'd': 'g',
    'e': 'b',
    'f': 'p',
    'g': 'i',
    'h': 'm',
    'i': 'l',
    'j': 'w',
    'k': 'j',
    'l': 'y',
    'm': 't',
    'n': 'x',
    'o': 'a',
    'p': 'o',
    'r': ['h', 'q'],
    's': 'r',
    't': 'z',
    'u': 'n',
    'v': 's',
    'w': 'c',
    'x': 'u',
    'y': 'k',
    'z': 'v',
};

// function capitilize(word){

// }

// String.prototype.replaceAt = function (index, replacement) {
//     return this.substr(0, index) + replacement + this.substr(index + replacement.length);
// };

// INPUT
const hello = process.argv[2] ? String(process.argv[2]) : 'Hello World!';

// SHOW INPUT
console.log('Input: ', hello);
function replacement(theString) {
    let newString = '';
    for (let x = 0; x < hello.length; x++) {
        // console.log(hello[x]);
        switch (hello[x]) {
            case 'a':
                newString += letters.a;
                break;
            case 'b':
                newString += letters.b;
                break;
            case 'c':
                newString += letters.c;
                break;
            case 'd':
                newString += letters.d;
                break;
            case 'e':
                newString += letters.e;
                break;
            case 'f':
                newString += letters.f;
                break;
            case 'g':
                newString += letters.g;
                break;
            case 'h':
                newString += letters.h;
                break;
            case 'i':
                newString += letters.i;
                break;
            case 'j':
                newString += letters.j;
                break;
            case 'k':
                newString += letters.k;
                break;
            case 'l':
                newString += letters.l;
                break;
            case 'm':
                newString += letters.m;
                break;
            case 'n':
                newString += letters.n;
                break;
            case 'o':
                newString += letters.o;
                break;
            case 'p':
                newString += letters.p;
                break;
            case 'q':
                newString += letters.q;
                break;
            case 'r':
                newString += letters.r;
                break;
            case 's':
                newString += letters.s;
                break;
            case 't':
                newString += letters.t;
                break;
            case 'u':
                newString += letters.u;
                break;
            case 'v':
                newString += letters.v;
                break;
            case 'w':
                newString += letters.w;
                break;
            case 'x':
                newString += letters.x;
                break;
            case 'y':
                newString += letters.y;
                break;
            case 'z':
                newString += letters.z;
                break;
            case 'A':
                newString += letters.a.toUpperCase();
                break;
            case 'B':
                newString += letters.b.toUpperCase();
                break;
            case 'C':
                newString += letters.c.toUpperCase();
                break;
            case 'D':
                newString += letters.d.toUpperCase();
                break;
            case 'E':
                newString += letters.e.toUpperCase();
                break;
            case 'F':
                newString += letters.f.toUpperCase();
                break;
            case 'G':
                newString += letters.g.toUpperCase();
                break;
            case 'H':
                newString += letters.h.toUpperCase();
                break;
            case 'I':
                newString += letters.i.toUpperCase();
                break;
            case 'J':
                newString += letters.j.toUpperCase();
                break;
            case 'K':
                newString += letters.k.toUpperCase();
                break;
            case 'L':
                newString += letters.l.toUpperCase();
                break;
            case 'M':
                newString += letters.m.toUpperCase();
                break;
            case 'N':
                newString += letters.n.toUpperCase();
                break;
            case 'O':
                newString += letters.o.toUpperCase();
                break;
            case 'P':
                newString += letters.p.toUpperCase();
                break;
            case 'Q':
                newString += letters.q.toUpperCase();
                break;
            case 'R':
                newString += letters.r.toUpperCase();
                break;
            case 'S':
                newString += letters.s.toUpperCase();
                break;
            case 'T':
                newString += letters.t.toUpperCase();
                break;
            case 'U':
                newString += letters.u.toUpperCase();
                break;
            case 'V':
                newString += letters.v.toUpperCase();
                break;
            case 'W':
                newString += letters.w.toUpperCase();
                break;
            case 'X':
                newString += letters.x.toUpperCase();
                break;
            case 'Y':
                newString += letters.y.toUpperCase();
                break;
            case 'Z':
                newString += letters.z.toUpperCase();
                break;
            default:
                newString += hello[x];
                break;
        }
    }
    return newString;
}

// // OUTPUT


function replacement2(theString) {
    let newString = '';
    for (let x = 0; x < theString.length; x++) {
        // this gets the accurate letter in array and adds that at the end
        // console.log(theString.substr(0, (theString.indexOf(theString[x]) + 1)) + 'r');
        // this adds the letter at the end of the array, but also prints the rest of the string
        // console.log(theString.substr(0, (theString.indexOf(theString[x]) + 1)) + 'r' + theString.substr(theString.indexOf(theString[x]) + 1));

        // none of them replace the letter, because we are doing theString.indexOf(+1);
        // the function below will replace the letter at the right place.
        // console.log(theString.substr(0, theString.indexOf(theString[x])) + 'r' + theString.substr(theString.indexOf(theString[x]) + 1));
        // console.log(Object.values(letters).find(value => letters[value] === theString[x]));

        // NOW we combine BOTH to replace the letters:

        // this prints KLEYS for some reason.
        newString += Object.values(letters).find(value => letters[value] === theString[x]) ? Object.values(letters).find(value => letters[value] === theString[x]) : theString[x];
        console.log('thestring[x]: ', theString[x]);
        // this is just to check what the fuck is going on under the hoods
        // console.log(Object.keys(letters).find(key => {
        //     console.log('letters[key]: ', letters[key]);
        //     if (letters[key] === theString[x]) {
        //         console.log('letters[key] matches thestring[x]: ', letters[key]);
        //     }
        //     return letters[key] === theString[x];
        // }));
    }
    return newString;
}



// for (let x = 0; x < hello.length; x++) {
//     console.log(hello.indexOf(hello[x]));
// }

// console.log(Object.keys(letters));
// console.log(Object.values(letters));


function replacement3(theString) {
    let newString = '';
    for (let x = 0; x < theString.length; x++) {
        for (const prop in letters) {
            if (letters.hasOwnProperty(prop)) {
                if (letters[prop] === theString[x]) {
                    newString += prop;
                } else {
                    newString += theString[x];
                }
            }
        }
    }
    return newString;
}



// const map = { "first": "1", "second": "2" };
// console.log(getKeyByValue(letters, "c"));

// okay so it does whats its supposed ot. BUT to get the translation THIS way,
// i will need to create objects in a different way.

// function getKeyByValue(object, value) {
//     return Object.keys(object).find(key => object[key] === value);
// }

// console.log(Object.keys(letters).find(key => letters[key] === 'f'));

// and i think its this way:
function replacement4(theString) {
    let newString = '';
    for (let x = 0; x < theString.length; x++) {
        newString += Object.keys(letters2).find(key => letters2[key] === theString[x]);
    }
    return newString;
}

// it worked but now i need to figure out how to handle big and small letters and spaces
function replacement5(theString) {
    let newString = '';
    for (let x = 0; x < theString.length; x++) {
        newString += Object.keys(letters2).find(key => letters2[key] === theString[x]) ? Object.keys(letters2).find(key => letters2[key] === theString[x]) : theString[x];
    }
    return newString;
}


// it worked but now i need to figure out how to handle big and small letters
function replacement6(theString) {
    let newString = '';
    for (let x = 0; x < theString.length; x++) {
        if (Object.keys(letters2).find(key => letters2[key] === theString[x])) {
            newString += Object.keys(letters2).find(key => letters2[key] === theString[x]);
        } else if (Object.keys(letters2).find(key => letters2[key] === theString[x].toLowerCase())) {
            const newKey = Object.keys(letters2).find(key => letters2[key] === theString[x].toLowerCase());
            newString += newKey.toUpperCase();
        } else {
            newString += theString[x];
        }
    }
    return newString;
}

try {
    // console.log('Output: ', replacement(hello));
    // console.log('Output 2: ', replacement2(hello), ' expected: ', replacement(hello));
    // console.log('Output 3: ', replacement3(hello), ' | expected: ', replacement(hello));
    // console.log('Output 4: ', replacement4(hello), ' | expected: ', replacement(hello));
    // console.log('Output 5: ', replacement5(hello), ' | expected: ', replacement(hello));
    console.log('Output 6: ', replacement6(hello), ' | expected: ', replacement(hello));
}
catch (err) {
    console.error(err);
}
