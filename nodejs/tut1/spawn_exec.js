const cp = require('child_process');

// A set of options for the EXEC
const exec_options = {
    // Current Working Directory
    cwd: null,
    env: null,
    encoding: 'utf8',
    timeout: 0,
    // Prevents loading too much data at one time, in turn hanging the program.
    maxBuffer: 200 * 1024,
    killSignal: 'SIGTERM',
};

// Exec
// Async
// This wil be run last, as SYNC stuff is always ran first.
cp.exec('ls -l', exec_options, (err, stdout, stderr) => {
    console.log('#1. exec');
    console.log(stdout);
});

// Exec sync
// Synchronus
try{
    const data = cp.execSync('ls -l', exec_options);
    console.log('#2. exec sync');
    console.log(data.toString());
}
catch(err){
    console.error(err);
}

const spawn_options = {
    cwd: null,
    env: null,
    detached: false,
};

const ls = cp.spawn('ls', ['-l'], spawn_options);

// Node is very event based.
// So when something happens ON data event, something happens.
// This is STANDARD OUTPUT
ls.stdout.on('data', stdout => {
    console.log('#3. spawn');
    console.log(stdout.toString());
});

// This is STANDARD ERROR
ls.stderr.on('data', stderr => {
    console.error(stderr.toString());
});

// Stuff will happen on FINISHING.
ls.on('close', code => {
    // End with code
});

// teh STDOUT and STDERR are just actual strings.
const { stdout, stderr, pid, status } = cp.spawnSync('ls', ['-l'], spawn_options);
console.log('#4. spawn sync');
console.log(stdout.toString());