const fs = require('fs');

// FOr everything:
// average: 10. Max = 20. min = 1. If you put 10 somewhere, put 10 in the opposite. As in: 10 attack, 10 defense. 15 attakc: 5 defese and so on.

const races = [
    {
        id: 1,
        name: 'Human',
        health: 10,
        mana: 10,
        attack: 10,
        defense: 10,
        mAttack: 5,
        mDefense: 5,
    },
    {
        id: 2,
        name: 'Orc',
        health: 20,
        mana: 1,
        attack: 15,
        defense: 5,
        mAttack: 1,
        mDefense: 1,
    },
    {
        id: 3,
        name: 'Elf',
        health: 5,
        mana: 15,
        attack: 5,
        defense: 5,
        mAttack: 8,
        mDefense: 7,
    },
];

const classes = [
    {
        id: 1,
        name: 'Warrior',
        health: 15,
        mana: 5,
        attack: 15,
        defense: 5,
        mAttack: 3,
        mDefense: 2,
    },
    {
        id: 2,
        name: 'Mage',
        health: 1,
        mana: 20,
        attack: 1,
        defense: 1,
        mAttack: 15,
        mDefense: 5,
    },
    {
        id: 3,
        name: 'Wizard',
        health: 5,
        mana: 15,
        attack: 10,
        defense: 10,
        mAttack: 6,
        mDefense: 7,
    },
    {
        id: 4,
        name: 'Barbarian',
        health: 20,
        mana: 1,
        attack: 15,
        defense: 5,
        mAttack: 1,
        mDefense: 1,
    },
    {
        id: 5,
        name: 'Paladin',
        health: 10,
        mana: 10,
        attack: 10,
        defense: 10,
        mAttack: 10,
        mDefense: 10,
    },
];

function randomArray(array){
    return array[Math.floor(Math.random() * array.length)];
}

async function run(){
    await fs.writeFileSync('fight2.txt', '', function(err){
        if(err){ return console.error(err); }
    });
    await fs.writeFileSync('chars2.json', '', function(err){
        if(err){ return console.error(err); }
    });

    const names = [];
    const characters = [];
    let char;

    // generate names
    let generatedName;
    const firstNames = [],
    middleNames = [],
    lastNames = [];

    const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    const consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];
    for(let x = 0; x < 10; x++){
        let firstName = randomArray(letters);
        let middleName = randomArray(letters);
        let lastName = randomArray(letters);
        if(vowels.includes(firstName)){ firstName = firstName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(firstName)){ firstName = firstName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(middleName)){ middleName = middleName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(middleName)){ middleName = middleName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(lastName)){ lastName = lastName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(lastName)){ lastName = lastName + randomArray(vowels) + randomArray(letters); }

        firstName = firstName.charAt(0).toUpperCase() + firstName.slice(1);
        middleName = middleName.charAt(0).toUpperCase() + middleName.slice(1);
        lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

        firstNames.push(firstName);
        while(firstNames.includes(firstName)){
            firstName = randomArray(letters);
            if(vowels.includes(firstName)){ firstName = firstName + randomArray(consonants) + randomArray(letters); }
            else if(consonants.includes(firstName)){ firstName = firstName + randomArray(vowels) + randomArray(letters); }
        }
        middleNames.push(middleName);
        while(middleNames.includes(middleName)){
            middleName = randomArray(letters);
            if(vowels.includes(middleName)){ middleName = middleName + randomArray(consonants) + randomArray(letters); }
            else if(consonants.includes(middleName)){ middleName = middleName + randomArray(vowels) + randomArray(letters); }
        }
        lastNames.push(lastName);
        while(lastNames.includes(lastName)){
            lastName = randomArray(letters);
            if(vowels.includes(lastName)){ lastName = lastName + randomArray(consonants) + randomArray(letters); }
            else if(consonants.includes(lastName)){ lastName = lastName + randomArray(vowels) + randomArray(letters); }
        }
        console.log('Generated first name: ', firstName, ' | middle name: ', middleName, ' | last name: ', lastName);
    }

    // Count possible name combinations
    let possibleNamesCombo = 0;
    for(let x = 0; x < firstNames.length; x++){
        for(let y = 0; y < middleNames.length; y++){
            for(let z = 0; z < lastNames.length; z++){
                possibleNamesCombo++;
                generatedName = firstNames[x] + '-' + middleNames[y] + ' ' + lastNames[z];
                names.push(generatedName);
            }
        }
    }
    await fs.writeFileSync('names2.txt', names.join(', '), function(err){
        if(err){ return console.error(err); }
    });
    console.log('done generating:\n', names, '\nPossible name combinations: ', possibleNamesCombo);

    // Make characters
    for(let x = 0; x < races.length; x++){
        for(let y = 0; y < classes.length; y++){
            const usedNames = [];
            let usedName = randomArray(names);
            usedNames.push(usedName);
            while(usedNames.includes(usedName)){ usedName = randomArray(names); }
            char = {
                id: (x + y * 3),
                name: usedName,
                race: races[x].name,
                class: classes[y].name,
                health: (races[x].health + classes[y].health) * 10,
                mana: (races[x].mana + classes[y].mana),
                attack: races[x].attack + classes[y].attack,
                defense: races[x].defense + classes[y].defense,
                mAttack: races[x].mAttack + classes[y].mAttack,
                mDefense: races[x].mDefense + classes[y].mDefense,
            };
            characters.push(char);
            await fs.appendFileSync('chars2.json', JSON.stringify(char), (err) => {
                if(err){ return console.error(err); }
            });
            await fs.appendFileSync('chars2.json', ',\n', (err) => {
                if(err){ return console.error(err); }
            });
        }
    }

    let createdCharacters = 0;
    for(const i of characters){ createdCharacters++; }
    // fs.writeFile('chars2.json', JSON.stringify(characters), function(err){
    //     if(err){ return console.error(err); }
    // });
    console.log('Created characters: ' + createdCharacters + '\n\n');

    let possibleFights = 0;
    for(let x = 0; x < characters.length; x++){
        for(let y = 0; y < characters.length; y++){
            if(characters[x].id == characters[y].id){ break; }
            else{
                possibleFights++;
                let result;
                // if your magical attack is equal or lower than physical
                if(characters[x].mAttack <= characters[x].attack){
                    result = 'Physical fight!\n';
                    result += (`${characters[x].name}, with ${characters[x].attack} attack is attacking ${characters[y].name}, with ${characters[y].defense} defense and ${characters[y].health} health!\n`);

                    // check if enemy is already dead.
                    if(characters[y].health <= 0){ result += `\n\nI am sorry to say, but ${characters[y].name} is ALREADY dead and can not fight.\n\n`; }
                    else{
                        // if your attack is larger than tehri defense
                        const damageDone = Math.floor((characters[x].attack * (Math.floor(Math.random() * 7) + 1)) / 1.5);
                        if(characters[x].attack > characters[y].defense){
                            characters[y].health = characters[y].health - (damageDone - characters[y].defense);
                            result += `The characters[x] ${characters[x].name} WINS against ${characters[y].name}!\ncharacters[y] ${characters[y].name} remains with ${characters[y].health} health!\n`;
                            if(characters[y].health <= 0){ result += `\n\n${characters[y].name} DIES after his confrontation with the characters[x] ${characters[x].name}!\n\n`; }
                        }
                        else if(characters[x].attack == characters[y].defense){ result += `It's a DRAW between ${characters[x].name} and ${characters[y].name}!\n`; }
                        else if(characters[x].attack < characters[y].defense){
                            result += `The characters[x] ${characters[x].name} LOSES the fight against ${characters[y].name} and now has to pay the price by losing 10% of his health!\n`;
                        }
                    }
                }
                // if magical attack is beigger then use magic.
                else if(characters[x].mAttack > characters[x].attack && characters[x].mana > 0){
                    result = 'Magical fight!\n';
                    result += (`${characters[x].name}, with ${characters[x].mAttack} attack and ${characters[x].mana} mana, is attacking ${characters[y].name}, with ${characters[y].mDefense} defense and ${characters[y].health} health!\n`);

                    // if the defender has 0 health
                    if(characters[y].health <= 0){ result += `\n\nI am sorry to say, but ${characters[y].name} is ALREADY dead and can not fight.\n\n`; }
                    else{
                        // calculate mana usage
                        characters[x].mana = characters[x].mana - Math.floor(Math.random() * 7) + 1;

                        // if characters attack, not DAMAGE done is bigger, then do the fight
                        if(characters[x].mAttack > characters[y].mDefense){
                            const damageDone = Math.floor((characters[x].mAttack * (Math.floor(Math.random() * 7) + 1)) / 1.5);
                            // take health away
                            characters[y].health = characters[y].health - (damageDone - characters[y].mDefense);
                            result += `The characters[x] ${characters[x].name} WINS against ${characters[y].name}!\ncharacters[y] ${characters[y].name} remains with ${characters[y].health} health!\nThe characters[x] loses some mana and is now left with: ${characters[x].mana}\n`;
                            // also check if they die during the fight
                            if(characters[y].health <= 0){ result += `\n\n${characters[y].name} DIES after his confrontation with the characters[x] ${characters[x].name}!\n\n`; }
                        }
                        // if your attack is EQUAL than their defense
                        else if(characters[x].mAttack == characters[y].mDefense){ result += `It's a DRAW between ${characters[x].name} and ${characters[y].name}!\nThe characters[x] loses some mana and is now left with: ${characters[x].mana}\n`; }
                        // if your attack is lower than their defense
                        else if(characters[x].mAttack < characters[y].mDefense){
                            result += `The characters[x] ${characters[x].name} LOSES the fight against ${characters[y].name} and now has to pay the price by losing 10% of his health!\nThe characters[x] loses some mana and is now left with: ${characters[x].mana}\n`;
                        }
                    }
                }
                // print to file
                fs.appendFileSync('fight2.txt', result + '\n', function(err){
                    if(err){ console.error(err); }
                });
            }
        }
    }
    console.log('Possible fights: ' + possibleFights);

    let deadCounter = 0;
    let aliveCounter = 0;
    for(const i of characters){
        if(i.health <= 0){
            console.log('Dead: ' + i.name);
            deadCounter++;
        }
        else if(i.health > 0){
            console.log('Alive: ' + i.name);
            aliveCounter++;
        }
    }
    console.log('Alive: ' + aliveCounter + ' | Dead: ' + deadCounter);
    // await fs.writeFileSync('deaths2.txt', )
}

run();