const fs = require('fs');

// FOr everything:
// average: 10. Max = 20. min = 1. If you put 10 somewhere, put 10 in the opposite. As in: 10 attack, 10 defense. 15 attakc: 5 defese and so on.

// add genders
// add attributes: strength intelligence etc.
// calculate defense / attack /mattack / mdefesne based on strength, agility, endurance, intelligence, willpower.

// base health differnet for each class. Generally make it 100 first.
// health added per point in vitality = 10 basic. physical classes add more. mental add less.

// willpower = mana defesne
// wisdom = mana attack
// intelligence = mana/mana attack/mana defense
// vitality = health/defense
// strength = attack/defensegit
// dexterity = attack/dodge

// so far i just did 1 attribute to one thing.

const genders = [
    {
        // male focus on physical
        id: 1,
        name: 'Male',
        health: 110,
        mana: 50,
        vitality: 11,
        strength: 11,
        dexterity: 9,
        intelligence: 9,
        wisdom: 11,
        willpower: 9,
    },
    {
        id: 2,
        name: 'Female',
        health: 90,
        mana: 70,
        vitality: 9,
        strength: 9,
        dexterity: 11,
        intelligence: 11,
        wisdom: 9,
        willpower: 11,
    }
]

const races = [
    {
        id: 1,
        name: 'Human',
        vitality: 10,
        strength: 10,
        dexterity: 10,
        intelligence: 10,
        wisdom: 10,
        willpower: 10,
    },
    {
        id: 2,
        name: 'Orc',
        vitality: 15,
        strength: 20,
        dexterity: 7,
        intelligence: 5,
        wisdom: 5,
        willpower: 9,
    },
    {
        id: 3,
        name: 'Elf',
        vitality: 7,
        strength: 5,
        dexterity: 15,
        intelligence: 20,
        wisdom: 15,
        willpower: 10,
    },
];

const classes = [
    {
        id: 1,
        name: 'Warrior',
        vitality: 15,
        strength: 10,
        dexterity: 10,
        intelligence: 10,
        wisdom: 10,
        willpower: 10,
    },
    {
        id: 2,
        name: 'Mage',
        vitality: 5,
        strength: 9,
        dexterity: 9,
        intelligence: 20,
        wisdom: 15,
        willpower: 15,
    },
    {
        id: 3,
        name: 'Wizard',
        vitality: 7,
        strength: 5,
        dexterity: 9,
        intelligence: 15,
        wisdom: 20,
        willpower: 10,
    },
    {
        id: 4,
        name: 'Barbarian',
        vitality: 15,
        strength: 15,
        dexterity: 5,
        intelligence: 5,
        wisdom: 5,
        willpower: 10,
    },
    {
        id: 5,
        name: 'Paladin',
        vitality: 15,
        strength: 10,
        dexterity: 10,
        intelligence: 10,
        wisdom: 10,
        willpower: 15,
    },
];

const orcNames = [];
const elfNames = [];
const humanNames = [];
const characters = [];
let possibleFights = 0;

/**
 * Instead of typing array[Math.floor] etc, use this
 *
 * @param {*} array - THe array you wish to randomise
 * @returns The fucking uhhh random entry in the array
 */
function randomArray(array){
    return array[Math.floor(Math.random() * array.length)];
}

/**
 * Generate names for humans, elves and orcs.
 *
 */
function generateNames(){
    const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    const consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'];

    const humanFirstNames = ['Jhonny', 'John', 'Charlie', 'Francis', 'Frank', 'Stephanie', 'Tommy', 'Thomas', 'Steve', 'Robert'];
    const humanLastNames = ['Johnson', 'Stevenson', 'Charlseton', 'Chucklefish', 'Terradom', 'Tosterone', 'Toasterson', 'Svensson', 'Niggerson'];
    let humanPossibleNames = 0;
    for(let x = 0; x < humanFirstNames.length; x++){
        for(let y = 0; y < humanLastNames.length; y++){
            humanPossibleNames++;
            const name = humanFirstNames[x] + ' ' + humanLastNames[y];
            humanNames.push(name);
        }
    }
    console.log('Done generating human names:\n', humanNames, '\nPossible name combinations: ', humanPossibleNames);

    let orcGeneratedName;
    const orcFirstNames = [], orcMiddleNames = [], orcLastNames = [];
    for(let x = 0; x < 10; x++){
        let orcFirstName = randomArray(letters);
        let orcMiddleName = randomArray(letters);
        let orcLastName = randomArray(letters);
        if(vowels.includes(orcFirstName)){ orcFirstName = orcFirstName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(orcFirstName)){ orcFirstName = orcFirstName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(orcMiddleName)){ orcMiddleName = orcMiddleName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(orcMiddleName)){ orcMiddleName = orcMiddleName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(orcLastName)){ orcLastName = orcLastName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(orcLastName)){ orcLastName = orcLastName + randomArray(vowels) + randomArray(letters); }
        orcFirstName = orcFirstName.charAt(0).toUpperCase() + orcFirstName.slice(1);
        orcMiddleName = orcMiddleName.charAt(0).toUpperCase() + orcMiddleName.slice(1);
        orcLastName = orcLastName.charAt(0).toUpperCase() + orcLastName.slice(1);
        orcFirstNames.push(orcFirstName);
        orcMiddleNames.push(orcMiddleName);
        orcLastNames.push(orcLastName);
        console.log('Generated orc = first name: ', orcFirstName, ' | middle name: ', orcMiddleName, ' | last name: ', orcLastName);
    }
    let orcPossibleNamesCombo = 0;
    for(let x = 0; x < orcFirstNames.length; x++){
        for(let y = 0; y < orcMiddleNames.length; y++){
            for(let z = 0; z < orcLastNames.length; z++){
                orcPossibleNamesCombo++;
                orcGeneratedName = orcFirstNames[x] + '-' + orcMiddleNames[y] + ' ' + orcLastNames[z];
                orcNames.push(orcGeneratedName);
            }
        }
    }
    console.log('Done generating orc names:\n', orcNames, '\nPossible name combinations: ', orcPossibleNamesCombo);

    let elfGeneratedName;
    const elfFirstNames = [], elfMiddleNames = [], elfLastNames = [];
    for(let x = 0; x < 10; x++){
        let elfFirstName = randomArray(letters);
        let elfMiddleName = randomArray(letters);
        let elfLastName = randomArray(letters);

        if(vowels.includes(elfFirstName)){ elfFirstName = elfFirstName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(elfFirstName)){ elfFirstName = elfFirstName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(elfMiddleName)){ elfMiddleName = elfMiddleName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(elfMiddleName)){ elfMiddleName = elfMiddleName + randomArray(vowels) + randomArray(letters); }
        if(vowels.includes(elfLastName)){ elfLastName = elfLastName + randomArray(consonants) + randomArray(letters); }
        else if(consonants.includes(elfLastName)){ elfLastName = elfLastName + randomArray(vowels) + randomArray(letters); }
        elfFirstName = elfFirstName.charAt(0).toUpperCase() + elfFirstName.slice(1);
        elfMiddleName = elfMiddleName.charAt(0).toUpperCase() + elfMiddleName.slice(1);
        elfLastName = elfLastName.charAt(0).toUpperCase() + elfLastName.slice(1);
        elfFirstNames.push(elfFirstName);
        elfMiddleNames.push(elfMiddleName);
        elfLastNames.push(elfLastName);
        console.log('Generated elf = first name: ', elfFirstName, ' | middle name: ', elfMiddleName, ' | last name: ', elfLastName);
    }

    let elfPossibleNamesCombo = 0;
    for(let x = 0; x < elfFirstNames.length; x++){
        for(let y = 0; y < elfMiddleNames.length; y++){
            for(let z = 0; z < elfLastNames.length; z++){
                elfPossibleNamesCombo++;
                elfGeneratedName = elfFirstNames[x] + '\'' + elfMiddleNames[y] + '-' + elfLastNames[z];
                elfNames.push(elfGeneratedName);
            }
        }
    }
    console.log('done generating elf names:\n', elfNames, '\nPossible name combinations: ', elfPossibleNamesCombo);
}

/**
 * create names using the names generated from generateNames and classes and races.
 *
 */
async function makeCharacters(){
    let char;
    let usedNames = [];
    let idsUsed = [];
    for(let z = 0; z < genders.length; z++){
        for(let x = 0; x < races.length; x++){
            for(let y = 0; y < classes.length; y++){
                let id;
                do{ id = Math.floor(Math.random() * 99999); }
                while(idsUsed.includes(id))
                idsUsed.push(id);
                char = {
                    id: id,
                    race: races[x].name,
                    gender: genders[z].name,
                    class: classes[y].name,
                    health: genders[z].health + (races[x].vitality * classes[y].vitality * genders[z].vitality) * 2,
                    mana: genders[z].mana + (races[x].intelligence * classes[y].intelligence * genders[z].intelligence),
                    attack: genders[z].strength + races[x].strength + classes[y].strength + genders[z].dexterity + races[x].dexterity + classes[z].dexterity,
                    defense: genders[z].vitality + classes[y].vitality + races[x].vitality + genders[z].strength + races[x].strength + classes[y].strength,
                    mAttack: genders[z].wisdom + classes[y].wisdom + races[x].wisdom + genders[z].intelligence + races[x].intelligence + classes[z].intelligence,
                    mDefense: genders[z].willpower + races[x].willpower + classes[y].willpower + genders[z].intelligence + races[x].intelligence + classes[z].intelligence,
                    vitality: genders[z].vitality + races[x].vitality + classes[z].vitality,
                    strength: genders[z].strength + races[x].strength + classes[z].strength,
                    dexterity: genders[z].dexterity + races[x].dexterity + classes[z].dexterity,
                    intelligence: genders[z].intelligence + races[x].intelligence + classes[z].intelligence,
                    wisdom: genders[z].wisdom + races[x].wisdom + classes[z].wisdom,
                    willpower: genders[z].willpower + races[x].willpower + classes[z].willpower,
                };
                let charName;
                if(races[x].name == 'Orc'){
                    let name = { name: randomArray(orcNames), }
                    while(usedNames.includes(name)){ name = { name: randomArray(orcNames), } }
                    usedNames.push(name);
                    charName = { ...char, ...name };
                }
                else if(races[x].name == 'Human'){
                    let name = { name: randomArray(humanNames), }
                    while(usedNames.includes(name)){ name = { name: randomArray(humanNames), } }
                    usedNames.push(name);
                    charName = { ...char, ...name };
                }
                else if(races[x].name == 'Elf'){
                    let name = { name: randomArray(elfNames), }
                    while(usedNames.includes(name)){ name = { name: randomArray(elfNames), } }
                    usedNames.push(name);
                    charName = { ...char, ...name };
                }
                characters.push(charName);
                await fs.appendFileSync('chars.json', JSON.stringify(charName), (err) => {
                    if(err){ return console.error(err); }
                });
                await fs.appendFileSync('chars.json', ',\n', (err) => {
                    if(err){ return console.error(err); }
                });
            }
        }
    }
}

/**
 * Make the generated characters fight for fun lol
 *
 * @param {object} attacker - obviously the attacking character object
 * @param {object} defender - obviously the defending character object
 */
function fight(attacker, defender){
    try{
        if(attacker.id == defender.id){ return; }
        else{
            possibleFights++;
            let result;
            // Make a check somwhere, that if attacker has more magical attack than physical, magical is chosne.
            if(attacker.mAttack <= attacker.attack){
                result = 'Physical fight!\n';
                result += (`${attacker.name} a ${attacker.race} ${attacker.gender} ${attacker.class}, with ${attacker.attack} attack is attacking ${defender.name} a ${defender.race} ${defender.gender} ${defender.class}, with ${defender.defense} defense and ${defender.health} health!\n`);
                if(defender.health <= 0){ result += `\n\nI am sorry to say, but ${defender.name} is ALREADY dead and can not fight.\n\n`; }
                else{
                    // make damage ranodom.
                    // for now make check based on the random damage thing. later think about some other solution, to check the attack skill against defense and then figure out forumla for damage.
                    const damageDone = Math.floor((attacker.attack * (Math.floor(Math.random() * 7) + 1)) / 2.5);
                    if(attacker.attack > defender.defense){
                        // make loser lose health from attack.
                        defender.health = defender.health - (damageDone - defender.defense);
                        result += `The attacker ${attacker.name} WINS against ${defender.name}!\nDefender ${defender.name} remains with ${defender.health} health!\n`;
                        if(defender.health <= 0){ result += `\n\n${defender.name} DIES after his confrontation with the attacker ${attacker.name}!\n\n`; }
                    }
                    else if(attacker.attack == defender.defense){ result += `It's a DRAW between ${attacker.name} and ${defender.name}!\n`; }
                    else if(attacker.attack < defender.defense){
                        result += `The attacker ${attacker.name} LOSES the fight against ${defender.name} and now has to pay the price by losing 10% of his health!\n`;
                        // make loser lose 10% health
                        attacker.health = Math.floor(attacker.health * 0.9);
                        // ^ that works too, the same as the code below.
                        // attacker.health = attacker.health - Math.floor(attacker.health * 0.10);
                    }
                }
            }

            else if(attacker.mAttack > attacker.attack && attacker.mana > 0){
                result = 'Magical fight!\n';
                result += (`${attacker.name} a ${attacker.race} ${attacker.gender} ${attacker.class}, with ${attacker.mAttack} attack and ${attacker.mana} mana, is attacking ${defender.name} a ${defender.race} ${defender.gender} ${defender.class}, with ${defender.mDefense} defense and ${defender.health} health!\n`);
                if(defender.health <= 0){ result += `\n\nI am sorry to say, but ${defender.name} is ALREADY dead and can not fight.\n\n`; }
                else{
                    // make damage ranodom.
                    // for now make check based on the random damage thing. later think about some other solution, to check the attack skill against defense and then figure out forumla for damage.
                    attacker.mana = attacker.mana - Math.floor(Math.random() * 7) + 1;
                    const damageDone = Math.floor((attacker.mAttack * (Math.floor(Math.random() * 7) + 1)) / 2.5);
                    if(attacker.mAttack > defender.mDefense){
                        // make loser lose health from attack.
                        defender.health = defender.health - (damageDone - defender.mDefense);
                        result += `The attacker ${attacker.name} WINS against ${defender.name}!\nDefender ${defender.name} remains with ${defender.health} health!\nThe attacker loses some mana and is now left with: ${attacker.mana}\n`;
                        if(defender.health <= 0){ result += `\n\n${defender.name} DIES after his confrontation with the attacker ${attacker.name}!\n\n`; }
                    }
                    else if(attacker.mAttack == defender.mDefense){ result += `It's a DRAW between ${attacker.name} and ${defender.name}!\nThe attacker loses some mana and is now left with: ${attacker.mana}\n`; }
                    else if(attacker.mAttack < defender.mDefense){
                        result += `The attacker ${attacker.name} LOSES the fight against ${defender.name} and now has to pay the price by losing 10% of his health!\nThe attacker loses some mana and is now left with: ${attacker.mana}\n`;
                        // make loser lose 10% health
                        attacker.health = attacker.health - Math.floor(attacker.health * 0.10);
                    }
                }
            }
            fs.appendFile('fight.txt', result + '\n', function(err){
                if(err){ console.error(err); }
            });
        }
    }
    catch(err){ return console.error(err); }
}

async function run(){
    try{
        await fs.writeFileSync('fight.txt', '', function(err){ if(err){ return console.error(err); }});
        await fs.writeFileSync('chars.json', '', (err) =>{ if(err){ return console.error(err); }});
        await fs.writeFileSync('names.txt', '', (err) => { if(err){ return console.error(err); }});

        await generateNames();
        await fs.appendFileSync('names.txt', orcNames.join(', '), (err) => { if(err){ return console.error(err); }});
        await fs.appendFileSync('names.txt', elfNames.join(', '), (err) => { if(err){ return console.error(err); }});
        await fs.appendFileSync('names.txt', humanNames.join(', '), (err) => { if(err){ return console.error(err); }});
        await makeCharacters();
        let createdCharacters = 0;
        for(const i of characters){ createdCharacters++; }
        console.log('Created characters: ' + createdCharacters + '\n\n');

        for(let x = 0; x < characters.length; x++){
            for(let y = 0; y < characters.length; y++){ fight(characters[x], characters[y]); }
        }
        console.log('Possible fights: ' + possibleFights);

        let deadCounter = 0;
        let aliveCounter = 0;
        for(const i of characters){
            if(i.health <= 0){
                console.log('Dead: ' + i.name);
                deadCounter++;
            }
            else if(i.health > 0){
                console.log('Alive: ' + i.name);
                aliveCounter++;
            }
        }
        console.log('Alive: ' + aliveCounter + ' | Dead: ' + deadCounter);
    }
    catch(err){ throw new Error(err); }
}

try{
    run();
}
catch(err){ throw new Error(err); }