const users = [
    {
        id: 1,
        name: 'Naggot Nugent',
        health: 100,
        attack: 10,
        defense: 5,
        mana: 20,
        magicAttack: 10,
        magicDefense: 5,
    },
    {
        id: 2,
        name: 'Rykter Rokter',
        health: 200,
        attack: 5,
        defense: 10,
        mana: 10,
        magicAttack: 5,
        magicDefense: 1,
    },
    {
        id: 3,
        name: 'Fredrik Fragent',
        health: 50,
        attack: 20,
        defense: 1,
        mana: 40,
        magicAttack: 5,
        magicDefense: 10,
    },
    {
        id: 4,
        name: 'Starling Steph',
        health: 300,
        attack: 1,
        defense: 20,
        mana: 30,
        magicAttack: 7,
        magicDefense: 7,
    },
];

// Addding more users is easy.
// They require a ID, name, health, attack, defense, mana, magicAttack and magicDefense.
// then push to users.push(new)

for(let x = 0; x < users.length; x++){
    for(let y = 0; y < users.length; y++){
        fightPhysical(users[x], users[y]);
        fightMagic(users[x], users[y]);
    }
}

function fightPhysical(attacker, defender){
    if(attacker.id == defender.id){ return; }
    else{
        let result;
        console.log('Physical fight\n');
        console.log(`Attacker ${attacker.name}, with ${attacker.attack} attack is attacking ${defender.name}, with ${defender.defense} defense and ${defender.health} health!`);
        if(defender.health <= 0){
            return console.log(`I am sorry to say, but ${defender.name} is dead.\n`);
        }
        const damageDone = (attacker.attack * (Math.floor(Math.random() * 8) + 1)) / 1.5;
        if(damageDone > defender.defense){
            defender.health = defender.health - (damageDone - defender.defense);
            result = `The attacker ${attacker.name} WINS this fight against ${defender.name}, leaving ${defender.name} with ${defender.health} health!`;
            if(defender.health <= 0){
                return console.log(`I am sorry to say, but ${defender.name} is dead.\n`);
            }
        }
        else if(damageDone == defender.defense){
            result = `It's a DRAW between ${attacker.name} and ${defender.name}!`;
        }
        else if(damageDone < defender.defense){
            result = `The attacker ${attacker.name} LOSES the fight against ${defender.name}!`;
        }
        return console.log(result + '\n');
    }
}

function fightMagic(attacker, defender){
    if(attacker.id == defender.id){ return; }
    else{
        let result;
        const damageDone = (attacker.magicAttack * (Math.floor(Math.random() * 8) + 1)) / 1.5;
        console.log('Magical fight\n');
        console.log(`Attacker ${attacker.name}, with ${attacker.magicAttack} magic attack and ${attacker.mana} mana is attacking ${defender.name}, with ${defender.magicDefense} magic defense and ${defender.health} health!`);
        if(defender.health <= 0){
            return console.log(`I am sorry to say, but ${defender.name} is dead.\n`);
        }
        if(attacker.mana <= 0){
            return console.log(`I am sorry, but ${attacker.name} has ${attacker.mana} and is unable to attack.`);
        }
        attacker.mana = attacker.mana - (Math.floor(Math.random() * 8) + 1);
        if(damageDone > defender.magicDefense){
            defender.health = defender.health - (damageDone - defender.magicDefense);
            result = `The attacker ${attacker.name} WINS this fight against ${defender.name}, leaving ${defender.name} with ${defender.health} health! The attacker loses mana, remaing with: ${attacker.mana}`;
            if(defender.health <= 0){
                return console.log(`I am sorry to say, but ${defender.name} dies in the fight.\n`);
            }
        }
        else if(damageDone == defender.magicDefense){
            result = `It's a DRAW between ${attacker.name} and ${defender.name}! The attacker loses mana, remaing with: ${attacker.mana}`;
        }
        else if(damageDone < defender.magicDefense){
            result = `The attacker ${attacker.name} LOSES the fight against ${defender.name}! The attacker loses mana, remaing with: ${attacker.mana}`;
        }
        return console.log(result + '\n');
    }
}