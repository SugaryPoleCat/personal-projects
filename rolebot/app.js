const Discord = require('discord.js');
const token = require('./botconfig.json').token;
const prefix = require('./botconfig.json').prefix;
const fs = require('fs');

const client = new Discord.Client();

let r_what;
let arrayOfRoles = [];
function findRoles(arrayName, message){
    for(let x = 0; x <= arrayName.length; x++){
        const foundRole = message.member.roles.find(r => r.name == arrayName[x]);
        if(foundRole) arrayOfRoles.push(foundRole.name);
    }
}

function searchRoles(roleName, message){
    const r_roleName = message.guild.roles.find(role => role.name == roleName);
    return r_roleName;
}

async function joinRole(message){
    try{
        const arr_things = ['Unicorn', 'Earth pony', 'Pegasus', 'Alicorn', 'Changeling', 'Batpony', 'Wolf', 'Fox', 'Other', 'Horse', 'Male', 'Female', 'Stalion', 'Mare', 'Other gender', 'Artisans', 'Cooks', 'Maids', 'Hospital staff', 'RP', 'NSFW'];
        const intro_embed = new Discord.RichEmbed()
            .setTitle('JOIN')
            .setThumbnail(message.author.avatarURL)
            .setColor(message.member.displayHexColor)
            .setDescription(`What do you wish to join? You can join:\n\`\`\`${type_match.join(', ')}\`\`\`\nI'm waiting 15 seconds for you to pick, before canceling.`);
            await message.channel.send(intro_embed);
            try{
                const collected = await message.channel.awaitMessages(type, { maxMatches: 1, time: 15000, errors: ['time'] });
                r_type = collected.first().content.toLowerCase();
                console.log('r_type : ' + r_type);
            }
            catch(err2){
                await message.reply('You waited to long. Try again.');
                return console.error(err2);
            }
    }
    catch(err){
        await message.reply('Sorry but something went wrong, tell Sugar about it');
        return console.error(err);
    }
}

client.once('ready', () => {
    console.log('=========\nREADY\n=========');
});

client.on('message', async message => {
    try{
        if(message.author.bot == true){ return; }
        else{
            // console.log('jesus eslint wont shut up about lonely if, fine here you go jesus');
            if(message.content.startsWith(prefix) == true){ 
                await joinRole(message);
            }
            else{ return; }
        }
    }
    catch(err){ throw new Error(err); }
});

client.login(token).then(console.log('logged in'));