const express = require('express');
const path = require('path');

const app = express();

const exphbs = require('express-handlebars');
app.engine('handlebars', exphbs({ defaultLayout: 'main'} ));
app.set('view engine', 'handlebars');

const indexRouter = require('./routes/index');

// app.get('/', (req, res)=>{
//     res.send('Helloooo');
// });

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// app.get('/', (req, res) => res.render('index', {
//     title: 'Nigga',
// }));

app.use('/', indexRouter);
// app.use('/userlist', userListRouter);
// app.use('/oclist', ocListRouter);
// app.use('/userpage', userPageRouter);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Listening on port ${PORT}!`));