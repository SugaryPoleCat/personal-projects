// const Discord = require('discord.js');
const logger = require('../utils/logger');

async function run(){
    const what = process.argv[2];
    if(what == 'process-test'){
        try{
            const test = process.argv[3];
            logger.log('INFO', test, 'This is the argument');
            const [ , , ...args ] = process.argv;
            logger.log('INFO', args.join(', '), 'This is also possible. This gets all the processes following the BIN (argv[0]) and SOURCEPATH (argv[1])');
        }
        catch(err){
            logger.log('ERROR', err);
        }
    }
    if(what == 'arr'){
        const array = ['nigg', 'fag', 'pussy'];
        const result = array.slice(0, 2);
        logger.log('INFO', result, 'This is an array of multiple things');
    }
}
run();