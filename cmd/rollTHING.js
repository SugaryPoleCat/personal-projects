const utils = require('../utils/utils');
const logger = require('../utils/logger');
let total = 0;
let arr_totals = [];
const input = '!troll 2d20';
const input2 = '!troll 2d20 + d30';

async function getTotal(times, rolling){
    if(!times){
        times = await 1;
    }
    await logger.log('LOG', times, 'times');
    for(let i = 0; i < times; i++){
        const rolled = await getRoll(rolling);
        await logger.log('LOG', rolled, 'rolled');
        await arr_totals.push(rolled);
        // total += rolled;
    }
    // total = arr_totals[0];
}
async function getRoll(rolling){
    const ayyroll = await Math.floor((Math.random() * rolling) + 1);
    // await logger.log('LOG', ayyroll, 'ayyroll');
    return ayyroll;
}
async function bitch(){
try{
    // * repeats at least 0 times, + repeasts at least 1 time. This way you get, d20,,20;
    // const curated = input.slice('!troll '.length);
    const curated = await process.argv[2];
    await logger.log('LOG', curated, 'curated');
    const modulator = await process.argv[3];
    await logger.log('LOG', modulator, 'modulator');
    const matches = await curated.match(/^(\d*)d(\d+)$/);
    await logger.log('LOG', matches, 'matches');
    const amount = await matches[2];
    await logger.log('LOG', amount, 'amount');
    const multiplier = await matches[1];
    await logger.log('', multiplier, 'multiplier');

    await getTotal(multiplier, amount);
    const rolled = await getRoll(amount);

    switch(modulator){
        case 'mod':
            for(let x = 0; x < arr_totals.length; x++){
                total += await arr_totals[x];
                await logger.log('LOG', total, 'total');
            }
            total = await total / arr_totals.length;
            break;
        case 'add':
            for(let x = 0; x < arr_totals.length; x++){
                total += await arr_totals[x];
                await logger.log('LOG', total, 'total');
            }
            break;
        case 'mul':
        // FOR THIS ONE TRY TO GET THE FIRS OF THE ARRAY AS THE TOTAL FIRST SUM THEN START COUNTING FROM ONE INDEX ABOV ETHAT. HOW? IDK
            for(let x = 0; x < arr_totals.length; x++){
                total = total * await arr_totals[x];
                await logger.log('LOG', total, 'total');
            }
            break;
        default:
            for(let x = 0; x < arr_totals.length; x++){
                total += await arr_totals[x];
                await logger.log('LOG', total, 'total');
            }
            break;
    }

    await console.log(`\nMatches: ${matches}\narr_totals: ${arr_totals}\ntotal: ${total}`);
}
catch(err){
    logger.log('ERROR', err);
}
}
bitch();
/* MAKE IT MORE ADVANCED
HOW? Simple. Have a QUANTITY: 2d, have number of sides 2d20, have some sort of operation like 2d20+3.
Also to make it more advanced, maybe something like 2d20+3 - 2d20+2

SO.
1
so how do we do taht?
*/

// try{
//     const curated2 = input2.slice('!troll '.length);
//     const matches2 = curated2.match(/^(\d*)d(\d+)$/);
//     console.log(matches2);
//     const amount = matches2[2];
//     const multiplier = matches2[1];

//     getTotal(multiplier, amount);
//     const rolled = getRoll(amount);

//     console.log(`Message.content: ${input}\nMatches: ${matches2}\nRolled: ${rolled}\narr_totals: ${arr_totals}\ntotal: ${total}`);
// }
// catch(err){
//     logger.log('ERROR', err);
// }

// try{
// // require the dice-roller library
// const { DiceRoller } = require('rpg-dice-roller');

// // create a new instance of the DiceRoller
// const diceRoller = new DiceRoller();

// // roll the dice
// diceRoller.roll('4d20-L');

// // get the latest dice rolls from the log
// const latestRoll = diceRoller.log.shift();

// // output the latest roll - it has a toString method for nice output
// console.log(latestRoll + '');
// }
// catch(err){
//     logger.log('ERROR', err);
// }