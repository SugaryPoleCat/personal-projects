const utils = require('../utils/utils');
const logger = require('../utils/logger');

let x = [5, 4, 10, '2'];
let y = [3, 8, 10, 2];

let arr_compare = ['===', '==', '<', '>', '<=', '>=', '!==', '!='];
let arr_math = ['+', '-', '*', '/'];
let arr_phrase = ['Nope', 'am i alive', 'what is life', 'There is a booot in my snake', 'no one lives forever, except me'];

function compare(arr, arrNum1, arrNum2){
    let result;
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arrNum1.length; j++){
            for(let k = 0; k < arrNum2.length; k++){
                result = utils.comparator(arrNum1[j], arrNum2[k], arr[i]);
                console.log(`A: ${arrNum1[j]} ${arr[i]} B: ${arrNum2[k]}\nResult: ${result}`);
            }
        }
    }
}

function mathman(arr, arrNum1, arrNum2){
    let result;
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arrNum1.length; j++){
            for(let k = 0; k < arrNum2.length; k++){
                // This will not work if a number is in a string, despite conversion in the function
                // Like at all. Like, not even result.toFixed fixes that.
                result = (utils.mathNumbers(arrNum1[j], arrNum2[k], arr[i])).toFixed(3);
                // This will work. but will refuse to use .toFixed.
                result = (utils.mathNumbers(arrNum1[j], arrNum2[k], arr[i]));
                console.log(`MATHMAN\nA: ${arrNum1[j]} ${arr[i]} B: ${arrNum2[k]}\nResult: ${result}`);
            }
        }
    }
}

function mathman2(arr, arrNum1, arrNum2){
    let result;
    for(let i = 0; i < arr.length; i++){
        for(let j = 0; j < arrNum1.length; j++){
            for(let k = 0; k < arrNum2.length; k++){
                // This will not work if a number is in a string, despite conversion in the function
                // Like at all. Like, not even result.toFixed fixes that.
                result = (utils.mathNumbers2(arrNum1[j], arrNum2[k], arr[i])).toFixed(3);
                // This will work. but will refuse to use .toFixed.
                result = (utils.mathNumbers2(arrNum1[j], arrNum2[k], arr[i]));
                console.log(`MATHMAN2\nA: ${arrNum1[j]} ${arr[i]} B: ${arrNum2[k]}\nResult: ${result}`);
            }
        }
    }
}

try{
    compare(arr_compare, x, y);
    mathman(arr_math, x, y);
    mathman2(arr_math, x, y);
    console.log(utils.capitalize('is this a pussy'));
    for(let i = 0; i < arr_phrase.length; i++){
        console.log(utils.capitalize(arr_phrase[i]));
    }
}
catch(err){
    logger.log('ERROR', err);
}
try{
    console.log(utils.randomNumber(1, 10));
    let arr_idk = ['error', 'warning', 'success', 'random', ''];
    for(let i = 0; i < arr_idk.length; i++){
        console.log(utils.randomHexColor(arr_idk[i]));
    }
    for(let k = 0; k < arr_phrase.length; k++){
        console.log(utils.capitalizeWords(arr_phrase[k]));
    }
    for(let p = 0; p < x.length; p++){
        console.log(utils.randomNumber(1, x[p]));
    }
}
catch(err){
    logger.log('ERROR', err);
}
try{
    const myObj = new Object(),
    str = 'myString',
    rand = Math.random(),
    obj = new Object();

myObj.type              = 'Dot syntax';
myObj['date created']   = 'String with space';
myObj[str]              = 'String value';
myObj[rand]             = 'Random Number';
myObj[obj]              = 'Object';
myObj['']               = 'Even an empty string';

console.log(myObj);
}
catch(err){
    logger.log('ERROR', err);
}

try{
    const diceThrows = [];
    for(let i = 0; i < 5; i++){
        diceThrows.push(utils.randomNumber(1, 6));
    }
    for(let k = 0; k < diceThrows.length; k++){
        console.log(utils.mathNumbers(diceThrows[k], diceThrows[k], '+'));
    }
    console.log(diceThrows);
}
catch(err){
    logger.log('ERROR', err);
}