const input = "\n   !wash  sugar \"VERY very thurougly\"";

function parseArgs(input) {
	const len = input.length;
	let curStr = "";
	let curArgs = [];
	let isParsingArg = false;
	let parsingArgHasQuotes = false;
	for(let pos = 0; pos < len; pos++) {
		const c = input[pos];
		if (!isParsingArg) {
			if (c.match(/\s/)) {
				continue;
			}
			isParsingArg = true;
			parsingArgHasQuotes = c == "\"";
			curStr = parsingArgHasQuotes ? "" : c;
			continue;
		}

		if (!parsingArgHasQuotes) {
			if (c.match(/\s/)) {
				isParsingArg = false;
				curArgs.push(curStr);
				continue;
			}
			curStr += c;
			continue;
		}

		if (c == "\"" && (pos + 1 >= len || input[pos+1].match(/\s/))) {
			isParsingArg = false;
			curArgs.push(curStr);
			continue;
		}
		curStr += c;
	}
	if (isParsingArg) {
		curArgs.push(curStr);
	}
	return curArgs;
}

console.log(parseArgs(input));
