// const config = require('../../config.json');
const logger = require('../utils/logger');
const Discord = require('discord.js');
async function test(){
    try{
        // GET TIMES.
        const pastDate = parseInt(1557967101000 / 1000);
        logger.log('LOG', pastDate, 'pastDate');
        // const nowDate = parseInt(Date.now() / 1000);
        const nowDate = parseInt(1558051701000 / 1000);
        logger.log('LOG', nowDate, 'nowDate');
        const pastDateUTC = new Date(pastDate * 1000);
        logger.log('', pastDateUTC, 'pastDateUTC');

        // these are in secodns.
        const oneHour = 3600;
        logger.log('LOG', oneHour, 'oneHour');
        const oneMinute = 60;
        logger.log('LOG', oneMinute, 'oneMinute');
        // 1440 minutes is an hour.
        const oneDay = oneHour * 24;
        logger.log('LOG', oneDay, 'oneDay');

        // Get the difference in time.
        const between = nowDate - pastDate;
        logger.log('LOG', between, 'between');
        const diffDay = between / oneDay;
        logger.log('LOG', diffDay, 'diffDay');
        const diffHour = between / oneHour;
        logger.log('LOG', diffHour, 'diffHour');
        const diffMinute = parseInt(between / oneMinute);
        logger.log('LOG', diffMinute, 'diffMinute');

        // This cheks if you are still on a daily streak.
        if(between <= oneDay * 2){logger.log('', 'You are still on a daily streak! +1 point!');}
        else{logger.log('', 'Your streak has been lost!');}

        // Check if the difference between past date and now date is bigger than 86400 or equal.
        if(between >= oneDay) logger.log('', 'The BETWEEN is BIGGER than a day.');
        else{
            logger.log('', 'The BETWEEN is SMALLER than a day.');
            // Now the logic on how long you must wait.
            const waitHours = parseInt(24 - diffHour);
            logger.log('', waitHours, 'waitHours');
            // now if hours is non esitant...
            if(waitHours == 0){
                const waitMinutes = 1440 - diffMinute;
                logger.log('', waitMinutes, 'waitMinutes');
            }
        }

    }
    catch(err){
        logger.log('ERROR', err);
    }
}
test();