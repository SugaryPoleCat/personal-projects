const logger = require('../utils/logger');
const utils = require('../utils/utils');
const reasonWarn = '!warn <@2324834329328> reason I am gay.';
const noReasonWarn = '!warn <@2324834329328> I am penis.';

let args;
let what;
let reason;
let target;
const description = 'This person is getting warned.\n';

async function processText(text){
    await console.log('Length of string: ' + text.length);
    target = await text.match(/<@(\d+)>/);
    await logger.log('LOG', target, 'target');
    args = await text.split(/ +/);
    for(let x = 0; x < args.length; x++){
        await console.log('Length of each arg: ' + args[x].length);
    }
    await logger.log('LOG', args, 'args');
    what = await args[2];
    await logger.log('LOG', what, 'what');
    reason = await text.slice(6 + target[0].length + 1 + what.length + 1);
    await logger.log('LOG', reason, 'reason');
}
async function run(){
try{
    await processText(noReasonWarn);
    switch(target[1]){
        case undefined:
            return console.log('Target is undefined');
        case '2324834329328+':
            return console.log('ID matches');
        default:
            await console.log('Target is found');
            switch(what){
                case 'reason':
                    await console.log('reason');
                    if(reason) return console.log(description + 'Reason: ' + reason);
                    else return console.log(description + 'No reason provided');
                case 'pardon':
                    await console.log('pardon');
                    if(reason) return console.log(description + 'Reason: ' + reason);
                    else return console.log(description + 'No reason provided');
                case 'reset':
                    await console.log('Reset');
                    if(reason) return console.log(description + 'Reason: ' + reason);
                    else return console.log(description + 'No reason provided');
                default:
                    await console.log('what not found');
                    if(reason) return console.log(description + 'Reason: ' + reason);
                    else return console.log(description + 'No reason provided');
            }
    }
}
catch(err){
    await logger.log('ERROR', err);
}
}
run();