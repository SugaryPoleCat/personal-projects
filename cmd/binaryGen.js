const logger = require('../utils/logger');

try{
    const input = 'This is my text';
    logger.log('', input, 'input');
    const inputVal = input.value;
    logger.log('', inputVal, 'inputVal');
    const output = parseInt(inputVal, 2).toString(10);
    logger.log('', output, 'output');
}
catch(err){
    logger.log('ERROR', err);
}