const logger = require('../utils/logger');
async function bitch(X, Y, Z){
    try{
        let rolls = [];
        // logger.log('', rolls, 'rolls');
        let total = 0;
        logger.log('', total, 'total');
        // const x = process.argv[2];    // Sides
        const x = X;
        logger.log('', x, 'x');
        // const y = process.argv[3];    // Times
        const y = Y;
        logger.log('', y, 'y');
        // const z = process.argv[4];    // Mod
        let z = Z;
        logger.log('', z, 'z');
        if(y && !isNaN(y)){
            logger.log('', 'Y exists and is a number');
            for(let j = 0; j < y; j++){
                rolls[j] = Math.floor((Math.random() * x) + 1);
                logger.log('', rolls[j], 'rolls[j]');
            }
        }
        else{
            logger.log('', 'Y doesnt exist or is not a number');
            total = x;
            logger.log('', total, 'total');
        }
        logger.log('', rolls, 'rolls');
        logger.log('', rolls[0], 'rolls[0]');
        logger.log('', rolls.length, 'rolls.length');
        const rolllength = rolls.length + 1;
        if(z && isNaN(z)){
            logger.log('', 'Z found and is not a number');
            total = rolls[0];
            logger.log('', total, 'total');
            z = z.toLowerCase();
            for(let k = 1; k < rolls.length; k++){
                logger.log('', 'For');
                if(z == 'add'){
                    total += rolls[k];
                    logger.log('', total, 'Add');
                }
                else if(z == 'sub'){
                    total -= rolls[k];
                    logger.log('', total, 'Sub');
                }
                else if(z == 'mul'){
                    total *= rolls[k];
                    logger.log('', total, 'Mul');
                }
                else if(z == 'div'){
                    total /= rolls[k];
                    logger.log('', total, 'Div');
                }
                else if(z == 'mod'){
                    total += rolls[k];
                    total = total / rolls.length;
                    logger.log('', total, 'Mod');
                }
            }
            logger.log('', `INT: ${parseInt(total)}, Actual: ${total.toFixed(3)}`, 'total');
        }
        else{
            logger.log('', 'Z doesnt exist or is a number');
        }
    }
    catch(err){
        logger.log('ERROR', err);
    }
}
const modulators = ['add', 'sub', 'mul', 'div', 'mod'];
const numOne = [20, 10, 5];
const numTwo = [1, 2, 5];
for(let q = 0; q < numOne.length; q++){
    for(let w = 0; w < numTwo.length; w++){
        for(let e = 0; e < modulators.length; e++){
            logger.log('', `Now doing = sides: ${numOne[q]}, times: ${numTwo[w]}, modulator: ${modulators[e]}`);
            bitch(numOne[q], numTwo[w], modulators[e]);
        }
    }
}
// bitch(20, 2, 'div');