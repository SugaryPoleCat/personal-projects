const Discord = require('discord.js');
const token = require('./botconfig.json').token;
const prefix = require('./botconfig.json').prefix;
const CAT_API_KEY = require('./botconfig.json').CAT_API_KEY;
const querystring = require('querystring');
const r2 = require('r2');
// FS in case we want to delete the files.
const fs = require('fs'),
    gm = require('gm').subClass({ imageMagick: true });

const client = new Discord.Client();
const colors = ['red', 'white', 'black', 'blue', 'black', 'yellow', 'green'];

async function doAction(message){
    try{
        const args = message.content.slice(prefix.length).split(/ +/);
        const what = args[0].toLowerCase();
        const action = args[1];
        let previous = args[2];
        let image, messageArray, foundMessage, intensity, color, returnImage, stringus;
        let images, breed;
        let something, whatever;
        switch(what){
            case 'pic':
                if(previous == 'previous'){
                    // has to be await otehrwise it cant find the message;
                    messageArray = await message.channel.fetchMessages({ limit: 2 });
                    foundMessage = messageArray.find(msg => msg.attachments.size >= 1);
                    if(!foundMessage){ return message.reply('There has to be a message with an image before this one, darling.'); }
                    else{ image = foundMessage.attachments.first().proxyURL; }
                }
                else if(message.mentions.users.size >= 1){ image = message.mentions.users.first().displayAvatarURL; }
                else{ image = message.author.displayAvatarURL; }
                returnImage = gm(image).noProfile();
                switch(action){
                    case 'flip':
                        returnImage.out('-flip');
                        break;
                    case 'invert':
                        returnImage.out('-channel', 'RGB', '-negate');
                        break;
                    case 'mirror':
                        returnImage.out('-flop');
                        break;
                    case 'pixelate':
                        intensity = checkNumber(args, message, 30);
                        // SCALE DOWN
                        intensity = 0.1 / intensity * 100;
                        console.log('[' + action.toUpperCase() + ' SCALE DOWN] = Intensity: ' + intensity);
                        returnImage.scale(intensity + '%');
                        // SCALE UP
                        intensity = 100 * (100 / intensity);
                        console.log('[' + action.toUpperCase() + ' SCALE UP] = Intensity: ' + intensity);
                        returnImage.scale(intensity + '%');
                        break;
                    case 'charcoal':
                        intensity = checkNumber(args, message, 100);
                        console.log('[' + action.toUpperCase() + '] = Intensity: ' + intensity);
                        returnImage.out('-channel', 'RGB', '-charcoal', intensity);
                        break;
                    case 'explode':
                        intensity = checkNumber(args, message, 100);
                        console.log('[' + action.toUpperCase() + '] = Intensity: ' + intensity);
                        returnImage.implode(0 - intensity);
                        break;
                    case 'transparent':
                        intensity = checkNumber(args, message, 100);
                        if(previous == 'previous'){ color = args[3]; }
                        else if(message.mentions.users.size >= 1){ color = args[4]; }
                        else{ color = args[3]; }
                        if(color == undefined){ color = '#' + Math.floor(Math.random() * 2 ** 24).toString(16); }
                        console.log('[' + action.toUpperCase() + '] = Fuzz: ' + intensity + ' | Color: ' + color);
                        returnImage.fuzz(intensity).transparent(color);
                        break;
                    case 'magnify':
                        intensity = checkNumber(args, message, 200);
                        console.log('[' + action.toUpperCase() + '] = Intensity: ' + intensity);
                        returnImage.magnify(intensity);
                        break;
                    case 'blur':
                        intensity = checkNumber(args, message, 200);
                        console.log('[' + action.toUpperCase() + '] = Intensity: ' + intensity);
                        returnImage.blur(intensity);
                        break;
                    default:
                        console.error('Well, something done fucking goofed');
                        return message.reply('Nibba you didnt use one of the right commands lol');
                }
                await returnImage
                    .toBuffer('PNG', (err, buffer) => {
                        console.log(buffer);
                        if(err){ return console.error(err); }
                        else{
                            message.channel.send(new Discord.Attachment(buffer, action + 'Image.png'))
                                .then(console.log('Sent attachemnt with: [' + action + ']'));
                        }
                    });
                await returnImage
                    .write('./discordbotthing/' + action + 'Image.png', (err) => {
                        if(err){ return console.error(err); }
                        else{ console.log('Write of [' + action + '] is succesful.'); }
                    });
                return;
            case 'txt':
                switch(action){
                    case 'zalgo':
                        stringus = message.content.slice(12 + previous.length);
                        previous = parseInt(args[2]);
                        if(previous <= 0) previous = 1;
                        else if(previous > 10) previous = 10;
                        console.log(stringus);
                        stringus = zalgoIt(stringus, previous);
                        break;
                    default:
                        return message.reply('shit dumbass you\'re a stupid mothafucka');
                }
                return message.reply(stringus);
            case 'cat':
                something = args[1];
                whatever = args[2];
                // images is basically the json object of image
                console.log(args);
                images = await loadImage(message, something, whatever);
                // so get the first json object
                image = images[0];
                // and its breed
                breed = image.breeds[0];
                console.log('getting a kitty', 'showing', breed);
                // then post it as a file, using the json object url.
                return message.channel.send(new Discord.RichEmbed()
                    .setTitle(breed.name)
                    .setAuthor(message.author.username, message.author.displayAvatarURL)
                    .setFooter('Cat command', client.user.displayAvatarURL)
                    .setImage(image.url)
                    .addField('Intelligence', `${breed.intelligence} / 5`, true)
                );
            case 'fox':
                images = Math.floor(Math.random() * (122 - 1)) + 1;
                image = `http://randomfox.ca/images/${images}.jpg`;
                return message.reply('***NEW FOX***', { files: [image] });
            default:
                return message.reply('You dumbass, you have to use `!pic` or `!txt` or `!cat`');
        }
    }
    catch(err){ return console.error(err); }
}

async function loadImage(message, what, level){
    const CAT_API_URL = 'https://api.thecatapi.com/';
    console.log(what, level);
    try{
        const headers = {
            'x-api-key': CAT_API_KEY,
        };
        let query_params = {
            // IF you only want pictures that is in SOME breed category, must set to true
            'has_breeds': true,
            // Discord no likey .gif? SO i heard.
            'mime_types': 'jpg,png,gif',
            // Dfine size. Small is perfect. Even meidum.
            'size': 'small',
            // USE the sub_id to TRACK which user used the commands the most. AKA WHO LOVES KITTENS THE MOST!!!
            'sub_id': message.author.username,
            // we only want ONE image, unless you're making something else than a discord bot.
            'limit': 1,
        };
        let query_params2;
        if(what){
            if(what == 'breed'){ query_params2 = { 'breed': level };}
        }
        console.log(JSON.stringify(query_params));
        // Stringify the queary params specified above, so that we can use them in the URL to get your precious IMAGE!
        // well actually, what you're getting is, specified in below try/catch.
        let queryString = querystring.stringify(query_params);
        if(what){ queryString += '&' + querystring.stringify(query_params2); }
        console.log(queryString);
        let response;
        try{
            const _url = CAT_API_URL + `v1/images/search?${queryString}`;
            // r2 gets the request, like postman shit, with hreaders being your API, cause you need access, then makes it into json, which we can then use to get ONE or MANY MORE images.
            // BUT we want just one image so we use JUST the limit: 1 in hte params and thus we dont need to iterate through json for multiple files
            // yadaydayd, it works shut up.
            console.log(_url);
            response = await r2.get(_url, { headers }).json;
            console.log(JSON.stringify(response));
        }
        catch(err2){ message.reply('something done goofed, check console'); return console.error(err2); }
        return response;
    }
    catch(err){ return console.error(err); }
}

const zalgoIt = function(str, num){
    try{
        // num is power, cause it gonna remake the text into zalgo text the NUM amount of times
        console.log('zalgoing', str, 'x', num);
        const chars = ['̍', '̎', '̄', '̅', '̿', '̑', '̆', '̐', '͒', '͗', '͑', '̇', '̈', '̊', '͂', '̓', '̈́', '͊', '͋', '͌', '̃', '̂', '̌', '̀', '́', '̋', '̏', '̒', '̓', '̔', '̽', '̉', '̾', '͆', '̚', '̖', '̗', '̘', '̙', '̜', '̝', '̞', '̟', '̠', '̤', '̥', '̦', '̩', '̪', '̫', '̬', '̭', '̮', '̯', '̰', '̱', '̲', '̳', '̹', '̺', '̻', '̼', 'ͅ', '͇', '͈', '͉', '͍', '͎', '͓', '͚', '̣', '̕', '̛', '̀', '́', '͘', '̡', '̢', '̧', '̨', '̴', '̵', '̶', '͏', '͜', '͝', '͞', '͟', '͠', '͢', '̸', '̷', '͡', '҉'];
        // const randChar = ()=>chars[Math.floor(Math.random() * chars.length)];
        str = Array.from(str).map(char => {
            for(let i = 0; i < num; i++){
                char += chars[Math.floor(Math.random() * chars.length)];
            }
            return char;
        }).join('');
        console.log('zalgoed to', str);
        return str;
    }
    catch(err){ return console.error(err); }
};

function checkNumber(args, message, max){
    try{
        let number;
        if(args[1] == 'previous'){ number = parseInt(args[3]); }
        else if(message.mentions.users.size >= 1){ number = parseInt(args[3]); }
        else{ number = parseInt(args[2]); }
        if(isNaN(number)){ number = Math.floor(Math.random() * max); }
        if(number <= 0){ number = 1; }
        else if(number > max){ number = max; }
        console.log('Checked number and the return is: ' + number);
        return number;
    }
    catch(err){ return console.error(err); }
}

// Get teh bot ready
client.once('ready', () => {
    console.log('======\nREADY\n======\n');
});

// on every mesage
client.on('message', async message => {
    try{
        if(message.author.bot == true){ return console.log('author bot'); }
        else{
            console.log('author not bot');
            if(message.content.startsWith(prefix) == true){
                await doAction(message);
            }
        }
    }
    catch(err){ throw new Error(err); }
});

// Login
client.login(token).then(console.log('logged in'));

process.on('exit', (code) => {
    client.destroy().then(console.log('Discord dead with code: \n' + code));
});