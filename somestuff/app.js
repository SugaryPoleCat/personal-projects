// This denotes it's a class.
// const EventEmitter = require('events');

const Logger = require('./logger');
const log = new Logger();

log.on('messageLogged', (arg) => {
    console.log('listener called', arg);
});

log.log('message');