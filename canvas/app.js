const Canvas = require('canvas');
const snekfetch = require('snekfetch');

function run() {
    try{
        // function searchRoles(roleName){
        //     const Guild = member.guild;
        //     const r_roleName = Guild.roles.find(role => role.name == roleName);
        //     return r_roleName;
        // }
        // await member.addRole(searchRoles('=== SPECIES ==='));
        // await member.addRole(searchRoles('=== PROFESSION ==='));
        // await member.addRole(searchRoles('=== ACCESS ROLES ==='));
        // await member.addRole(searchRoles('=== GENDER ==='));
        // await member.addRole(searchRoles('=== NORMAL ROLES ==='));

        const applyText = (canvas, text) => {
            const ctx = canvas.getContext('2d');
            let fontSize = 70;
            do{
                ctx.font = `${fontSize -= 10}px sans-serif`;
            } while (ctx.measureText(text).width > canvas.width - 300);
            return ctx.font;
        };
        // console.log(`${member.joinedAt} - A new member joined! ${member.id}`);
        const Channel = member.guild.channels.find(ch => ch.name === 'welcome');
        // if (!channel) return;
        if (!member.user.displayAvatarURL){
            const embed = new Discord.RichEmbed()
                .setTitle(`Welcome to ${member.guild.name}!`)
                .setDescription(`Welcome to the server ${member.user}! Unfortunately I was not able to get your avatar, hence you will get this unfortunate boring text as welcome!\n\nWelcome to the server! Please read through <#548135767742873620> before proceeding!\nIf you are confused what a channel does, please read here <#549039064867799049>! It should have all the information you need!\n<#548403623843856394> here you can view cool events that are happening or are planned to happen on the server!\nPLEASE do not mute <#548135859334021160> this  channel or mute \`@everyone\` as only \`STAFF\` and I are able to use that!\nAnd when all that has been completed, you are welcome to have fun!\n\nAlso listen to when others tell you to stop something, especially \`STAFF\`... You can discuss, why, but in private with staff.`)
                .setColor(utils.randomHexColor('random'))
                .setTimestamp();
            return Channel.send(embed);
        }
        // Create a canvas (width, height)
        const canvas = Canvas.createCanvas(700, 250);
        // Plane of existance
        const ctx = canvas.getContext('2d');

        // Load the image
        const background = await Canvas.loadImage('./welcome.png');
        // This stretches the image into the entire canvas
        ctx.drawImage(background, 0, 0, canvas.width, canvas.height);
        // Set color of border
        ctx.strokeStyle = '#fff';
        // Set width of borders
        ctx.lineWidth = 7;
        // The first 0 is X,width, the other is Y, height OFFSETS.
        ctx.strokeRect(0, 0, canvas.width, canvas.height);

        ctx.font = '28px sans-serif';
        ctx.fillStyle = '#fff';
        ctx.fillText(`Welcome to ${member.guild.name},`, canvas.width / 2.5, canvas.height / 3.5);

        ctx.font = applyText(canvas, `${member.displayName}!`);
        ctx.fillStyle = '#fff';
        ctx.fillText(member.displayName, canvas.width / 2.5, canvas.height / 1.8);

        ctx.beginPath();
        ctx.moveTo(125, 25);
        ctx.lineTo(52, 56);
        ctx.lineTo(25, 125);
        ctx.lineTo(52, 194);
        ctx.lineTo(125, 225);
        ctx.lineTo(195, 197);
        ctx.lineTo(225, 125);
        ctx.lineTo(195, 52);
        ctx.closePath();
        ctx.strokeStyle = '#fff';
        ctx.lineWidth = 10;
        ctx.shadowBlur = 20;
        ctx.stroke();

        // This part clips the avatar
        ctx.beginPath();
        ctx.moveTo(125, 25);
        ctx.lineTo(52, 56);
        ctx.lineTo(25, 125);
        ctx.lineTo(52, 194);
        ctx.lineTo(125, 225);
        ctx.lineTo(195, 197);
        ctx.lineTo(225, 125);
        ctx.lineTo(195, 52);
        ctx.closePath();
        ctx.clip();

        // Fetch avatar image
        const { body: buffer } = await snekfetch.get(member.user.avatarURL);
        // Apply image to buffer
        const avatar = await Canvas.loadImage(buffer);
        // To then load it to the thing and draw it. (image,X.offest,y.offest,x.size,y.size)
        ctx.drawImage(avatar, 25, 25, 200, 200);

        // And make it an attachment
        const attachment = new Discord.Attachment(canvas.toBuffer(), 'welcome-image.png');
        const embed = new Discord.RichEmbed()
            .setDescription('Welcome to the server! Please read through <#548135767742873620> before proceeding!\nIf you are confused what a channel does, please read here <#549039064867799049>! It should have all the information you need!\n<#548403623843856394> here you can view cool events that are happening or are planned to happen on the server!\nPLEASE do not mute <#548135859334021160> this  channel or mute `@everyone` as only `STAFF` and I are able to use that!\nAnd when all that has been completed, you are welcome to have fun!\n\nAlso listen to when others tell you to stop something, especially `STAFF`... You can discuss, why, but in private with staff.')
            .setColor(member.displayHexColor)
            .setTimestamp();

        await Channel.send(attachment);
        return Channel.send(embed);
    }
    catch(err){
        return utils.error(err);
    }
}