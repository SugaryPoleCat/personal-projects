// const config = require('../config.json');
// const Discord = require('discord.js');
/**
 * this gets either a random color or a previously encoded color.
 *
 * @param {*} what - you can pick if you want: 'error', 'warning', 'success', 'random' - for a radnom color.
 * @returns color.
 */
function randomHexColor(what){
    let clr = '';
    const hash = '#';
    let string;
    switch(what){
        case 'error':
            clr = 'bf0000';
            break;
        case 'warning':
            clr = 'f1a91d';
            break;
        case 'success':
            clr = '0bc661';
            break;
        case 'random':
            clr = Math.floor(Math.random() * 2 ** 24).toString(16);
            break;
        default:
            clr = Math.floor(Math.random() * 2 ** 24).toString(16);
            break;
        }
    return string = hash + clr;
}
/**
 * This gets a user highlight!
 *  thanks to soru
 * @param {*} member - THIS, is the member to get
 * @param {*} fallback- IF IT FAILS, this is the fallback
 * @returns Returns, either fallback or JUST the id of the member
 */
function getUserHighlight(member, fallback) {
    if(member){
        return [`<@${member.id}>`, member.id];
    }
    return [fallback, null];
}
/**
 * This gets a highlight from a message
 *
 * @param {*} message - WHICH message to get the highlight from
 * @returns getUserHighlight, which means either the ID or a fallback
 */
function getUserHighlightFromMessage(message) {
    return getUserHighlight(message.member, message.author.username);
}

/**
 * THIS gets a mention from a string
 *
 * @param {*} guild - WHICH guild to look through for the member, to highlight
 * @param {*} str - Which string to get it from
 * @returns getUserHighlight, which means either the ID or a fallback
 */
function getUserHighlightFromStr(guild, str){
    const matches = str.match(/^\s*<@!?(\d+)>\s*$/);
    let member = null;
    if(matches){
        member = guild.members.get(matches[1]);
    }
    return getUserHighlight(member, str);
}

/**
 * This gets a phrase, actually, it processes an array based on its length
 *
 * @param {*} what - The array to process based on its length
 * @returns A random value or phrase from the array
 */
function arrayRandomIndex(arr){
    return arr[Math.floor(Math.random() * arr.length)];
}

/**
 * This takes in a string and capitalises FIRST letter of the whole string.
 *
 * @param {*} string - The string to capitalise
 * @returns The capitalised string
 */
function capitalize(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * This should be in the REQUEST and SUGGESTION and REPORT thing
 */
// async function noClue(message, category, channel, guild){
//     let color = message.member.displayHexColor;
//     if(color == '#000000') color = randomHexColor('random');
//     const suggestion = message.content.slice(9);
//     const embed = new Discord.RichEmbed()
//         .setTitle(message.member.displayName)
//         .setColor(color)
//         .setThumbnail(message.member.displayAvatarURL);
//     const logChannel = guild.channels.find(ch => ch.name === config.channels[0]);
//     logChannel.send(embed.setDescription(`__They have a ${category} for us:__\n${suggestion}\n\n**By: **${message.author.tag} with **ID: **${message.author.id}`));
//     channel.send(embed.setDescription(`Thank you for your ${category}! The staff will look into it.`));
// }

/**
 * This compares 2 numbers between each otehr.
 * @param {*} x - The first number
 * @param {*} y - The second nunmber
 * @param {*} symbol - The symbol used to comapre them
 * @returns - The compared result
 */
function comparator(x, y, symbol){
    switch(symbol){
        case '===':
            return x === y;
        case '==':
            return x == y;
        case '<':
            return x < y;
        case '>':
            return x > y;
        case '<=':
            return x <= y;
        case '>=':
            return x >= y;
        case '!==':
            return x !== y;
        case '!=':
            return x != y;
        default:
            return false;
    }
}

/**
 * This takes in two numbers and a symbol, to do math with the numbers.
 * @param {*} x - The first number
 * @param {*} y - The second number
 * @param {*} symbol - what to do with them
 * @returns The result
 */
function mathNumbers(x, y, symbol){
    if(!isNaN(x)) x = parseFloat(x);
    if(!isNaN(y)) y = parseFloat(y);
    switch(symbol){
        case '+':
            return x + y;
        case '-':
            return x - y;
        case '*':
            return x * y;
        case '/':
            return x / y;
        default:
            return x + y;
    }
}

function mathNumbers2(x,y,symbol){
    if(!isNaN(x)) x = parseFloat(x);
    if(!isNaN(y)) y = parseFloat(y);
    return{
        '+': x + y,
        '-': x - y,
        '*': x * y,
        '/': x / y,
    }[symbol];
}

/**
 * This generates a random number.
 *
 * @param {*} min - The minimum value
 * @param {*} max - The maximum value
 * @returns - the random number
 */
function randomNumber(min, max){
    min = min ? parseInt(min, 10) : 1;
    max = max ? parseInt(max, 10) : min;

    if(max <= min){
        return min;
    }

    return Math.floor(Math.random() * (max - min + 1) + min);
}

function capitalizeWords(str){
    str = str.split(' ');
    for (let i = 0; i < str.length; i++){
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }
    return str.join(' ');
}
module.exports = {
    randomHexColor,
    getUserHighlight,
    getUserHighlightFromStr,
    getUserHighlightFromMessage,
    arrayRandomIndex,
    // noClue,
    capitalize,
    comparator,
    mathNumbers,
    randomNumber,
    capitalizeWords,
    mathNumbers2,
};