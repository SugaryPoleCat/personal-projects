const fs = require('fs');
const EventEmitter = require('events');
// log: function log(type, thing, title){
//     /*
//     1. TYPE affects waht it is. Is it error log? info log? General log?
//     2. Print out in console, be versatile
//     3. Print to appropriate file
//     4. THIS will minimise the amount of different fucking logs above
//     */

//     // DAMIAN PLEASE WRITE COMMENTS EXPLAINING WHAT HTE SHIT THIS SHIT DOES YOU DUMASS.
/**
 * This logs the ERROR thingies.
 * @param {*} Thing - The thing you wish to log.
 * @param {*} Title - The optional title.
 */
function error(Thing, Title){
    const date = new Date().toUTCString();
    if(!Title) Title = 'AN ERROR HAS OCCURED!';
    const consoleLog = `[ERROR]: ${date}\n${Title} = ${Thing}\n`;
    return console.error(consoleLog);
}
/**
 * This logs the INFO thingies.
 * @param {*} Thing - The thing you wish to log.
 * @param {*} Title - The optional title.
 */
function info(Thing, Title){
    const date = new Date().toUTCString();
    if(!Title) Title = 'General Info';
    const consoleLog = `[INFO]: \n${date}\n${Title} = ${Thing}\n`;
    return console.info(consoleLog);
}
/**
 * This logs the general log thingies.
 * @param {*} Thing - The thing you wish to log.
 * @param {*} Title - The optional title.
 */
function log(Thing, Title){
    const date = new Date().toUTCString();
    if(!Title) Title = 'General Log';
    const consoleLog = `[LOG]: \n${date}\n${Title} = ${Thing}\n`;
    return console.log(consoleLog);
}

// Working on making it with events instead.
const date = new Date().toUTCString();
class Logger extends EventEmitter {
    log(thing, title){
        if(!title) title = 'General Log';
        const consoleLog = `[LOG]: \n${date}\n${title} = ${thing}\n`;
        console.log(consoleLog);
    }
}

module.exports = { error, info, log };