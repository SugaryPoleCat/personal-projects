const logger = require('../../utils/logger');
const db = require('./ponyBasics.json');
const fs = require('fs');
const util = require('util');

let descriptionAge;
let descriptionHeight;
let Gender;
let Race = [];
let Sexuality;
let Homeland;
let Class;
let Technology;
const arrChars = [];

/**
 *this generates a description of the AGE of the character.
 *
 * @param {*} Age - The AGE parameter.
 * @returns The description phrase.
 */
function genDescriptionAge(Age){
    if(Age <= 11){
        return descriptionAge = 'filly';
    }
    else if(Age > 11 && Age <= 17){
        return descriptionAge = 'teen';
    }
    else if(Age > 17 && Age <= 25){
        return descriptionAge = 'young adult';
    }
    else if(Age > 25 && Age <= 35){
        return descriptionAge = 'adult';
    }
    else if(Age > 35 && Age <= 60){
        return descriptionAge = 'middle aged';
    }
    else if(Age > 60){
        return descriptionAge = 'elder';
    }
}

/**
 *this generates a description of the HEIGHT of the character.
 *
 * @param {*} Height - The HEIGHT parameter.
 * @returns The description phrase.
 */
function genDescriptionHeight(Height){
    if(Height <= 75){
        return descriptionHeight = 'stunted';
    }
    else if(Height > 75 && Height <= 85){
        return descriptionHeight = 'short';
    }
    else if(Height > 85 && Height <= 115){
        return descriptionHeight = 'average';
    }
    else if(Height > 115 && Height <= 140){
        return descriptionHeight = 'tall';
    }
    else if(Height > 140){
        return descriptionHeight = 'gigantic';
    }
}

function pickStuff(ayy, Array){
    const pickThing = Math.floor(Math.random() * Array.length);
    ayy.push(Array[pickThing]);
    console.log(Race[0].name);
}

function createChar(){
    const pickFirstName = Math.floor(Math.random() * db.firstName.length);
    const pickLastName = Math.floor(Math.random() * db.lastName.length);
    const pickGender = Math.floor(Math.random() * db.gender.length);

    // SO this solution doesnt work.
    pickStuff(Race, db.race);
    pickStuff(Class, db.class);
    pickStuff(Homeland, db.homeland);
    pickStuff(Sexuality, db.sexuality);
    pickStuff(Technology, db.technology);
    pickStuff(Gender, db.gender);

    const minHeight = db.height.min + db.gender[pickGender].general.minHeight;
    const maxHeight = db.height.max + db.gender[pickGender].general.maxHeight;
    const chHeight = Math.floor(Math.random() * (maxHeight - minHeight + 1)) + minHeight;
    const chAge = Math.floor(Math.random() * (db.age.max - db.age.min + 1)) + db.age.min;
    genDescriptionAge(chAge);
    genDescriptionHeight(chHeight);

    const newChar = {
        name: db.firstName[pickFirstName] + ' ' + db.lastName[pickLastName],
        race: Race.name,
        gender: Gender.name,
        class: Class.name,
        general: {
            age: chAge,
            height: chHeight,
            homeland: Homeland.name,
            technology: Technology.name,
        },
        stat: {
            healthPoints: Race.stat.healthPoints + Gender.stat.healthPoints + Class.stat.healthPoints,
            manaPoints: Race.stat.manaPoints + Gender.stat.manaPoints + Class.stat.manaPoints,
        },
        attribute: {
            strength: Race.attribute.strength + Gender.attribute.strength + Class.attribute.strength,
            intelligence: Race.attribute.intelligence + Gender.attribute.intelligence + Class.attribute.intelligence,
            agility: Race.attribute.agility + Gender.attribute.agility + Class.attribute.agility,
        },
        sexuality: {
            sexuality: Sexuality.name,
        },
    };
    arrChars.push(newChar);
    console.log(newChar);
}

function run(){
    try{
        console.log('Generating....');
        for(let x = 0; x < 1; x++){
            createChar();
        }
        console.log('Done!');

        if(!process.argv[2]) return console.log('Please provide which character you want to print, by providing a number.');
        else{
            const ch = arrChars[process.argv[2]];
            // console.log(ch);
            // console.log(`\n${ch.name} is a ${ch.general.age} year old, ${descriptionAge} ${ch.gender.name} ${ch.race.name} ${ch.class.name}.\nStanding at ${descriptionHeight} ${ch.general.height}cm, coming from ${ch.general.homeland.name} biome.\nSTATS: HP = ${ch.stat.healthPoints} | MP = ${ch.stat.manaPoints}\nATTRIBUTES: STRENGTH = ${ch.attribute.strength} | INTELLIGENCE = ${ch.attribute.intelligence} | AGILITY = ${ch.attribute.agility}\n\n`);
        }
    }
    catch(err){
        logger.log('ERROR', err);
    }
}
run();