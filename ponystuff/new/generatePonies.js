////
/// @page new/generatePonies
/// - This generates a new pony.
/// 
////

const fs = require('fs');
const logger = require('../../utils/logger');

const names = require('./db/names.json');

const generatedNames = [];

const traits = require('./db/traits.json');
const phys = require('./db/physical.json');
const defects = require('./db/defects.json');
const rareTraits = require('./db/rareTraits.json');
const livingCon = require('./db/livingCon.json');
const homelands = require('./db/homelands.json');
const sexualities = require('./db/sexualities.json');
const kinks = require('./db/kinks.json');
const sexPref = require('./db/sexPref.json');
const techs = require('./db/techs.json');
const races = require('./db/races.json');
const genders = require('./db/genders.json');

const pons = [];

function randomIndex(array){
    return array[Math.floor(Math.random() * array.length)];
}

function createPon(){
    let randomLastName, randomHomeland, newName;
    const randomFirstName = randomIndex(names['firstnames']);
    do{
        randomLastName = randomIndex(names['lastnames']);
    }while(randomLastName == randomFirstName);
    const randomRace = randomIndex(races);
    // do{
    //     randomHomeland = randomIndex(homelands);
    // }
    // while(randomRace != 'Pegasus' && randomHomeland == 'Cloudsdale');
    // do{
    //     newName = randomFirstName + ' ' + randomLastName;
    //     generatedNames.push(newName);
    // }while(!generatedNames.includes(newName));
    const newPon = {
        name: randomFirstName + ' ' + randomLastName,
        gender: randomIndex(genders).name,
        race: randomRace.name,
        // homeland: randomHomeland.name,
        homeland: randomIndex(homelands).name,
        livingCon: randomIndex(livingCon).name,
        physical: randomIndex(phys).name,
        traits: randomIndex(traits).name,
        defects: randomIndex(defects).name,
        rareTraits: randomIndex(rareTraits).name,
        techs: randomIndex(techs).name,
    };
    // logger.info(JSON.stringify(newPon), 'New pon created');
    logger.info(`New pone created!\n${newPon.name}, as a ${newPon.gender} ${newPon.race}, coming from ${newPon.homeland} with ${newPon.techs} technology level.\nTheir living conditions are: ${newPon.livingCon}.\nTheir physical: ${newPon.physical}, traits: ${newPon.traits}, defects: ${newPon.defects} and rare traits: ${newPon.rareTraits}`)
    pons.push(newPon);
}

function run(){
    try{
        for(let x = 0; x < 10; x++){
            createPon();
        }

        // fs.writeFile('./data/pons.json', JSON.stringify(pons), function(err){
        //     if(err) throw new Error(err);
        // });
    }
    catch(err){
        throw new Error(err);
    }
}
run();
// module.exports = {
//     run,
// };