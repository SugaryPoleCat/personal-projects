////
/// @page new/createThings
/// - This creates new files
///
////

const fs = require('fs');
// const logger = require('../../utils/logger');

const arrayTraits = [];
const arrayPhysical = [];
const arrayDefects = [];
const arrayRareTraits = [];
const arrayLivingConditios = [];
const arrayHomelands = [];
const arraySexualities = [];
const arrayKinks = [];
const arraySexPreferences = [];
const arrayTechnologies = [];
const arrayRaces = [];
const arrayGenders = [];

/**
 * - This creates a new thing, to be pushed into the right array.
 * @param {array} thing - Array to push the new thing into.
 * @param {string} name - The name of the new thing.
 * @param {string} description - A short description of what it does.
 */
function createThing(thing, name, description){
    const arrayCheck = [{
        what: name, name: 'name', type: 'string',
    }, {
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(arrayCheck);
    const newThing = {
        name: name, description: description,
    };
    thing.push(newThing);
}

function createName(name){
    const arrayCheck = [{
        what: name, name: 'name', type: 'string',
    }];
    checkStuff(arrayCheck);
    const newName = {
        firstnames: [
            name,
        ],
        lastnames: [
            name,
        ],
    };
}

/**
 * - This will check whether a type is the right type or not.
 * @param {array} thing - Array of OBJECT things to check.
 * @param {string} wFunction - This is the name of the function in which we are checking.
 */
function checkStuff(thing, wFunction){
    const keys = Object.keys(thing);
    for(const key of keys){
        const ding = thing[key];
        if(typeof ding.what !== ding.type) throw new Error(`${wFunction} = ${ding.name} is not a ${ding.type}!`);
        if(ding.type == 'number'){
            if(ding.what > 10 || ding.what < -10) throw new Error(`${wFunction} = ${ding.name} must be between -10 and 10!`);
        }
    }
}

/**
 * - This will write things to a file.
 * @param {string} name - The name of the file
 * @param {array} thing - The array of things to write.
 */
function writeToFile(name, thing){
    fs.writeFile(`db/${name}.json`, JSON.stringify(thing), function(err){
        if(err){
            throw new Error(err);
        }
    });
}

function run(){
    try{
        // the question is, is it even necessary?
        const cTraits = true;
        const cPhys = true;
        const cDefects = true;
        const cRareTraits = true;
        const cLivingCon = true;
        const cHomelands = true;
        const cSexualities = true;
        const cKinks = true;
        const cSexPref = true;
        const cTech = true;
        const cRaces = true;
        const cGenders = true;

        // CREATE THINGS //
        // traits
        if(cTraits == true){
        createThing(arrayTraits, 'Robust', 'Strong immune system');
        createThing(arrayTraits, 'Hoof-eye coordination', 'Heightened sense of balance');
        createThing(arrayTraits, 'Durable hooves', '');
        createThing(arrayTraits, 'Thicker Limbs', '');
        createThing(arrayTraits, 'Body Markings', '');
        createThing(arrayTraits, 'Heterochromia', '');
        createThing(arrayTraits, 'Longer limbs', '');
        createThing(arrayTraits, 'Thicker Coat', '');
        createThing(arrayTraits, 'Ambidextrous', '');
        createThing(arrayTraits, 'Double jointed', '');
        }

        // physical
        createThing(arrayPhysical, 'Cursed', '');
        createThing(arrayPhysical, 'Weak', '');
        createThing(arrayPhysical, 'Normal', '');
        createThing(arrayPhysical, 'Strong', '');
        createThing(arrayPhysical, 'Blessed', '');

        // defects
        createThing(arrayDefects, 'Mangy coat', '');
        createThing(arrayDefects, 'Frail', '');
        createThing(arrayDefects, 'Stunted', '');
        createThing(arrayDefects, 'Feral Tongue', '');
        createThing(arrayDefects, 'Weak magic', '');
        createThing(arrayDefects, 'Blind', '');
        createThing(arrayDefects, 'Deaf', '');
        createThing(arrayDefects, 'Nullification', '');
        createThing(arrayDefects, 'Thin limbs', '');
        createThing(arrayDefects, 'Albino', '');

        // rareTraits
        createThing(arrayRareTraits, 'Strong magic', '');
        createThing(arrayRareTraits, 'Extraordinary senses', '');
        createThing(arrayRareTraits, 'Physically pristine', '');
        createThing(arrayRareTraits, 'Able mind', '');
        createThing(arrayRareTraits, 'Genius', '');

        // livingCon
        createThing(arrayLivingConditios, 'Nomad', '');
        createThing(arrayLivingConditios, 'Exiled', '');
        createThing(arrayLivingConditios, 'Tribe', '');
        createThing(arrayLivingConditios, 'Civilization', '');
        createThing(arrayLivingConditios, 'Socially integrated', '');
        createThing(arrayLivingConditios, 'Enslaved', '');

        // homelands
        createThing(arrayHomelands, 'Arctic', 'tech');
        createThing(arrayHomelands, 'Arcane', '');
        createThing(arrayHomelands, 'Aquatic', '');
        createThing(arrayHomelands, 'Cavern', '');
        createThing(arrayHomelands, 'Coastal', '');
        createThing(arrayHomelands, 'Desert', '');
        createThing(arrayHomelands, 'Forest', '');
        createThing(arrayHomelands, 'Grassland', '');
        createThing(arrayHomelands, 'Islands', '');
        createThing(arrayHomelands, 'Jungle', '');
        createThing(arrayHomelands, 'Mountain', '');
        createThing(arrayHomelands, 'Ruins', '');
        createThing(arrayHomelands, 'Savannah', '');
        createThing(arrayHomelands, 'Swamp', '');
        createThing(arrayHomelands, 'Urban', '');
        createThing(arrayHomelands, 'Volcanic', '');

        // sexualities
        createThing(arraySexualities, 'Heterosexual', '');
        createThing(arraySexualities, 'Homosexual', '');
        createThing(arraySexualities, 'Pansexual', '');
        createThing(arraySexualities, 'Asexual', '');

        // kinks
        createThing(arrayKinks, 'Hoof fetish', '');
        createThing(arrayKinks, 'Costumes', '');
        createThing(arrayKinks, 'BDSM', '');

        // sexPref
        createThing(arraySexPreferences, 'Top', '');
        createThing(arraySexPreferences, 'Bottom', '');
        createThing(arraySexPreferences, 'Switch', '');
        createThing(arraySexPreferences, 'Powerbottom', '');

        // techs
        createThing(arrayTechnologies, 'Tribal', '');
        createThing(arrayTechnologies, 'Ancient', '');
        createThing(arrayTechnologies, 'Medieval', '');
        createThing(arrayTechnologies, 'Colonial', '');
        createThing(arrayTechnologies, 'Industrial', '');
        createThing(arrayTechnologies, 'Modern', '');
        createThing(arrayTechnologies, 'Futuristic', '');

        // races
        createThing(arrayRaces, 'Unicorn', 'A horned boi');
        createThing(arrayRaces, 'Pegasus', 'A winged boi');
        createThing(arrayRaces, 'Alicorn', 'A horned and winged boi');
        createThing(arrayRaces, 'Changeling', 'An evil boi');
        createThing(arrayRaces, 'Earth Pony', 'A walking boi');
        createThing(arrayRaces, 'Batpony', 'An eeeing boi');

        // genders
        createThing(arrayGenders, 'Mare', 'A female');
        createThing(arrayGenders, 'Stallion', 'A male');
        createThing(arrayGenders, 'Other', 'Some other gender');

        // WRITE TO FILE //
        writeToFile('traits', arrayTraits);
        writeToFile('physical', arrayPhysical);
        writeToFile('defects', arrayDefects);
        writeToFile('rareTraits', arrayRareTraits);
        writeToFile('livingCon', arrayLivingConditios);
        writeToFile('homelands', arrayHomelands);
        writeToFile('sexualities', arraySexualities);
        writeToFile('kinks', arrayKinks);
        writeToFile('sexPref', arraySexPreferences);
        writeToFile('techs', arrayTechnologies);
        writeToFile('races', arrayRaces);
        writeToFile('genders', arrayGenders);
    }
    catch(err){
        throw new Error(err);
    }
}
run();