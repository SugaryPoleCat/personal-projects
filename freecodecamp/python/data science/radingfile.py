file = open('stuff.txt')
for stuff in file:
	print(stuff)


print("")

f_hand = open('stuff2.txt')
for line in f_hand:
	line = line.rstrip()
	if not line.startswith('From:'):
		continue
	print(line)


print("")


fname = input('Enter filename: ')
try:
	fhand = open(fname)
except:
	print('File cannot be opened:', fname)
	quit()
count = 0
for line in fhand:
	if line.startswith('Subject'):
		count = count + 1
print('There were', count, 'subject lines in', fname)


