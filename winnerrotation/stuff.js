const participants = [
	{
		name: 'Daniel',
		count: 0,
		wins: 0,
	},
	{
		name: 'Damian',
		count: 0,
		wins: 0,
	},
	{
		name: 'David',
		count: 0,
		wins: 0,
	},
	{
		name: 'Sugar',
		count: 0,
		wins: 0,
	},
	{
		name: 'Sketch',
		count: 0,
		wins: 0,
	},
	{
		name: 'Serene',
		count: 0,
		wins: 0,
	},
	{
		name: 'Joel',
		count: 0,
		wins: 0,
	},
	{
		name: 'Caycay',
		count: 0,
		wins: 0,
	},
	{
		name: 'Luna',
		count: 0,
		wins: 0,
	},
	{
		name: 'Primrose',
		count: 0,
		wins: 0,
	},
	{
		name: 'Ayy',
		count: 0,
		wins: 0,
	},
	{
		name: 'Eyy',
		count: 0,
		wins: 0,
	},
];
const lastWinners = [];
let spinCount = 0,
	rawTimes = 10,
	winners = 1,
	times = participants.length * rawTimes;
$('document').ready(function() {
	$('#rawTimesInput').val(rawTimes);
	spinCount = 0;
	$('#spinCount').text(spinCount);
	lastWinners.length = 0;
	$('#participantNumber').text(participants.length);
	participants.map(function(p, n) {
		$('#arrayPosition').append(`<b>${p.name}</b> index: ${n}<br/>`);
	});
	$('#runAmount').text(times);
	$('#winnerAmount').text(winners);
	$('#winnersAmount').val(winners);
});
$('#rawTimesInput').change(async function() {
	let newTime = await parseInt($(this).val());
	const minTime = await parseInt($(this).prop('min'));
	if (newTime < minTime){
		newTime = await minTime;
	}
	rawTimes = await newTime;
	if($('#changeLoop').is(':checked')){
		times = await rawTimes;
	} else {
		times = await participants.length * rawTimes;
	}
	$('#runAmount').text(times);
});
$('#winnersAmount').change(async function() {
	let newWin = await parseInt($(this).val());
	const minWin = await parseInt($(this).prop('min')),
		maxWin = await parseInt($(this).prop('max'));
	if(newWin < minWin){
		newWin = await minWin;
	} else if(newWin > maxWin){
		newWin = await maxWin;
	}
	winners = await newWin;
	$('#winnerAmount').text(winners);
});
$('#addParticipant').click(function() {
	$('#arrayPosition').empty();
	const newP = {
		name: $('#participantName').val(),
		count: 0,
		wins: 0,
	};
	participants.push(newP);
	$('#participantNumber').text(participants.length);
	$('#participantName').val('');
	participants.map(function(p, n) {
		$('#arrayPosition').append(`<b>${p.name}</b> index: ${n}<br/>`);
	});
});
$('#winnerSpin').click(function() {
	spinCount++;
	$('#spinCount').text(spinCount);
	$('#winnerList').empty();
	$('#winnerWinList').empty();
	// times = participants.length * rawTimes;
	console.log('the loop will run: ', times);
	// THE LOOP
	if ($('#changeLoop').is(':checked')){
		for (let y = 0; y < rawTimes; y++) {
			let winner;
			winner = participants[Math.floor(Math.random() * participants.length)];
			winner.count++;
		}
	} else {
		winner = participants[Math.floor(Math.random() * participants.length)];
		winner.count++;
		// THIS LOOP FEELS UNFAIR, IS IT ALREADY GOES THROUGH, AS IF YOU PRSSED THE 'SPIN' BUTTON THIS MANY TIMES.
		// for (let y = 0; y < rawTimes; y++) {
		// 	for (let x = 0; x < times; x++) {
		// 		let winner;
		// 		winner = participants[Math.floor(Math.random() * participants.length)];
		// 		winner.count++;
		// 	}
		// }
	}

	// Now filtr by highest winners.
	// sort
	// participants.sort((a, b) => b.count - a.count)
	// 	.map(function(p, n) {
	// 		console.log(p);
	// 		if (n < winners){
	// 			$('#winnerList').append(`#${n + 1} winner is: ${p.name}<br/>`);
	// 			p.wins++;
	// 		}
	// 	});
	// THIS SHOWS ITS WORKING AS INTENDED.
	$('#winnerList').text(`Winner is: ${winner.name}!`);
	winner.wins++;
	participants.sort((a, b) => b.wins - a.wins)
		.map(function(p) {
			$('#winnerWinList').append(`<b>${p.name}</b> won: <b>${p.wins}</b> out of: <b>${spinCount}</b> | They lost: <b>${spinCount - p.wins}</b> out of <b>${spinCount}</b><br/>`);
		});
});
$('#changeLoop').change(async function() {
	if($(this).is(':checked')){
		times = rawTimes;
		$('#runAmount').text(times);
	} else {
		times = participants.length * rawTimes;
		$('#runAmount').text(times);
	}
});