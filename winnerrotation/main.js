const participants = [
	{
		name: 'Derg',
		count: 0,
	},
	{
		name: 'Derwerewrg',
		count: 0,
	},
	{
		name: 'Derwerg',
		count: 0,
	},
	{
		name: 'Derwerewwtwg',
		count: 0,
	},
	{
		name: 'Dererteg',
		count: 0,
	},
	{
		name: 'Derwerg',
		count: 0,
	},
	{
		name: 'sdf',
		count: 0,
	},
	{
		name: 'dfg',
		count: 0,
	},
	{
		name: 'fdg',
		count: 0,
	},
	{
		name: 'qwe',
		count: 0,
	},
	{
		name: 'asd',
		count: 0,
	},
];

// How many times to loop this
const rawTimes = 10,
	times = participants.length * rawTimes;
// THE LOOP
for (let y = 0; y < rawTimes; y++) {
	for (let x = 0; x < times; x++) {
		const winner = participants[Math.floor(Math.random() * participants.length)];
		winner.count++;
	}
}

console.log(participants);

// Now filtr by highest winners.
const winners = 1;
// participants.map(function(p) {
// 	console.log(p);
// });

// sort
participants.sort((a, b) => b.count - a.count)
	.map(function(p, n) {
		if (n < 5){
			console.log(`#${n + 1} winner is: ${p.name}`);
		}
	})
	.join('\n');