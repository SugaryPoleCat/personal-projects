const classes = require('./chardb.json');
const names = ['Damian', 'Aleks', 'Alex', 'Edward', 'Jonathan', 'Jakob'];

const chars1 = [];
const chars2 = [];

function randy(array){
    return array[Math.floor(Math.random() * array.length)];
}

function createChar(array, name){
    const getClass = randy(classes['classes']);
    const statClass = getClass.stats;
    const attrClass = getClass.attributes;
    const getRace = randy(classes['races']);
    const statRace = getRace.stats;
    const attrRace = getRace.attributes;
    let newName;
    if(!name || name == undefined){
        newName = randy(names);
    }
    else{
        newName = name;
    }
    const newChar = {
        name: newName,
        race: getRace.name,
        class: getClass.name,
        stats: {
            hp: statClass.hp + statRace.hp, mp: statRace.mp + statClass.mp,
        },
        attributes: {
            str: attrClass.str + attrRace.str, int: attrClass.int + attrRace.int, agi: attrClass.agi + attrRace.agi, end: attrClass.end + attrRace.end,
        },
    };
    array.push(newChar);
}

function displayData(array){
    const keys = Object.keys(array);
    for(const key of keys){
        const o = array[key];
        console.log(`${o.name} is a ${o.race} ${o.class}\nThey have: HP = ${o.stats.hp} | MP = ${o.stats.mp}\nThey have: STR = ${o.attributes.str} | INT = ${o.attributes.int} | AGI = ${o.attributes.agi} | END = ${o.attributes.end}\n`);
    }
}

function run(randomorno, inorder){
    if(randomorno == true){
        for(let x = 0; x < 10; x++){
            createChar(chars1);
        }
        displayData(chars1);
    }

    if(inorder == true){
        const nameKeys = Object.keys(names);
        for(const namekey of nameKeys){
            createChar(chars2, names[namekey]);
        }
        displayData(chars2);
    }
}
run(true, false);