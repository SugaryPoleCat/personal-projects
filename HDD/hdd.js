const words = require('./wordslol.json');

const hwords = words['hwords'];
const dwords = words['dwords'];

// This is for the write to file method.
const words2 = [];
// This is for the randomised words to be printed. I mean... its not necessary.
const words1 = [];

/**
 * This takes in a string and capitalises FIRST letter of the whole string.
 * @param {string} string - The string to capitalise
 */
function capitalize(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * - This will output a random word, form an array of words.
 * @param {array} array - this takes an array of words to randomise through
 */
function randy(array){
    return array[Math.floor(Math.random() * array.length)];
}

/**
 * - This is the old method. This randomises the word and also randomises again and again, as long as the word already exists, so that each HDD is unique.
 */
function newWord(){
    let newword, findName;
    do{
        newword = capitalize(randy(hwords)) + ' ' + capitalize(randy(dwords)) + ' ' + 'Dick';
        findName = words1.includes(newword);
    }while(findName);
    words1.push(newword);
}

/**
 * - This is the new method. This takes the entire array of Hwords and Dwords and loops through each one, to complete an entire wordlist, for each H word and D word.
 */
function newWord2(){
    const hkeys = Object.keys(hwords);
    const dkeys = Object.keys(dwords);
    for(const hkey of hkeys){
        for(const dkey of dkeys){
            const newword = capitalize(hwords[hkey]) + ' ' + capitalize(dwords[dkey]) + ' ' + 'Dick';
            words2.push(newword);
        }
    }
}

/**
 * - This runs the program
 * @param {boolean} countorno - Should it count how many words or no?
 * @param {boolean} randomiseorno - Should it randomise new words based on what we have in JSON?
 * @param {boolean} writeorno - Should it write out all possibilities or not?
 */
function run(countorno, randomiseorno, writeorno){
    try{
        if(countorno == true){
            // make like.... so it finds if words appear duplicate?
            console.log('amount of H words: ' + hwords.length);
            console.log('amount of D words: ' + dwords.length);
            console.log('Total amount of words to generate: ' + Math.floor(dwords.length * hwords.length));
        }

        // THIS WILL RANDOMISE STUFF
        if(randomiseorno == true){
            console.log('Generating words....');
            // this is how many times you want to generate a completely random HDD word.
            for(let x = 0; x < 10; x++){
                newWord();
            }
            // Show the randomised word.
            console.log(words1.join('\n'));
        }

        // THIS WILL WRITE TO A FILE ALL THE WORDS THAT CAN BE MADE FROM HDD
        if(writeorno == true){
            // Generate an entire wordlist.
            console.log('Generating words....');
            newWord2();

            // Write an entire .txt file of words.
            console.log('Writing to file....');
            const fs = require('fs');
            fs.writeFile('what.txt', words2.join(', '), function(err){
                if(err) throw new Error(err);
            });
            // WRITE AS JSON
            fs.writeFile('what.json', JSON.stringify(words2), function(err){
                if(err) throw new Error(err);
            });
        }

        console.log('DONE!');
    }
    catch(err){
        throw new Error(err);
    }
}

run(true, true, false);