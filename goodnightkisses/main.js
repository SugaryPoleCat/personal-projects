const firstKisses = [
    'wiggles',
    'little purrs',
    'moans',
    'blushing and purring',
    'purring and moaning',
],
    secondKisses = [
        'rubbing her chest',
        'rubbing crotchies',
        'rubbing legs with his',
        'rubbing legs with his while moaning',
        'rubbing boobs against him and blushing',
        'rubbing boobs against him and moaning while blushing',
    ],
    thirdKisses = [
        'legs rubbing over his',
        'groping his sides',
        'groping his hips',
        'extra rubs on his back',
        'rubs her wet crotch agasint his and moans',
    ],
    fourKisses = [
        'drools',
        'many drools',
        'too many drools',
        'dripping drools',
    ],
    deepKisses = [
        'tongue push into his mouth, which then wiggles around in his mouth and then pulls away and bites his lips with her fangs',
        'tongue pushing into his mouth, then wiggles it around in there and shares saliva and taste, then pulls away and bites his lips with her fangs'
    ],
    kissPhrases = [
        'deeply', 'once', 'twice', 'thrice', 'four times', 'five times', 'six times', 'seven times', 'eight times', 'nine times', 'ten times',
    ];

// const usedWords = []

let string = '';

function capitalise(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
}

function randomKiss(kissArray, muahAmount) {
    let muahs = '';
    if (muahAmount == 0) {
        muahs = 'mmmmuuuuuaaaaahhhh~';
    } else {
        for (let x = 0; x < muahAmount; x++) {
            muahs += 'muah ';
        }
    }
    const kiss = `**Kisses ${kissPhrases[muahAmount]}, with ${kissArray[Math.floor(Math.random() * kissArray.length)]}** ${capitalise(muahs)}\n`;
    string += kiss;
}

randomKiss(firstKisses, 1);
randomKiss(secondKisses, 2);
randomKiss(thirdKisses, 3);
randomKiss(fourKisses, 4);
randomKiss(deepKisses, 0);

console.log(string);