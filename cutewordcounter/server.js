const Discord = require('discord.js');
const conf = require('./conf');
const client = new Discord.Client();
let messages = 0;
let meows = 0;
let cuteness = 0;

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
	if (msg.content == 'meow' || msg.content == 'Meow') {
		meows += 1;
	}

	if (msg.content == 'show me cute') {
		msg.reply(`Your cuteness is: ${cuteness} | you used: ${meows}`);
	}
});

client.login(conf.token);