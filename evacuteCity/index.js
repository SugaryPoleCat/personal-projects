const path = require('path');
const fNames = require('./fNames.json');
const lNames = require('./lNames.json');

function l(item) {
	console.log(item);
}

function r(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}

function ra(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}

let total_time_to_exit_apt = 0,
total_time_to_exit_house = 0,
total_time_to_exit_city = 0;

(() => {
	try {
		const city = {
			total_time_to_exit_city: 0,
			houses: [],
		};
		const houses = r(1, 10);
		for (let x = 0; x < houses; x++) {
			let time_to_exit_city = r(20, 40);
			if (x === 0) {
				time_to_exit_city *= 1;
			} else {
				time_to_exit_city *= x;
			}
			const house = {
				time_to_exit_city: time_to_exit_city,
				floor: [],
			};
			city.houses.push(house);
			const floors = r(1, 7);
			for (let y = 0; y < floors; y++) {
				let time_to_go_down_floor = r(6, 12);
				if (y === 0) {
					time_to_go_down_floor *= 1;
				} else {
					time_to_go_down_floor *= y;
				}
				const floor = {
					time_to_go_down_floor: time_to_go_down_floor,
					apartament: [],
				};
				city.houses[x].floor.push(floor);
				const apartaments = r(1, 4);
				for (let z = 0; z < apartaments; z++) {
					const apartament = {
						// time_to_exit_apartament: r(5, 8),
						guests: [],
					};
					city.houses[x].floor[y].apartament.push(apartament);
					const people = r(1, 3);
					for (let k = 0; k < people; k++) {
						const guest = {
							time_to_exit_apt: r(5, 10),
							name: `${ra(fNames)} ${ra(lNames)}`,
						};
						city.houses[x].floor[y].apartament[z].guests.push(guest);
					}
				}
			}
		}

		l(city);
		l(city.houses[0]);
		l(city.houses[0].floor[0]);
		l(city.houses[0].floor[0].apartament[0]);
		l(city.houses[0].floor[0].apartament[0].guests);

		
		// l(JSON.stringify(city));
	} catch (e) {
		console.error(`[${new Date().toLocaleString('en-GB', { timezone: 'UTC' })}] ERROR\nIN: ${path.join(__dirname, __filename)}\n`, e);
		return;
	}
})();