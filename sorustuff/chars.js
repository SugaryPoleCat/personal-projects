const fs = require('fs');
const headmates = require('./headmates.json');
const soru = require('./soru.json');
const sugar = require('./sugar.json');

const chars = [];

chars.push(soru);
chars.push(sugar);
const keys = Object.keys(chars);
for(const key of keys){
    const c = chars[key];

    let log = `\n${c.name} is a ${c.species} ${c.race}.`;

    // THis gets the index, based on the number in the character.json file.
    log += '\nTheir headmates are: ';
    for(const hKey of c.headmates){
        const h = headmates[hKey];
        log += `${h.name} the ${h.species}, `;
    }

    const keys3 = Object.keys(c.favColours);
    log += '\nTheir favourite colours are: ';
    for(const key3 of keys3){
        log += `${c.favColours[key3]}, `;
    }

    const keys4 = Object.keys(c.stats);
    for(const key4 of keys4){
        log += `\nTheir stats are: ${c.stats[key4].name}: ${c.stats[key4].amount}`;
    }

    log += `\nThey have ${c.hooves} hooves`;
    log += `\nTheir mane is ${c.mane.style} ${c.mane.colour}`;
    log += `\nTheir tail is ${c.tail.style} ${c.tail.colour}`;
    log += `\nThey have ${c.floof} floof level.`;
    console.log(log);
}
fs.writeFile('sorustuff/chars.json', JSON.stringify(chars), function(err){
    if(err) throw new Error('error in fs\n' + err);
});
