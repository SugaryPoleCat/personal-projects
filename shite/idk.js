// try{
//     let classes = [];
//     let races = [];
//     let characters = [];
//     let genders = [];
//     /**
//      *This creates a new class
//     *
//     * @param {*} Name - The name of the race
//     * @param {*} Str - Strength
//     * @param {*} Int - Intelligence
//     * @param {*} Agi - Agility
//     * @returns A console log
//     */
//     // function createClass(Name, Str, Int, Agi){
//     //     const newClass = {
//     //         name: Name,
//     //         strength: Str,
//     //         intelligence: Int,
//     //         agility: Agi,
//     //     };
//     //     classes.push(newClass);
//     //     // return console.log(`New class created: ${newClass.name}, strength: ${newClass.strength}, intelligence: ${newClass.intelligence}, agility: ${newClass.agility}`);
//     // }
//     /**
//      *This creates a new gender
//     *
//     * @param {*} Name - The name of the race
//     * @param {*} Str - Strength
//     * @param {*} Int - Intelligence
//     * @param {*} Agi - Agility
//     * @returns A console log
//     */
//     // function createGender(Name, Str, Int, Agi){
//     //     const newGender = {
//     //         name: Name,
//     //         strength: Str,
//     //         intelligence: Int,
//     //         agility: Agi,
//     //     }
//     //     genders.push(newGender);
//     // }
//     /**
//      *This creates a new race
//     *
//     * @param {*} Name - The name of the race
//     * @param {*} Str - Strength
//     * @param {*} Int - Intelligence
//     * @param {*} Agi - Agility
//     * @returns A console log
//     */
//     // function createRace(Name, Str, Int, Agi){
//     //     const newRace = {
//     //         name: Name,
//     //         strength: Str,
//     //         intelligence: Int,
//     //         agility: Agi,
//     //     };
//     //     races.push(newRace);
//     //     // return console.log(`New race created: ${newRace.name}, strength: ${newRace.strength}, intelligence: ${newRace.intelligence}, agility: ${newRace.agility}`);
//     // }
//     /**
//      *This encompasses the different things to create, into one function.
//     *
//     * @param {*} Name - Name of the thing, like 'Wizard'.
//     * @param {*} Str - Amount of STRENGTH.
//     * @param {*} Int - Amount of INTELLIGENCE.
//     * @param {*} Agi - Amount of AGILITY.
//     * @param {*} What - What do you wish to add. You can choose: 'race', 'gender', 'class'.
//     * @returns Adds to the correct array the object.
//     */
//     function createStuff(Name, What, Str, Int, Agi){
//         const newStuff = {
//             name: Name,
//             strength: Str,
//             intelligence: Int,
//             agility: Agi,
//         };
//         switch(What){
//             case 'gender':
//                 return genders.push(newStuff);
//             case 'race':
//                 return races.push(newStuff);
//             case 'class':
//                 return classes.push(newStuff);
//         };
//     }
//     /**
//      *Create a new character
//     *
//     * @param {*} Name - Name of the character
//     * @param {*} Class - The class from the class array
//     * @param {*} Race - Race from the race class
//     * @returns A console log
//     */
//     function createChar(Name, Class, Race, Gender){
//         const newChar = {
//             name: Name,
//             race: Race,
//             class: Class,
//             totals: {
//                 strength: Class.strength + Race.strength + Gender.strength,
//                 intelligence: Class.intelligence + Race.intelligence + Gender.intelligence,
//                 agility: Class.agility + Race.agility + Gender.agility,
//             },
//         };
//         characters.push(newChar);
//         return console.log(`New character created: ${newChar.name}, race: ${newChar.race.name}, class: ${newChar.class.name}\nTotal strength: ${newChar.totals.strength}, total intelligence: ${newChar.totals.intelligence}, total agility: ${newChar.totals.agility}\n`);
//     }
//     // This will create teh different things.
//     // createClass('Warrior', 3, 1, 2);
//     // createClass('Wizard', 1, 3, 2);
//     // createClass('Rogue', 1, 2, 3);
//     // createRace('Human', 3, 2, 1);
//     // createRace('Elf', 1, 2, 3);
//     // createGender('Male', 3, 2, 1);
//     // createGender('Female', 1, 2, 3);
//     // This is the same thing as above.
//     createStuff('Warrior', 'class', 3, 1, 2);
//     createStuff('Wizard', 'class', 1, 3, 2);
//     createStuff('Rogue', 'class', 1, 2, 3);
//     createStuff('Human', 'race', 3, 2, 1);
//     createStuff('Elf', 'race', 1, 2, 3);
//     createStuff('Male', 'gender', 3, 2, 1);
//     createStuff('Female', 'gender', 1, 2, 3);
//     createChar('Damian', classes[2], races[0], genders[1]);
//     createChar('Joel', classes[1], races[1], genders[0]);
//     createChar('Daniel', classes[0], races[0], genders[1]);
//     console.log('Lets see: ' + classes.find(n => n.name == 'Wizard'));
//     const faggot = classes.find(n => n.name == 'Wizard');
//     console.log(faggot.name + ' ' + faggot.strength);
//     console.log('lets see #2: ' + classes.find(n => n.name == 'Rogue').name);
//     // This prints an object as string.
//     // Object is essentially a JSON thing.
//     console.log(JSON.stringify(classes.find(n => n.name == 'Warrior')));
// }
// catch(err){
//     console.log('error: ' + err);
// }

try{
    // Soru method for josn:
    // {
    //     "race": {
    //         "alicorn": {
    //             "name": "bitch"
    //         },
    //         "pegasus": {
    //             "nmae": "nigg"
    //         }
    //     },
    //     "gender": {
    //         "aaa": "aa"
    //     }
    // }
    // const object = {
    //     "a": 42,
    //     " b": 1336
    // };
    // const keys = Object.keys(object);
    // for (const key of keys) {
    //     const o = object[key];
    //     console.log(o);
    // }

    let classes = [];
    let races = [];
    let genders = [];
    let names = ['Damian', 'Joel', 'Daniel', 'Katrin'];

    const idk = require('./ohman.json');

    // console.log('Json stringify: ' + JSON.stringify(idk));
    // console.log('\nidk.class[0]');
    // console.log(idk.class[0]);
    // console.log(`\nidk['class'][1]`);
    // console.log(idk['class'][1]);
    // console.log(`\nidk['class'][1]['stat'][2]`);
    // console.log(idk['class'][1]['stat'][2]);
    function addThing(What, Array){
        // for(let x = 0; x < Array.length; x++){
        //     What.push(Array[x]);
        // }
        for(const a of Array){
            What.push(a);
            console.log('Added: ' + JSON.stringify(a));
        }
    }
    addThing(classes, idk.class);
    // Works, but you know. Consistency.
    // addThing(races, idk['race']);
    addThing(races, idk.race);
    addThing(genders, idk.gender);
    // console.log(classes);
    // console.log(classes[1].stat[1]);
    // console.log(classes[0].stat[0].name + ' ' + classes[0].stat[0].amount);

    function createChar(Name, ClassPick, RacePick, GenderPick){
        const minHeight = RacePick.stat[2].minHeight + GenderPick.stat[2].minHeight;
        const maxHeight = RacePick.stat[2].maxHeight + GenderPick.stat[2].maxHeight;
        const newChar = {
            name: Name,
            gender: GenderPick.name,
            race: RacePick.name,
            class: ClassPick.name,
            stat: {
                healthPoints: ClassPick.stat[0].amount + RacePick.stat[0].amount + GenderPick.stat[0].amount,
                manaPoints: ClassPick.stat[1].amount + RacePick.stat[1].amount + GenderPick.stat[1].amount,
                height: Math.floor(Math.random() * (maxHeight - minHeight + 1)) + minHeight
            },
            attribute: {
                strength: ClassPick.attribute[0].amount + RacePick.attribute[0].amount + GenderPick.attribute[0].amount,
                intelligence: ClassPick.attribute[1].amount + RacePick.attribute[1].amount + GenderPick.attribute[1].amount,
                agility: ClassPick.attribute[2].amount + RacePick.attribute[2].amount + GenderPick.attribute[2].amount
            }
        };
        // This isnt working properly
        // console.log(`New characater created: ${newChar.name} as a ${newChar.gender} ${newChar.race} ${newChar['class']}, with:\nHealth points: ${newChar.stat[0]}, Mana Points: ${newChar['stat'][1]}\nStrength: ${newChar.attribute.strength}, Intelligence: ${newChar['attribute'][1]}, Agility: ${newChar.attribute[2]}`);
        // This one however does. Even tho the one above SHOULD work, since [''] is an object.. it's weird.
        console.log(`New characater created: ${newChar.name} as a ${newChar.gender} ${newChar.race} ${newChar.class}, with:\nHealth points: ${newChar.stat.healthPoints}, Mana Points: ${newChar.stat.manaPoints}, Height: ${newChar.stat.height}\nStrength: ${newChar.attribute.strength}, Intelligence: ${newChar.attribute.intelligence}, Agility: ${newChar.attribute.agility}\n`);
    }
    createChar('Damian', classes[2], races[1], genders[1]);
    // for(let x = 0; x < names.length; x++){
    //     const pickClass = Math.floor(Math.random() * classes.length);
    //     const pickRace = Math.floor(Math.random() * races.length);
    //     const pickGender = Math.floor(Math.random() * genders.length);
    //     createChar(names[x], classes[pickClass], races[pickRace], genders[pickGender]);
    // }
    for(const x of names){
        const pickClass = Math.floor(Math.random() * classes.length);
        const pickRace = Math.floor(Math.random() * races.length);
        const pickGender = Math.floor(Math.random() * genders.length);
        createChar(x, classes[pickClass], races[pickRace], genders[pickGender]);
    }
}
catch(err){
    console.log('error: ' + err);
}