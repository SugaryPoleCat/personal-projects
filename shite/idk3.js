const logger = require('../utils/logger');
const fs = require('fs');

const homelandFirstNames = [];
const homelandMiddleNames = [];
const homelandLastNames = [];
const charFirstNames = [];
const charMiddleNames = [];
const charLastNames = [];
const locationFirstNames = [];
const locationMiddleNames = [];
const locationLastNames = [];

const homelands = [];
const technologies = [];
const biomes = [];
const locations = [];
const locationTypes = [];
const characters = [];

/**
 * - This creates a random name based on a certain template.
 * @param {string} what - This checks what do you wish to add. You can select between 'HOMELAND', 'CHARACTER' and 'LOCATION'.
 * @param {array} array - This is the ARRAY to wish you wish to push to.
 */
function nameGenerator(what, array){
    const letters = 'abcdefghijklmnopqrstuvwxyz';
    const checkArray = [{
        what: what, name: 'what', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    let middleLetters;
    switch(what){
        case 'homeland':
            middleLetters = 'oe';
            break;
        case 'character':
            middleLetters = 'a';
            break;
        case 'location':
            middleLetters = 'lr';
            break;
        // NO SORU NO DEFAULT CAUSE I FORGOT
        default:
            throw new Error('nameGenerator = what is an INVALID string!');
    }
    let firstLetter, middleLetter, lastLetter, newName, findName;
    do{
        firstLetter = randomIndex(letters);
        middleLetter = randomIndex(middleLetters);
        do{
            lastLetter = randomIndex(letters);
        }while(lastLetter == firstLetter);
        newName = firstLetter.toUpperCase() + middleLetter + lastLetter;
        findName = array.includes(newName);
    }while(findName);
    array.push(newName);
}

/**
 * - This gets a random index from the provided array. Returns the index number.
 * @param {array} array - This is the array for what to index.
 * @returns Returns the ACTUAL index number.
 */
function randomIndex(array){
    return array[Math.floor(Math.random() * array.length)];
}

/**
 * - This creates a new homeland.
 * @param {string} description - Short description of the homeland.
 */
function createHomeland(description){
    const biome = randomIndex(biomes);
    const technology = randomIndex(technologies);
    const checkArray = [{
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    const newHomeland = {
        name: `${randomIndex(homelandFirstNames)}'${randomIndex(homelandMiddleNames)}-${randomIndex(homelandLastNames)}`,
        biome: biome.name,
        technology: technology.name,
        description: description + '\n' + biome.description + '\n' + technology.description,
    };
    logger.info(newHomeland.name, 'New Homeland created!');
    homelands.push(newHomeland);
}

/**
 * - This creates a new location type, like a Village or a City.
 * @param {string} name - THe name of the location type, which also notes what type it is.
 * @param {string} description - The description of what kind of location type it is.
 */
function createLocationType(name, description){
    const checkArray = [{
        what: name, name: 'name', type: 'string',
    }, {
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    const newLocationType = {
        name: name,
        description: description,
    };
    logger.info(newLocationType.name, 'New location type created!');
    locationTypes.push(newLocationType);
}

/**
 * - This creates a new location. Everything else is randomised, but the description (for now) is still used for the umm... well description.
 * @param {string} description - Description of the location.
 * @returns Pushes to the array the new location.
 */
function createLocation(description){
    const homeland = randomIndex(homelands);
    const type = randomIndex(locationTypes);
    const checkArray = [{
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    const newLocation = {
        name: `${randomIndex(locationFirstNames)}'${randomIndex(locationMiddleNames)}-${randomIndex(locationLastNames)}`,
        homeland: homeland.name,
        description: description,
        type: type.name,
    };
    logger.info(newLocation.name, 'New location created!');
    locations.push(newLocation);
}

/**
 * - This creates a new biome type for the creation of the homeland.
 * @param {string} name - The name of the biome.
 * @param {string} description - The short description of the biome.
 * @returns Pushes a new biome into the array list.
 */
function createBiome(name, description){
    const checkArray = [{
        what: name, name: 'name', type: 'string',
    }, {
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    const newBiome = {
        name: name,
        description: description,
    };
    logger.info(newBiome.name, 'New Biome created!');
    biomes.push(newBiome);
}

/**
 * - This will check whether a type is the right type or not.
 * @param {array} thing - Array of OBJECT things to check.
 * @param {string} wFunction - This is the name of the function in which we are checking.
 */
function checkStuff(thing, wFunction){
    const keys = Object.keys(thing);
    for(const key of keys){
        const ding = thing[key];
        if(typeof ding.what !== ding.type) throw new Error(`${wFunction} = ${ding.name} is not a ${ding.type}!`);
        if(ding.type == 'number'){
            if(ding.what > 10 || ding.what < -10) throw new Error(`${wFunction} = ${ding.name} must be between -10 and 10!`);
        }
    }
}
/**
 * - This creates a new technology level to use in stuff.
 * @param {string} name - Name of the technology.
 * @param {number} level - The number of the level. MUST BE FROM 1-10.
 * @param {number} hp - This is the amount of health points. Must be between -10 and 10. Together with mp and sp they can't get over -10 or 10.
 * @param {number} mp - This is the amount of mana points. Must be between -10 and 10. Together with mp and sp they can't get over -10 or 10.
 * @param {number} sp - This is the amount of stamina points. Must be between -10 and 10. Together with mp and sp they can't get over -10 or 10.
 * @param {number} str - This is the amount of strength. Must be between -10 and 10. Together with agi, int and end they can't get over -10 or 10.
 * @param {number} agi - This is the amount of agility. Must be between -10 and 10. Together with str, int and end they can't get over -10 or 10.
 * @param {number} int - This is the amount of intelligence. Must be between -10 and 10. Together with str, agi and end they can't get over -10 or 10.
 * @param {number} end - This is the amount of endurance. Must be between -10 and 10. Together with str, agi and int they can't get over -10 or 10.
 * @param {string} description - A short description of what this technology level is.
 * @returns This pushes a new tech into the array to be later used for the JSON file.
 */
function createTech(name, level, hp, mp, sp, str, agi, int, end, description){
    const checkArray = [{
        what: name, name: 'name', type: 'string',
    }, {
        what: level, name: 'level', type: 'number',
    }, {
        what: hp, name: 'hp', type: 'number',
    }, {
        what: mp, name: 'mp', type: 'number',
    }, {
        what: sp, name: 'sp', type: 'number',
    }, {
        what: str, name: 'str', type: 'number',
    }, {
        what: agi, name: 'agi', type: 'number',
    }, {
        what: int, name: 'int', type: 'number',
    }, {
        what: end, name: 'end', type: 'number',
    }, {
        what: description, name: 'description', type: 'string',
    }];
    checkStuff(checkArray, 'createTech');
    const statsTotal = hp + mp + sp;
    const attriTotal = str + agi + int + end;
    if(statsTotal > 10 || statsTotal < -10) throw new Error(`createTech = ${name} = stats together must be between -10 and 10!`);
    if(attriTotal > 10 || attriTotal < -10) throw new Error(`createTech = ${name} = attributes together must be between -10 and 10!`);
    const newTech = {
        name: name,
        description: description,
        level: level,
        stats: {
            hp: hp, mp: mp, sp: sp,
        },
        attributes: {
            str: str, agi: agi, int: int, end: end,
        },
    };
    logger.info(newTech.name, 'New technology created!');
    technologies.push(newTech);
}

/**
 * - This creates a new character based on the things we already created, should be called last. Until i do some functionality to make it callable.
 */
function createCharacter(){
    const newChar = {
        name: `${randomIndex(charFirstNames)}'${randomIndex(charMiddleNames)}-${randomIndex(charLastNames)}`,
        homeland: randomIndex(homelands).name,
        location: randomIndex(locations).name,
    };
    logger.info(newChar.name, 'New character created!');
    characters.push(newChar);
}

/**
 * - This will write things to a file.
 * @param {string} name - The name of the file
 * @param {array} thing - The array of things to write.
 */
function writeToFile(name, thing){
    fs.writeFile(`shite/idk3data/${name}.json`, JSON.stringify(thing), function(err){
        if(err){
            throw new Error(err);
        }
    });
}

/**
 * - This runs the program.
 */
function run(){
    try{
        // CREATE NEW TECH
        createTech('Primitive', 1, 1, 1, 1, 1, 1, 1, 1, 'A wheel, is unheard for these people. They may know how to make a fire, but anything more than that is far beyond them.');
        createTech('Tribal', 2, 1, 1, 1, 1, 1, 1, 1, 'Fights with sticks and stones, old and primitive. Things newer than a wheel, are unkown to them.');
        createTech('Ancient', 3, 1, 1, 1, 1, 1, 1, 1, 'Uhh... old?');
        createTech('Medieval', 4, 1, 1, 1, 1, 1, 1, 1, 'Witches, swords, catapults are all known to these people. They might be a little naive, but most likely, theyknow how to fight in armed and unarmed melee combat.');
        createTech('Industrial', 5, 1, 1, 1, 1, 1, 1, 1, 'Age of machines and steam and air pollution.');
        createTech('Modern', 6, 1, 1, 1, 1, 1, 1, 1, 'The current level of technology. Things like cellphones and computers, radios, cars, are most likely known and researched.');

        // CREATE THE BIOMES
        createBiome('Swamp', 'Damp and moist, watery ground, grass hiding the deep water pool, vines growning from the trees. Can seem a bit like a jungle, but isn\'t.');
        createBiome('Urban', 'Cities, skyscrapers, slums.');

        // GENERATE NAMES
        for(let x = 0; x < 20; x++){
            nameGenerator('homeland', homelandFirstNames);
            nameGenerator('homeland', homelandMiddleNames);
            nameGenerator('homeland', homelandLastNames);
            nameGenerator('location', locationFirstNames);
            nameGenerator('location', locationMiddleNames);
            nameGenerator('location', locationLastNames);
            nameGenerator('character', charFirstNames);
            nameGenerator('character', charMiddleNames);
            nameGenerator('character', charLastNames);
        }

        // GENERATE LOCATION TYPES
        createLocationType('City', 'A city thingy');
        createLocationType('Village', 'A village size location');

        // CREATE NEW HOMELAND
        for(let x = 0; x < 10; x++){
            createHomeland('I am gay');
            createLocation('I am not gay!');
        }

        // THIS IS TO TEST CAHRACTER CREATION
        for(let x = 0; x < 10; x++){
            createCharacter();
        }

        // SORU CAN YOU MAKE IT BETTER
        writeToFile('technologies', technologies);
        writeToFile('homelands', homelands);
        writeToFile('locations', locations);
        writeToFile('locationTypes', locationTypes);
        writeToFile('biomes', biomes);
        writeToFile('characters', characters);
    }
    catch(err){
        logger.error(err);
    }
}

run();

// A TEST THING
// module.exports.init = function () {
//     console.log('hi');
//   };

module.exports = {
    createBiome, createCharacter, createHomeland, createLocation, createLocationType, createTech, nameGenerator, randomIndex, run, checkStuff,
};