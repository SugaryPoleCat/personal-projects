const logger = require('../utils/logger');
// const utils = require('../utils/utils');
const fs = require('fs');
// const util = require('util');

let createdCharacters, createdJobs, createdRaces;

const jobs = [];
const races = [];
const homelands = [];
const characters = [];

function checkNumber(Num, What){
    if(typeof Num === 'number') return Num;
    else return logger.log('ERROR', `${What} is not a number.`);
}

/**
 * This creates a new JOB for the characters.
 * @param {string} Arrray - IDK.
 * @param {string} Name - NAME of the job. Must be STRING.
 * @param {string} Description - Detailed DESCRIPTION. Must be STRING.
 * @param {number} HP - Amount of HEALTH. Must be INTEGER.
 * @param {number} MP - Amount of MANA. Must be INTEGER.
 * @param {number} STR - Amount of STRENGTH. Must be INTEGER.
 * @param {number} INT - Amount of INTELLIGENCE. Must be INTEGER.
 * @param {number} AGI - Amount of AGILITY. Must be INTEGER.
 * @param {number} END - Amount of ENDURANCE. Must be INTEGER.
 * @returns {Object} newJob.
 */
function createJobRace(what, Name, Description, HP, MP, STR, INT, AGI, END){
    try{
        const attributesTotal = AGI + INT + STR + END;
        const statsTotal = HP + MP;
        if(attributesTotal > 10){
            return logger.log('', `Attributes for '${Name}' exceed 10 total.`);
        }
        else{
            if(statsTotal > 10){
                return logger.log('', `Stats for '${Name}' exceed 10 total.`);
            }
            else{
                const newJobRace = {
                    name: Name,
                    description: Description,
                    stats: {
                        healthPoints: HP,
                        manaPoints: MP,
                    },
                    attributes: {
                        strength: STR,
                        intelligence: INT,
                        agility: AGI,
                        endurance: END,
                    },
                };
                if(what == 'job'){
                    jobs.push(newJobRace);
                    logger.log('', 'New job created.');
                }
                else if(what == 'race'){
                    races.push(newJobRace);
                    logger.log('', 'New race created.');
                }
            }
        }
    }
    catch(err){
        logger.log('ERROR', err);
    }
}

/**
 * This creates a new character, using data provided from the data around here.
 * @param {string} Name - Name of the character. Must be STRING.
 * @param {number} Age - Age in numbers.
 * @param {number} Height - Height in CM.
 * @param {string} Homeland - Where they come from. Must be string.
 * @param {number} HP - Amount of healthpoints.
 * @param {number} MP - Amount of manapoints.
 * @param {number} STR - Amount of strength.
 * @param {number} INT - Amount of intelligence.
 * @param {number} AGI  - Amount of agility.
 * @param {number} END - Amount of endurance.
 */
function createCharacter(Name, Age, Height, Homeland, HP, MP, STR, INT, AGI, END){
    try{
        const newCharacter = {
            name: Name,
            general: {
                age: Age,
                height: Height,
                homeland: Homeland,
            },
            stats: {
                healthPoints: HP,
                manaPoints: MP,
            },
            attributes: {
                strength: STR,
                intelligence: INT,
                agility: AGI,
                endurance: END,
            },
        };
        characters.push(newCharacter);
        createdCharacters = true;
        logger.log('', 'New character created.');
    }
    catch(err){
        logger.log('ERROR', err);
    }
}
function run(){
    try{
        const TEST = 'new';
        if(process.argv[2] == 'new' || TEST == 'new'){
            logger.log('', 'Creating new jobs...');
            createJobRace('job', 'Warrior', 'is a buff dude', 8, 2, 5, 1, 1, 2);
            createJobRace('job', 'Wizard', 'is a smart dude', 2, 8, 2, 5, 2, 1);
            createJobRace('job', 'Rogue', 'is a sneaky dude', 6, 4, 2, 3, 4, 1);
            fs.writeFile('shite/jobs.json', JSON.stringify(jobs), function(err){
                if(err){
                    logger.log('ERROR', err);
                }
            });
            logger.log('', 'Created new jobs.');

            logger.log('', 'Creating new races...');
            createJobRace('race', 'North', 'northern people are full of strong bodies', 7, 3, 4, 1, 2, 3);
            createJobRace('race', 'South', 'southern people are full of graceful bodies and potent magic', 6, 4, 1, 3, 4, 2);
            fs.writeFile('shite/races.json', JSON.stringify(races), function(err){
                if(err){
                    logger.log('ERROR', err);
                }
            });
            logger.log('', 'Created new races.');


        }
        else if(process.argv[2] == 'get'){
            const dataJobs = require('./characters.json');
            const keysJobs = Object.keys(dataJobs);
            for(const key of keysJobs){
                const o = dataJobs[key];
                logger.log('', `${o.name} is a ${o.description}.\nSTATS: HP = ${o.stats.healthPoints} | MP = ${o.stats.manaPoints}\nATTRIBUTES: STR = ${o.attributes.strength} | INT = ${o.attributes.intelligence} | AGI = ${o.attributes.agility} | END = ${o.attributes.endurance}\n`);
            }
        }
    }
    catch(err){
        logger.log('ERROR', err);
    }
}
run();