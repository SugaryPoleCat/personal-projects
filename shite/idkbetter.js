const idk = require('./ohman.json');
const logger = require('../utils/logger');
// const utils = require('../utils/utils');
const fs = require('fs');

const classes = [];
const races = [];
const genders = [];
const firstNames = [];
const middleNames = ['no'];
const lastNames = [];
const homelands = [];

const characters = [];

function randomiseFirstName(Array){
    const letters = 'abcdefghijklmnopqrstuvwxyzåäöąćżźńłóęś';
    let firstLetter, middleLetter, lastLetter, newName, findName;
    do{
        firstLetter = letters[Math.floor(Math.random() * letters.length)];
        middleLetter = 'o';
        lastLetter = letters[Math.floor(Math.random() * letters.length)];
        while(firstLetter == lastLetter){
            lastLetter = letters[Math.floor(Math.random() * letters.length)];
        }
        newName = firstLetter.toUpperCase() + middleLetter + lastLetter;
        findName = Array.includes(newName);
    }while(findName);
    Array.push(newName);
}

function addThing(What, Array){
    for(const a of Array){
        What.push(a);
    }
}
function createChar(){
    const Class = classes[Math.floor(Math.random() * classes.length)];
    const Gender = genders[Math.floor(Math.random() * genders.length)];
    const Race = races[Math.floor(Math.random() * races.length)];
    const Homeland = homelands[Math.floor(Math.random() * homelands.length)];
    const FirstName = firstNames[Math.floor(Math.random() * firstNames.length)];
    const Middlename = middleNames[Math.floor(Math.random() * middleNames.length)];
    const LastName = lastNames[Math.floor(Math.random() * lastNames.length)];

    const minHeight = Race.stat[2].minHeight + Gender.stat[2].minHeight;
    const maxHeight = Race.stat[2].maxHeight + Gender.stat[2].maxHeight;

    const newChar = {
        name: FirstName + '\'' + Middlename + '-' + LastName,
        gender: Gender.name,
        race: Race.name,
        class: Class.name,
        homeland: Homeland,
        stat: {
            healthPoints: Class.stat[0].amount + Race.stat[0].amount + Gender.stat[0].amount + Homeland.stat[0].amount,
            manaPoints: Class.stat[1].amount + Race.stat[1].amount + Gender.stat[1].amount + Homeland.stat[1].amount,
            height: Math.floor(Math.random() * (maxHeight - minHeight + 1)) + minHeight,
        },
        attribute: {
            strength: Class.attribute[0].amount + Race.attribute[0].amount + Gender.attribute[0].amount + Homeland.attribute[0].amount,
            intelligence: Class.attribute[1].amount + Race.attribute[1].amount + Gender.attribute[1].amount + Homeland.attribute[1].amount,
            agility: Class.attribute[2].amount + Race.attribute[2].amount + Gender.attribute[2].amount + Homeland.attribute[2].amount,
        },
    };
    characters.push(newChar);
}
try{
    if(process.argv[2] == 'new'){
        logger.log('', 'Generating things...');
        addThing(classes, idk.class);
        addThing(races, idk.race);
        addThing(genders, idk.gender);
        addThing(homelands, idk.homeland);
        logger.log('', 'Done generating things!');

        logger.log('', 'Generating names...');
        for(let y = 0; y < 1000; y++){
            randomiseFirstName(firstNames);
            randomiseFirstName(lastNames);
        }
        logger.log('', 'Done generating names!');

        logger.log('', 'Generating characters...');
        for(let x = 0; x < 100; x++){
            createChar(x);
        }
        logger.log('', 'Done generating characters!');
        fs.writeFile('data.json', JSON.stringify(characters), function(err){
            if(err){
                logger.log('ERROR', err);
            }
        });
    }
    else if(process.argv[2] == 'get'){
        const data = require('../data.json');
        const keys = Object.keys(data);
        for(const key of keys){
            const o = data[key];
            logger.log('', `${o.name} is a ${o.stat.height}cm tall ${o.gender} ${o.race} ${o.class}.\nHailing from ${o.homeland.name} a ${o.homeland.type} biome, ${o.homeland.description}, which gives them ${o.homeland.trait}.\nSTATS: HP = ${o.stat.healthPoints} | MP = ${o.stat.manaPoints}\nATTRIBUTES: STR = ${o.attribute.strength} | INT = ${o.attribute.intelligence} | AGI = ${o.attribute.agility}\n`);
        }
    }
    else{
        logger.log('', 'Please tell me what to do', 'HEY');
    }
}
catch(err){
    console.log('error: ' + err);
}