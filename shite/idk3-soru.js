const logger = require('../utils/logger');
const fs = require('fs');

const letters = 'abcdefghijklmnopqrstuvwxyz';
const homelandFirstnames = [];
const homelandMiddlenames = [];
const homelandLastnames = [];
const charFirstnames = [];
const charMiddlenames = [];
const charLastnames = [];
const locationFirstnames = [];
const locationMiddlenames = [];
const locationLastnames = [];

const homelands = [];
const technologies = [];
const biomes = [];
const locations = [];
const locationtypes = [];
const characters = [];

/**
 * This creates a random name based on a certain template.
 * @param {string} what - This checks what do you wish to add. You can select between 'HOMELAND', 'CHARACTER' and 'LOCATION'.
 * @param {array} array - The array to append to
 */
function nameGenerator(what, array){ // lowercase variable name, append to string later
    // remove try...catch to let errors propagate outwards
    if(typeof what !== 'string'){
        throw new Error('nameGenerator = what is not a STRING!'); // throw error when things aren't working
    }
    let middleLetters;
    switch(what){
        case 'homeland':
            middleLetters = 'oe';
            break;
        case 'character':
            middleLetters = 'a';
            break;
        case 'location':
            middleLetters = 'lr';
            break;
        // no default case?
        default:
            throw new Error('nameGenerator = what is an invalid string');
    }
    let firstLetter, middleLetter, lastLetter, newname, findname;
    do{
        firstLetter = letters[randomIndex(letters)];
        middleLetter = middleLetters[randomIndex(middleLetters)];
        do{ // do...while to save a line of code
            lastLetter = letters[randomIndex(letters)];
        }while(firstLetter == lastLetter);
        newname = firstLetter.toUpperCase() + middleLetter + lastLetter;
    }while(array.includes(newname));
    // logger.info(newname, 'New name created for ' + What + '!');
    array.push(newname);
}

/**
 * This gets a random index from the provided array. Returns the index number.
 * @param {array} Arrray - This is the array for what to index.
 * @returns Returns the ACTUAL index number.
 */
function randomIndex(arrray){
    return arrray[Math.floor(Math.random() * arrray.length)];
}

/**
 * This creates a new homeland.
 * @param {string} description - Short description of the homeland.
 * @returns Pushes a new homeland into the array to be used in JSON stringfy.
 */
function createhomelang(description){
    const biome = randomIndex(biomes);
    const technology = randomIndex(technologies);
    if(typeof description !== 'string'){
        return logger.error(description, 'createhomelang = description is not a STRING!');
    }
    if(typeof biome !== 'object'){
        return logger.error(biome, 'createhomelang = Biome is not a OBJECT!');
    }
    if(typeof technology !== 'object'){
        return logger.error(technology, 'createhomelang = Technology is not a OBJECT!');
    }
    const newhomelang = {
        name: `${randomIndex(homelandFirstnames)}'${randomIndex(homelandMiddlenames)}-${randomIndex(homelandLastnames)}`,
        biome: biome.name,
        technology: technology.name,
        description: description + '\n' + biome.description + '\n' + technology.description,
    };
    logger.info(newhomelang.name, 'New homelang created!');
    homelands.push(newhomelang);
}

/**
 * This creates a new location type, like a Village or a City.
 * @param {string} name - THe name of the location type, which also notes what type it is.
 * @param {string} description - The description of what kind of location type it is.
 * @returns Pushes to the array the locationtype.
 */
function createLocationtype(name, description){
    try{
        if(typeof name !== 'string'){
            return logger.error(name, 'createLocationtype = name is not a STRING!');
        }
        if(typeof description !== 'string'){
            return logger.error(description, 'createLocationtype = description is not a STRING!');
        }
        const newLocationtype = {
            name: name,
            description: description,
        };
        logger.info(newLocationtype.name, 'New location type created!');
        return locationtypes.push(newLocationtype);
    }
    catch(err){
        return logger.error(err);
    }
}

/**
 * This creates a new location. Everything else is randomised, but the description (for now) is still used for the umm... well description.
 * @param {string} description - description of the location.
 * @returns Pushes to the array the new location.
 */
function createLocation(description){
    try{
        const homelang = randomIndex(homelands);
        const type = randomIndex(locationtypes);
        if(typeof description !== 'string'){
            return logger.error(description, 'createLocation = description is not a STRING!');
        }
        if(typeof homelang !== 'object'){
            return logger.error(homelang, 'createLocation = homelang is not a OBJECT!');
        }
        if(typeof type !== 'object'){
            return logger.error(type, 'createLocation = type is not a OBJECT!');
        }
        const newLocation = {
            name: `${randomIndex(locationFirstnames)}'${randomIndex(locationMiddlenames)}-${randomIndex(locationLastnames)}`,
            homeland: homelang.name,
            description: description,
            type: type.name,
        };
        logger.info(newLocation.name, 'New location created!');
        return locations.push(newLocation);
    }
    catch(err){
        return logger.error(err);
    }
}

/**
 * This creates a new biome type for the creation of the homeland.
 * @param {string} name - The name of the biome.
 * @param {string} description - The short description of the biome.
 * @returns Pushes a new biome into the array list.
 */
function createBiome(name, description){
    try{
        if(typeof name !== 'string'){
            logger.error(name, 'createBiome = name is not a STRING!');
            return; // this is soru-style, soru has seen your thing. too: return in next line to indicate clearly that nothing is returned
        }
        if(typeof description !== 'string'){
            return logger.error(description, 'createBiome = description is not a STRING!');
        }
        const newBiome = {
            name: name,
            description: description,
        };
        logger.info(newBiome.name, 'New Biome created!');
        biomes.push(newBiome); // return uneeded as array.push() doesn't return anything you need
    }
    catch(err){
        logger.error(err); // return unneeded
    }
}
/**
 * This creates a new technology level to use in stuff.
 * @param {string} name - name of the technology.
 * @param {string} description - A short description of what this technology level is.
 * @param {number} Level - The number of the level. MUST BE FROM 1-10.
 * @returns This pushes a new tech into the array to be later used for the JSON file.
 */
function createTech(name, description, Level){
    try{
        if(typeof name !== 'string'){
            return logger.error(name, 'createTech = name is not a STRING!');
        }
        if(typeof description !== 'string'){
            return logger.error(description, 'createTech = description is not a STRING!');
        }
        if(typeof Level !== 'number'){
            return logger.error(Level, 'createTech = Level is not a STRING!');
        }
        if(Level > 10 || Level < 1){
            return logger.error(Level, 'Level can\'t be above or below 10');
        }
        const newTech = {
            name: name,
            description: description,
            level: Level,
        };
        logger.info(newTech.name, 'New technology created!');
        technologies.push(newTech); // return unneeded, as `array.push()` doesn't return anything you use, is last of line anyways
    }
    catch(err){
        logger.error(err); // return unneeded
    }
}

function createCharacter(){
    try{
        const newChar = {
            name: `${randomIndex(charFirstnames)}'${randomIndex(charMiddlenames)}-${randomIndex(charLastnames)}`,
            homeland: randomIndex(homelands).name,
            location: randomIndex(locations).name,
        };
        return characters.push(newChar);
    }
    catch(err){
        return logger.error(err);
    }
}

/**
 * This runs the program.
 */
function run(){
    try{
        // CREATE NEW TECH
        createTech('Primitive', 'A wheel, is unheard for these people. They may know how to make a fire, but anything more than that is far beyond them.', 1);
        createTech('Tribal', 'Fights with sticks and stones, old and primitive. Things newer than a wheel, are unkown to them.', 2);
        createTech('Ancient', 'Uhh... old?', 3);
        createTech('Medieval', 'Witches, swords, catapults are all known to these people. They might be a little naive, but most likely, theyknow how to fight in armed and unarmed melee combat.', 4);
        createTech('Industrial', 'Age of machines and steam and air pollution.', 5);
        createTech('Modern', 'The current level of technology. Things like cellphones and computers, radios, cars, are most likely known and researched.', 6);
        // PASTE IT INTO THE FILE
        fs.writeFile('shite/idk3data/tech.json', JSON.stringify(technologies), function(err){
            if(err){
                return logger.error(err);
            }
        });

        // CREATE THE BIOMES
        createBiome('Swamp', 'Damp and moist, watery ground, grass hiding the deep water pool, vines growning from the trees. Can seem a bit like a jungle, but isn\'t.');
        createBiome('Urban', 'Cities, skyscrapers, slums.');
        // WRITE TO FILE
        fs.writeFile('shite/idk3data/biomes.json', JSON.stringify(biomes), function(err){
            if(err){
                return logger.error(err);
            }
        });

        // GENERATE NAMES
        for(let x = 0; x < 20; x++){
            nameGenerator('homeland', homelandFirstnames);
            nameGenerator('homeland', homelandMiddlenames);
            nameGenerator('homeland', homelandLastnames);
            nameGenerator('location', locationFirstnames);
            nameGenerator('location', locationMiddlenames);
            nameGenerator('location', locationLastnames);
            nameGenerator('character', charFirstnames);
            nameGenerator('character', charMiddlenames);
            nameGenerator('character', charLastnames);
        }

        // GENERATE LOCATION TYPES
        createLocationtype('City', 'A city thingy');
        createLocationtype('Village', 'A village size location');
        // WRITE TO FILE
        fs.writeFile('shite/idk3data/locationtypes.json', JSON.stringify(locationtypes), function(err){
            if(err){
                return logger.error(err);
            }
        });

        // CREATE NEW HOMELAND
        for(let x = 0; x < 10; x++){
            createhomelang('I am gay');
            createLocation('I am not gay!');
        }
        // PASTE IT INTO A FILE
        fs.writeFile('shite/idk3data/homelands.json', JSON.stringify(homelands), function(err){
            if(err){
                return logger.error(err);
            }
        });
        // WRITE TO FILE
        fs.writeFile('shite/idk3data/locations.json', JSON.stringify(locations), function(err){
            if(err){
                return logger.error(err);
            }
        });

        for(let x = 0; x < 10; x++){
            createCharacter();
        }
        fs.writeFile('shite/idk3data/characters.json', JSON.stringify(characters), function(err){
            if(err){
                return logger.error(err);
            }
        });
    }
    catch(err){
        return logger.error(err);
    }
}



run();

// module.exports.init = function () {
//     console.log('hi');
//   };
