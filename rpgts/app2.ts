const fs = require('fs');

const firstNames = require('./firstnames.json');
const lastNames = require('./lastnames.json');
const genders = require('./genders.json');
const locations = require('./locations.json');
const jobs = require('./jobs.json');
const races = require('./races.json');
const classes = require('./classes.json');

const characters: { id: number; name: string; class: string; gender: string; race: string; hometown: string; homeland: string, job: string; age: number; horns?: number; wings?: number; }[] = [];

/**
 *this is an interface for ponies.
 *
 * @interface ponyCheck
 */
interface ponyCheck{
    id: number;
    name: string;
    class: string;
    gender: string;
    race: string;
    hometown: string;
    homeland: string;
    age: number;
    job: string;
    wings?: number;
    horns?: number;
}

/**
 *This craetes the basic Pony character, without horns and such
 *
 * @class Pony
 */
class Pony{
    id: number;
    name: string;
    chClass: string;
    gender: string;
    race: string;
    hometown: string;
    homeland: string;
    age: number;
    job: string;
    constructor(getID: number, getName: string, getChclass: string, getGender: string, getRace: string, getHometown: string, getHomeland: string, getAge: number, getJob: string){
        this.id = getID;
        this.name = getName;
        this.chClass = getChclass;
        this.gender = getGender;
        this.race = getRace;
        this.hometown = getHometown;
        this.homeland = getHomeland;
        this.job = getJob
        this.age = getAge;
    }
    push(){
        const char = {
            id: this.id,
            name: this.name,
            class: this.chClass,
            gender: this.gender,
            race: this.race,
            hometown: this.hometown,
            homeland: this.homeland,
            job: this.job,
            age: this.age,
        };
        characters.push(char);
    }
}

/**
 *This creates a Pony character with wings and horns.
 *
 * @class Alicorn
 * @extends {Pony}
 */
class Alicorn extends Pony{
    wings: number;
    horns: number;
    constructor(id: number, name: string, chClass: string, gender: string, race: string, hometown: string, homeland: string, age: number, job: string, getWings: number, getHorns: number){
        super(id, name, chClass, gender, race, hometown, homeland, age, job);
        this.wings = getWings;
        this.horns = getHorns;
    }
    push(){
        const char = {
            id: this.id,
            name: this.name,
            class: this.chClass,
            gender: this.gender,
            race: this.race,
            hometown: this.hometown,
            homeland: this.homeland,
            job: this.job,
            age: this.age,
            horns: this.horns,
            wings: this.wings,
        };
        characters.push(char);
    }
}

/**
 *This creates a POny character with JUST wings.
 *
 * @class Winged
 * @extends {Pony}
 */
class Winged extends Pony{
    wings: number;
    constructor(id: number, name: string, chClass: string, gender: string, race: string, hometown: string, homeland: string, age: number, job: string, getWings: number){
        super(id, name, chClass, gender, race, hometown, homeland, age, job);
        this.wings = getWings;
    }
    push(){
        const char = {
            id: this.id,
            name: this.name,
            class: this.chClass,
            gender: this.gender,
            race: this.race,
            hometown: this.hometown,
            homeland: this.homeland,
            job: this.job,
            age: this.age,
            wings: this.wings,
        };
        characters.push(char);
    }
}

/**
 *This creates a Pony character with JUST horns.
 *
 * @class Horned
 * @extends {Pony}
 */
class Horned extends Pony{
    horns: number;
    constructor(id: number, name: string, chClass: string, gender: string, race: string, hometown: string, homeland: string, age: number, job: string, getHorns: number){
        super(id, name, chClass, gender, race, hometown, homeland, age, job);
        this.horns = getHorns;
    }
    push(){
        const char = {
            id: this.id,
            name: this.name,
            class: this.chClass,
            gender: this.gender,
            race: this.race,
            hometown: this.hometown,
            homeland: this.homeland,
            job: this.job,
            age: this.age,
            horns: this.horns,
        };
        characters.push(char);
        // console.log(char);
    }
}

/**
 *This just returns a random entry from the array.
 *
 * @param {any[]} array
 * @returns - The entry
 */
function randomArr(array: any[]){
    return array[Math.floor(Math.random() * array.length)];
}

/**
 *This will display info about a pony.
 *
 * @param {ponyCheck} obj - the object of the pony to display.
 * @returns
 */
function displayPony(obj: ponyCheck){
    let display = `${obj.name} is a ${obj.gender} ${obj.race} ${obj.class}.`;
    if(obj.horns != undefined){ display += `\nThey have ${obj.horns} horns.`; }
    if(obj.wings != undefined){ display += `\nThey have ${obj.wings} wings.`; }
    display += `\nThey hail from ${obj.hometown}, ${obj.homeland} and they are ${obj.age} years old.\n\n`;
    return console.log(display);
}

/**
 *This will build a character.
 *
 * @param {number} times - How many characters to build.
 * @returns
 */
async function characterBuilder(times: number){
    const idsUsed: number[] = [], namesUsed: string[] = [];
    for(let x = 0; x < times; x++){
        const race = randomArr(races);
        const gender = randomArr(genders);
        const home = randomArr(locations);
        const job = randomArr(jobs);
        const chClass = randomArr(classes);
        const age = Math.floor(Math.random() * 100) + 1;
        let id, name;
        do{ id = Math.floor(Math.random() * 10000); }while(idsUsed.includes(id));
        idsUsed.push(id);
        do{ name = randomArr(firstNames) + ' ' + randomArr(lastNames); }while(namesUsed.includes(name));
        namesUsed.push(name);
        let pony;
        switch(race.id){
            case 1:
                // unicorn
                pony = new Horned(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.horns);
                break;
            case 2:
                // pegasus
                pony = new Winged(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.wings);
                break;
            case 3:
                // earth pon
                pony = new Pony(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name);
                break;
            case 4:
                // alicorn
                pony = new Alicorn(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.wings, race.horns);
                break;
            case 5:
                // changlelimg
                pony = new Alicorn(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.wings, race.horns);
                break;
            case 6:
                // fyrian
                pony = new Horned(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.horns);
                break;
            case 7:
                // f pegasyus
                pony = new Alicorn(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.wings, race.horns);
                break;
            case 8:
                // f alicorn
                pony = new Alicorn(id, name, chClass.name, gender.name, race.name, home, race.homeland, age, job.name, race.wings, race.horns);
                break;
            default:
                return console.log('Invalid race.');
        }
        pony.push();
    }
}

/**
 *This will run the program in console because i am retarded and still haven't made a fucking web interface.
 * - yes it is a bit of spaghetti code, but bear with me lmao.
 * @returns
 */
async function run(){
    try{
        await characterBuilder(10);
        for(let x = 0; x < characters.length; x++){ displayPony(characters[x]); }
    }
    catch(err){ throw new Error(err); }
}

run();