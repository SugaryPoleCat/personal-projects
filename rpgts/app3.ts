// This is all for like, important stuff
const firstNames = require('./firstnames.json');
const lastNames = require('./lastnames.json');
const uniqueNames = require('./uniquenames.json');
const genders = require('./genders.json');
const locations = require('./locations.json');
const jobs = require('./jobs.json');
const races = require('./races.json');
const classes = require('./classes.json');

// Little less important.
const fs = require('fs');

/**
 *this is an interface for ponies.
 *
 * @interface ponyCheck
 */
interface ponyCheck{
    id: number;
    name: string;
    class: string;
    gender: string;
    race: string;
    hometown: string;
    homeland: string;
    age: number;
    height: number;
    weight: number;
    job: string;
    wings?: number;
    horns?: number;
    attributes: {
        strength: number;
        dexterity: number;
        endurance: number;
        intelligence: number;
        wisdom: number;
        knowledge: number;
    };
    stats: {
        health: number;
        mana: number;
        defence: number;
        attack: number;
        magicDefence: number;
        magicAttack: number;
    };
    colours: {
        mane: string;
        coat: string;
        iris: string;
    };
}

// List of characters.
const characters: ponyCheck[] = [];

/**
 *this gets a random entry from the array index
 *
 * @param {(string | any[])} arr
 * @returns
 */
function randomArr(arr: string | any[]){
    return arr[Math.floor(Math.random() * arr.length)];
}

/**
 *Idfk nigga.
 *
 * @param {ponyCheck} obj
 */
function createOC(obj: ponyCheck){
    const char = {
        id: obj.id,
        name: obj.name,
        class: obj.class,
        gender: obj.gender,
        race: obj.race,
        hometown: obj.hometown,
        homeland: obj.homeland,
        age: obj.age,
        height: obj.height,
        weight: obj.weight,
        job: obj.job,
        attributes: {
            strength: obj.attributes.strength,
            dexterity: obj.attributes.dexterity,
            endurance: obj.attributes.endurance,
            intelligence: obj.attributes.intelligence,
            wisdom: obj.attributes.wisdom,
            knowledge: obj.attributes.knowledge,
        },
        stats: {
            health: obj.stats.health,
            mana: obj.stats.mana,
            defence: obj.stats.defence,
            attack: obj.stats.attack,
            magicDefence: obj.stats.magicDefence,
            magicAttack: obj.stats.magicAttack,
        },
        colours: {
            mane: obj.colours.mane,
            coat: obj.colours.coat,
            iris: obj.colours.iris,
        },
    }
    let char2, char3, char4;
    if(obj.wings != undefined){ char2 = { wings: obj.wings, }}
    if(obj.horns != undefined){ char3 = { horns: obj.horns, }}
    char4 = {...char, ...char2, ...char3, }
    characters.push(char4);
}

/**
 *This is math random basically.
 *
 * @param {number} max - The max number
 * @param {number} min - The min number
 * @returns - Returns the random  number.
 */
function mathRandom(min: number, max: number){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 *Generate the new color number
 *
 * @returns - Returns the color in #
 */
function randomColour(){
    const colour = "#" + Math.floor(Math.random() * 16777215).toString(16);
    return colour
}

/**
 *This will build a character.
 *
 * @param {number} times - How many characters to build.
 * @returns
 */
async function characterBuilder(times: number){
    const idsUsed: number[] = [], namesUsed: string[] = [];
    for(let x = 0; x < times; x++){
        const race = randomArr(races);
        const gender = randomArr(genders);
        let home;
        // console.log(locations);
        switch(race.homeland){
            case 'Equestria':
                home = randomArr(locations.equestria);
                break;
            case 'Fyria':
                home = randomArr(locations.fyria);
                break;
            default:
                return console.error('fuck you doing bro');
        }
        const job = randomArr(jobs);
        const chClass = randomArr(classes);
        const age = mathRandom(1, 200);
        let id, name;
        // CREATE ID FOR LATER USE
        do{ id = mathRandom(1, 1000000); }while(idsUsed.includes(id));
        idsUsed.push(id);
        // CREATE NAME
        // This do will loop through again and again, if the name used is in the list of used names. If not, it will add the name and continue.
        do{
            const nameSwitch = mathRandom(1, 2);
            let fName, lName;
            // What this does, is it randomly picks if the name is going to be a Fluttershy or a Flutter Shy.
            switch(nameSwitch){
                case 1:
                    // This do, will randomise firstname and lastname again, if they happe to be the same name, as the lists has some names that are identical.
                    do{
                        fName = randomArr(firstNames);
                        lName = randomArr(lastNames);
                    }while(fName == lName);
                    name = fName + ' ' + lName;
                    break;
                case 2:
                    name = randomArr(uniqueNames);
                    break;
                default:
                    throw new Error('Something went terribly wrong.');
            }
        }while(namesUsed.includes(name));
        namesUsed.push(name);

        const height = mathRandom(120, 220);
        const weight = mathRandom(40, 200);

        // count attack and defnese and shit like that from attributes.
        const pony = {
            id: id,
            name: name,
            class: chClass.name,
            gender: gender.name,
            race: race.name,
            hometown: home,
            homeland: race.homeland,
            age: age,
            height: height,
            weight: weight,
            job: job.name,
            wings: race.wings,
            horns: race.horns,
            attributes: {
                strength: race.attributes.strength + gender.attributes.strength + job.attributes.strength + chClass.attributes.strength,
                dexterity: race.attributes.dexterity + gender.attributes.dexterity + job.attributes.dexterity + chClass.attributes.dexterity,
                endurance: race.attributes.endurance + gender.attributes.endurance + job.attributes.endurance + chClass.attributes.endurance,
                intelligence: race.attributes.intelligence + gender.attributes.intelligence + job.attributes.intelligence + chClass.attributes.intelligence,
                wisdom: race.attributes.wisdom + gender.attributes.wisdom + job.attributes.wisdom + chClass.attributes.wisdom,
                knowledge: race.attributes.knowledge + gender.attributes.knowledge + job.attributes.knowledge + chClass.attributes.knowledge,
            },
            stats: {
                health: race.attributes.endurance + gender.attributes.endurance + job.attributes.endurance + chClass.attributes.endurance + race.stats.health + gender.stats.health + job.stats.health + chClass.stats.health,
                mana: race.attributes.intelligence + gender.attributes.intelligence + job.attributes.intelligence + chClass.attributes.intelligence + race.stats.mana + gender.stats.mana + job.stats.mana + chClass.stats.mana,
                defence: race.attributes.endurance + gender.attributes.endurance + job.attributes.endurance + chClass.attributes.endurance + race.attributes.dexterity + gender.attributes.dexterity + job.attributes.dexterity + chClass.attributes.dexterity,
                attack: race.attributes.strength + gender.attributes.strength + job.attributes.strength + chClass.attributes.strength + race.attributes.dexterity + gender.attributes.dexterity + job.attributes.dexterity + chClass.attributes.dexterity,
                magicDefence: race.attributes.intelligence + gender.attributes.intelligence + job.attributes.intelligence + chClass.attributes.intelligence + race.attributes.knowledge + gender.attributes.knowledge + job.attributes.knowledge + chClass.attributes.knowledge,
                magicAttack: race.attributes.intelligence + gender.attributes.intelligence + job.attributes.intelligence + chClass.attributes.intelligence + race.attributes.wisdom + gender.attributes.wisdom + job.attributes.wisdom + chClass.attributes.wisdom,
            },
            colours: {
                mane: randomColour(),
                coat: randomColour(),
                iris: randomColour(),
            },
        }
        createOC(pony);
    }
}

/**
 *This is the display of characters. As in, it displays their info in more human way
 *
 * @param {ponyCheck} obj the object, aka the character to display
 * @returns - Returns the dispaly.
 */
function characterViewer(obj: ponyCheck){
    let details, heshe, herhis;
    if(obj.gender == 'Mare'){ heshe = 'She'; herhis = 'Her'; }
    else if(obj.gender == 'Stallion'){ heshe = 'He'; herhis = 'His'; }
    details = `${obj.name} is a ${obj.gender}, ${obj.race}, ${obj.class}.\n${heshe} hails from ${obj.homeland}, ${obj.hometown}.\n${heshe} is ${obj.age} years old, ${obj.height}cm tall, weights ${obj.weight}kg and works as ${obj.job}.`;
    if(obj.wings != undefined){ details += ` ${heshe} has ${obj.wings} wings.`}
    if(obj.horns != undefined){ details += ` ${heshe} has ${obj.horns} horn/s.`}
    details += `\n${herhis} attributes are: PHYSICAL = ${obj.attributes.strength} strength | ${obj.attributes.dexterity} dexterity | ${obj.attributes.endurance} endurance || MENTAL = ${obj.attributes.intelligence} intelligence | ${obj.attributes.knowledge} knowledge | ${obj.attributes.wisdom} wisdom`;
    details += `\n${herhis} stats are: ${obj.stats.health} health | ${obj.stats.mana} mana | ${obj.stats.defence} defence | ${obj.stats.attack} attack | ${obj.stats.magicDefence} magical defence | ${obj.stats.magicAttack} magical attack.`;
    details += `\n${herhis} colors are: ${obj.colours.mane} mane, ${obj.colours.coat} coat, ${obj.colours.iris} iris.`;
    fs.appendFileSync('./characters.txt', details + '\n\n', (err: string | undefined) => {
        if(err){ throw new Error(err); }
    });
    return console.log(details + '\n\n');
}

/**
 *This counts things. Like number of name combinations.
 *
 */
function counter(){
    const firstNameCount: number = firstNames.length;
    const lastNameCount: number = lastNames.length;
    const uniqueNamesCount: number = uniqueNames.length;
    let allNameCount: number = 0;
    for(let x = 0; x < firstNames.length; x++){
        for(let y = 0; y < lastNames.length; y++){
            allNameCount++;
        }
    }
    for(let x = 0; x < uniqueNames.length; x++){
        allNameCount++;
    }
    console.log('First names: ', firstNameCount);
    console.log('Last names: ', lastNameCount);
    console.log('Unique names: ', uniqueNamesCount);
    console.log('Total names: ', allNameCount);
    // more accurate counter for names;
    let namesUsed: string[] = [];
    let name: string;
    let fName: string, lName: string;
    for(let x = 0; x < firstNames.length; x++){
        for(let y = 0; y < lastNames.length; y++){
            fName = firstNames[x];
            lName = lastNames[y];
            name = fName + ' ' + lName;
            if(fName != lName && !namesUsed.includes(name)){ namesUsed.push(name); }
        }
    }
    for(let x = 0; x < uniqueNames.length; x++){
        namesUsed.push(uniqueNames[x]);
    }
    fs.writeFileSync('./names.txt', namesUsed.join('\n'), (err: string | undefined) => {
        if(err){ throw new Error(err); }
    })
    const allNamesCount2: number = namesUsed.length;
    console.log('Actual unique names we can use: ', allNamesCount2);
    const namesDifference: number = allNameCount - allNamesCount2;
    console.log('There is a difference of ', namesDifference, ' names less, which we can actually use.');

    let totalCombinations: number = 0;
    for(let x = 0; x < genders.length; x++){
        for(let y = 0; y < races.length; y++){
            for(let z = 0; z < classes.length; z++){
                for(let f = 0; f < jobs.length; f++){
                    for(let g = 0; g < locations.fyria.length; g++){
                        totalCombinations++;
                    }
                }
            }
        }
    }
    for(let x = 0; x < genders.length; x++){
        for(let y = 0; y < races.length; y++){
            for(let z = 0; z < classes.length; z++){
                for(let f = 0; f < jobs.length; f++){
                    for(let g = 0; g < locations.equestria.length; g++){
                        totalCombinations++;
                    }
                }
            }
        }
    }
    console.log('Total UNIQUE OC characteristics that can be made (without counting for amounts of horns, wings, height, weight and stats.):', totalCombinations, '\n\n');

    let totalCombinationsWithNames: number = 0;
    for(let h = 0; h < namesUsed.length; h++){
        for(let x = 0; x < genders.length; x++){
            for(let y = 0; y < races.length; y++){
                for(let z = 0; z < classes.length; z++){
                    for(let f = 0; f < jobs.length; f++){
                        for(let g = 0; g < locations.fyria.length; g++){
                            totalCombinationsWithNames++;
                        }
                    }
                }
            }
        }
        for(let x = 0; x < genders.length; x++){
            for(let y = 0; y < races.length; y++){
                for(let z = 0; z < classes.length; z++){
                    for(let f = 0; f < jobs.length; f++){
                        for(let g = 0; g < locations.equestria.length; g++){
                            totalCombinationsWithNames++;
                        }
                    }
                }
            }
        }
    }
    console.log('Total UNIQUE OC characteristics that can be made WITH unique names (without counting for amounts of horns, wings, height, weight and stats.):', totalCombinationsWithNames, '\n\n');
}

/**
 *RUN THE APP AAA
 *
 */
async function run(){
    try{
        characterBuilder(20);
        counter();
        fs.writeFileSync('./characters.txt', '', (err: string | undefined) => {
            if(err){ throw new Error(err); }
        });
        // for(const character of characters){ characterViewer(character); }
        console.log('Done weriting to file');
    }
    catch(err){ throw new Error(err); }
}

run();