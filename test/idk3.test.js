const expect = require('chai').expect;
const assert = require('chai').assert;
const idk3 = require('../shite/idk3');
const logger = require('../utils/logger');

describe('randomIndex', function (){
    it('should return a random letter', function (){
        const someArray = 'abcdefghijklmnopqrstuvwxyz';
        const randomindex = idk3.randomIndex(someArray);
        logger.info(randomindex);
        expect(randomindex).to.be.a('string');
    });
});

describe('nameGenerator', function (){
    it('Should return a name', function (){
        const name = [];
        const newname = idk3.nameGenerator('character',name);
        expect(newname).to.be.a('string');
    });
});