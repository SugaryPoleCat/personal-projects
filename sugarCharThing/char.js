const chars = [];

function generateChar(name, tease, cocktease, sexappeal, chaos, demonstuff, title, paininthebutt) {
    const char = {
        name: name,
        tease: tease,
        cocktease: cocktease,
        sexappeal: sexappeal,
        chaos: chaos,
        demonstuff: demonstuff,
        paininthebutt: paininthebutt,
        title: title,
    };
    chars.push(char);
}

generateChar('Sugar Polecat',
    100,
    100,
    420,
    100,
    100,
    'I\'M A DEMON DAMMIT NOT AN IMP',
    420);
generateChar('Sea BOOSH',
    69,
    '',
    '',
    '',
    '',
    'I\'m a SEA FERN.',
    100);
chars.map(ch => {
    let string = `Name: ${ch.name}\n`;
    if (ch.title) {
        string += `Title: ${ch.title}\n`
    }
    if (ch.tease) {
        string += `Tease: ${ch.tease} / 100\n`
    }
    if (ch.cocktease) {
        string += `Cocktease: ${ch.cocktease} / 100\n`
    }
    if (ch.sexappeal) {
        string += `Sexappeal: ${ch.sexappeal} / 100\n`
    }
    if (ch.chaos) {
        string += `Chaos: ${ch.chaos} / 100\n`
    }
    if (ch.paininthebutt) {
        string += `Pain in the butt: ${ch.paininthebutt} / 100\n`
    }
    if (ch.demonstuff) {
        string += `Demonstuff: ${ch.demonstuff} / 100\n`
    }
    console.log(string);
});