const items: iItem[] = [];

interface iItem {
	name: string;
	rarity: string;
	description: string;
	type: string;
	element: string;
}

enum eItemRarity {
	C = 'Common',
	R = 'Rare',
	E = 'Exotic',
	L = 'Legendary'
}

enum eItemElement {
	P = 'Physical',
	F = 'Fire',
	I = 'Ice',
	D = 'Darkness',
	L = 'Light'
}

enum eItemType {
	S = 'Sword',
	A = 'Axe',
	SG = 'Shotgun',
	AR = 'Assault rifle',
	SMG = 'SMG',
	LMG = 'LMG',
	SR = 'Sniper rifle'
}

function createItem(name: string, rarity: string, type: string, element: string, description: string): void {
	const item: iItem = {
		name: name,
		rarity: rarity,
		type: type,
		element: element,
		description: description,
	};
	items.push(item);
}

function start() {
	try {
		console.log(eItemType);
		for (const itemType of eItemType) {
			for (const itemRarity of eItemRarity) {
				for (const itemElement of eItemElement) {
					createItem(
						'Rose',
						itemType,
						eItemType.S,
						eItemElement.L,
						'This is a rose',
					)
				}
			}
		}
		console.log(items);
	} catch (err) {
		console.error(err);
	}
}
start();