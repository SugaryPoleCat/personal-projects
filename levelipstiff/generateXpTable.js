let xp = 0,
	maxXp = 50,
	level = 1,
	finalMaxXp = 0,
	maxLevel = 0;
while (isFinite(maxXp)) {
	xp += maxXp;
	if (xp >= maxXp) {
		xp = Math.floor(xp - maxXp);
		level++;
		finalMaxXp = maxXp;
		maxXp = Math.floor(maxXp * 1.05);
		// console.log('[', new Date().toUTCString(), ']\nXP: ' + `${xp} / ${maxXp} | Level: ${level}`);
	}

	// for some reason doesnt stop until it actuall becomes infinity.
	// same thing below

	// maxXp = Math.floor(maxXp * 1.1);
	// level++;
	// ^^ yeilds same results
}
maxLevel = level - 1;
console.log('[', new Date().toUTCString(), ']\nfinalMaxXp\n', finalMaxXp);
console.log('[', new Date().toUTCString(), ']\nmaxLevel\n', maxLevel);