const itemRarities = [],
	itemElements = [],
	itemSets = [],
	itemWeapons = [],
	itemArmors = [],
	itemAccessories = [],
	itemTypes = [];

// armor goes over: clothes and underwear.
/*
underwear: socks, underwear, undershirt
*/

function randomIndex(array) {
	return array[Math.floor(Math.random() * array.length)];
}

async function generatedItem(name) {
	console.log('[', new Date().toUTCString(), ']\nname\n', name);
	if (!name) {
		name = 'Test';
	}
	const item = {
		name: name,
		rarity: await randomIndex(itemRarities),
		type: {
			category: await randomIndex(itemTypes),
		},
	};

	if (item.rarity.name == 'Set') {
		item.set = await randomIndex(itemSets);
	}

	await $('#generatedItem').html(`
		<div id="itemThumbStuff">
			<div id="itemThumbnail"><img src="./icon.png" alt="icon" style="border-color: ${item.rarity.color}"/></div>
			<span id="itemCategory">${item.type.category}</span>
		</div>
		<b>Name:</b> <span style="color: ${item.rarity.color}">${item.name}</span><br/>
		<b>Rarity:</b> <span style="color: ${item.rarity.color}">${item.rarity.name}</span><br/>
	`)
		.css('border-color', item.rarity.color)
		.css('background', `linear-gradient(0deg, ${item.rarity.color} 0%, ${item.rarity.color}00 10%, ${item.rarity.color}00 90%, ${item.rarity.color} 100%`);

	await $('#itemThumbnail').css('border-color', item.rarity.color);

	let slot;
	switch (item.type.category) {
		case 'Weapon':
			item.element = await randomIndex(itemElements);
			slot = await randomIndex(itemWeapons);
			// item.type.name = await slot.name;
			item.type.slot = await slot.slot;
			item.type.name = await slot.name;
			await $('#generatedItem').append(`
				<br/>
				<b>Type:</b> <span>${item.type.name}</span><br/>
				<b>Slot:</b> <span>${item.type.slot}</span><br/>
				<b>Element:</b> <span style="color: ${item.element.color};">${item.element.name}</span><br/>
			`);
			break;
		case 'Armor':
			slot = await randomIndex(itemArmors);
			// item.type.name = await slot.name;
			item.type.slot = await slot.slot;
			await $('#generatedItem').append(`
				<br/>
				<b>Slot:</b> <span>${item.type.slot}</span><br/>
			`);
			break;
		case 'Accessory':
			slot = await randomIndex(itemAccessories);
			// item.type.name = await slot.name;
			item.type.slot = await slot.slot;
			await $('#generatedItem').append(`
				<br/>
				<b>Slot:</b> <span>${item.type.slot}</span><br/>
			`);
			break;
	}

	if (item.set) {
		await $('#generatedItem').append(`
			<br/>
			<b>Set:</b> <span>${item.set.name}</span><br/>
			<b>Items in set:</b> <span>${item.set.amount}</span><br/>
		`);
	}

	item.sockets = await Math.floor(Math.random() * (3 - 0 + 1) + 0);
	console.log('[', new Date().toUTCString(), ']\nitem.sockets\n', item.sockets);
	if (item.sockets > 0) {
		$('#generatedItem').append(`
			<br/>
			<b>Sockets:</b> <span>${item.sockets}</span><br/>`);
	}
}

$('document').ready(async function () {
	try {
		await $.getJSON('./itemStuff.json', async function (data) {
			await $.each(data.rarities, async function (i) {
				itemRarities.push(data.rarities[i]);
			});
			await $.each(data.elements, async function (i) {
				itemElements.push(data.elements[i]);
			});
			await $.each(data.types, async function (i) {
				itemTypes.push(data.types[i].category);
				if (data.types[i].category == 'Weapon') {
					$.each(data.types[i].items, function (j) {
						itemWeapons.push(data.types[i].items[j]);
					});
				}
				if (data.types[i].category == 'Armor') {
					$.each(data.types[i].items, function (j) {
						itemArmors.push(data.types[i].items[j]);
					});
				}
				if (data.types[i].category == 'Accessory') {
					$.each(data.types[i].items, function (j) {
						itemAccessories.push(data.types[i].items[j]);
					});
				}
			});
			await $.each(data.sets, async function (i) {
				itemSets.push(data.sets[i]);
			});
		});

		console.log('[', new Date().toUTCString(), ']\nitemRarities\n', itemRarities);
		console.log('[', new Date().toUTCString(), ']\nitemElements\n', itemElements);
		console.log('[', new Date().toUTCString(), ']\nitemWeapons\n', itemWeapons);
		console.log('[', new Date().toUTCString(), ']\nitemArmors\n', itemArmors);
		console.log('[', new Date().toUTCString(), ']\nitemAccessories\n', itemAccessories);
		// for (const rarity of itemRarities) {
		// 	await $('#things').append(`
		// 		<b>Rarity:</b> <span style="color: ${rarity.color}">${rarity.name}</span><br/>
		// 	`);
		// }
		// await $('#things').append('<br/>');
		// for (const element of itemElements) {
		// 	await $('#things').append(`
		// 		<b>Element:</b> <span style="color: ${element.color}">${element.name}</span><br/>
		// 	`);
		// }
		// await $('#things').append('<br/>');
		// for (const type of itemTypes) {
		// 	await $('#things').append(`
		// 		<b>Type:</b> <span>${type.name}</span><br/>
		// 	`);
		// }
		// await $('#things').append('<br/>');
		// for (const set of itemSets) {
		// 	await $('#things').append(`
		// 		<b>Set:</b> <span>${set.name}</span> | <b>Items in set:</b> <span>${set.amount}</span><br/>
		// 	`);
		// }
		// await $('#things').append('<br/>');

		generatedItem();
	} catch (err) {
		$('#generatedItem').html('<p style="color: red">Shit fucked up</p>');
		console.error('[', new Date().toUTCString(), ']\nSomething went wrong \n', err);
		return;
	}
});
$('#but_generate').on('click', function () {
	generatedItem($('#item_name').val());
});