// const items = [];
// // function createItem(name, perk, rarity, type, element){
// // 	const item = {
// // 		name: name,
// // 		perk: perk,
// // 		rarity: rarity,
// // 		type: type,
// // 		element: element,
// // 	};
// // 	items.push(item);
// // }
var items = [];
var eItemRarity;
(function (eItemRarity) {
    eItemRarity["C"] = "Common";
    eItemRarity["R"] = "Rare";
    eItemRarity["E"] = "Exotic";
    eItemRarity["L"] = "Legendary";
})(eItemRarity || (eItemRarity = {}));
var eItemElement;
(function (eItemElement) {
    eItemElement["P"] = "Physical";
    eItemElement["F"] = "Fire";
    eItemElement["I"] = "Ice";
    eItemElement["D"] = "Darkness";
    eItemElement["L"] = "Light";
})(eItemElement || (eItemElement = {}));
var eItemType;
(function (eItemType) {
    eItemType["S"] = "Sword";
    eItemType["A"] = "Axe";
    eItemType["SG"] = "Shotgun";
    eItemType["AR"] = "Assault rifle";
    eItemType["SMG"] = "SMG";
    eItemType["LMG"] = "LMG";
    eItemType["SR"] = "Sniper rifle";
})(eItemType || (eItemType = {}));
function createItem(name, rarity, type, element, description) {
    var item = {
        name: name,
        rarity: rarity,
        type: type,
        element: element,
        description: description
    };
    items.push(item);
}
function start() {
    try {
        console.log(eItemType);
        createItem('Rose', eItemRarity.L, eItemType.S, eItemElement.L, 'This is a rose');
        console.log(items);
    }
    catch (err) {
        console.error(err);
    }
}
start();
