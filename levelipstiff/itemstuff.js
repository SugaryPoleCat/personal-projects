const items = [];

const eItemRarity = {
	C: 'Common',
	R: 'Rare',
	E: 'Exotic',
	L: 'Legendary',
};

const eItemElement = {
	P: 'Physical',
	F: 'Fire',
	I: 'Ice',
	D: 'Darkness',
	L: 'Light',
};

const eItemType = {
	S: 'Sword',
	A: 'Axe',
	SG: 'Shotgun',
	AR: 'Assault rifle',
	SMG: 'SMG',
	LMG: 'LMG',
	SR: 'Sniper rifle',
};

function createItem(name, rarity, type, element, description) {
	const item = {
		name: name,
		rarity: rarity,
		type: type,
		element: element,
		description: description,
	};
	items.push(item);
}

function start() {
	try {
		console.log(eItemType);
		for (const [itemTypeKey, itemTypeValue] of Object.entries(eItemType)) {
			for (const itemRarity of Object.entries(eItemRarity)) {
				for (const itemElement of Object.entries(eItemElement)) {
					createItem(
						'Rose',
						itemRarity[1],
						itemTypeValue,
						itemElement[1],
						'Rose it is',
					);
				}
			}
		}
		console.log(items);
	} catch (err) {
		console.error(err);
	}
}
start();