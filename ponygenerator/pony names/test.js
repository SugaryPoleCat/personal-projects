const fs = require('fs');
let b = 0;
const fNames = fs.readFileSync('fnames.json');
const fNamesJson = JSON.parse(fNames);
const lNames = fs.readFileSync('lnames.json');
const lNamesJson = JSON.parse(lNames);
for (const fcat in fNamesJson) {
    const fnames = fNamesJson[fcat];
    for (const fname of fnames){
        for (const lcat in lNamesJson) {
            const lnames = lNamesJson[lcat];
            for (const lname of lnames){
                b++;
            }
        }
    }
}
console.log(b);