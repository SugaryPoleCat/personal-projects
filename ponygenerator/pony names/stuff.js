/**
 * This will capitalize your strings
 * @param {string} string What string to capitalize
 */
function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
$('document').ready(async function () {
    let names = [];
    let lastNames = [];
    let firstNames = [];
    let nameCombos = 0,
        firstNamesNumber = 0,
        lastNamesNumber = 0;
    await $('#found-names').text(`Names found: 0`);
    await $.getJSON('../names.json', async function (fNames) {
        $.each(fNames, function (fNameCat) {
            $('#generated-names').append(`<button class="btn btn-info btn-block" type="button" data-toggle="collapse" data-target="#collapse-${fNameCat}" aria-expanded="false" aria-controls="collapse-${fNameCat}">${fNameCat}</button>`);
            $('#generated-names').append(`<ul class="collapse list-group" id="collapse-${fNameCat}"></ul>`);
            // Load name
            $.each(fNames[fNameCat], async function (fName) {
                // get json for last names
                $(`#collapse-${fNameCat}`).append(`<button class="btn btn-outline-info btn-block" type="button" data-toggle="collapse" data-target="#collapse-${fNameCat}-${fName}" aria-expanded="false" aria-controls="collapse-${fNameCat}-${fName}">${fNames[fNameCat][fName]}</button>`);
                $(`#collapse-${fNameCat}`).append(`<ul class="collapse list-group" id="collapse-${fNameCat}-${fName}"></ul>`);
                await firstNames.push(fNames[fNameCat][fName]);
                await lastNames.push(fNames[fNameCat][fName]);
                // nameCombos = nameCombos + 1;
                $.getJSON('../lastnames.json', function (lNames) {
                    // get category
                    $.each(lNames, function (lNameCat) {
                        // get name
                        $.each(lNames[lNameCat], async function (lName) {
                            // check if same, if yes skip.
                            await lastNames.push(lNames[lNameCat][lName]);
                            if (fNames[fNameCat][fName] != lNames[lNameCat][lName]) {
                                // return result
                                // console.log('name combo in if', nameCombos);
                                const name = await `${fNames[fNameCat][fName]} ${lNames[lNameCat][lName]}`
                                await names.push(name);
                                $(`#collapse-${fNameCat}-${fName}`).append(`<li class="list-group-item list-group-item-info">${name}</li>`);
                            }
                        });
                    });
                });
            })
        });

    $('#name-search').on('keyup', function () {
        let namesFound = 0;
        let value = $(this).val().toLowerCase();
        $.getJSON('fnames.json', function (fNames) {
            $.each(fNames, function (fNameCat) {
                $(`#collapse-${fNameCat} li`).filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
                    if ($(this).text().toLowerCase().indexOf(value) > -1) {
                        namesFound++;
                        $('#found-names').text(`Names found: ${namesFound}`);
                    } else {
                        // namesFound = 0;
                        $('#found-names').text(`Names found: ${namesFound}`);
                    }
                });
            });
        });
    });
    $('#gen-name').on('click', function () {
        const nameLookup = [];
        if ($('#defaultCheck1').prop('checked') == true) {
            for (const name of names) {
                if (name.includes(capitalize($('#gen-name-with').val()))) {
                    nameLookup.push(name);
                } else {
                    $('#generated-name').text('Cannot find that name');
                }
            }
            $('#generated-name').text(nameLookup[Math.floor(Math.random() * nameLookup.length)]);
        } else if ($('#firstNameCheck').prop('checked') == true) {
            const name = capitalize($('#first-name-input').val());
            $('#generated-name').text(`${name} ${lastNames[Math.floor(Math.random() * lastNames.length)]}`);
        } else if ($('#lastNameCheck').prop('checked') == true) {
            const name = capitalize($('#last-name-input').val());
            $('#generated-name').text(`${firstNames[Math.floor(Math.random() * firstNames.length)]} ${name}`);
        } else if ($('#noSpecs').prop('checked') == true) {
            nameLookup.length = 0;
            $('#generated-name').text(names[Math.floor(Math.random() * names.length)]);
        }
    });
});