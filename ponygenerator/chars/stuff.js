const charNames = [];
const lastNames = [];
const firstNames = [];
const charGenders = [];
const charRaces = [];
const charSexes = [];
const charSexualities = [];
const charLocations = [];
const charSpecies = [];
// city belongs to country
// so city needs to be an object.
const charCountries = [];
const charCities = [];
const chars = [];
/**
 * This will take a random index from the array
 *
 * @param {any} arr - array
 * @returns The single tiem
 */
function randomArray(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
}
/**
 * This will capitalize first letter in a string
 *
 * @param {string} string - String to capitalize
 * @returns - The capitalized sttrng
 */
function capitalize(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
/**
 * This will get a double nested item in a json file
 *
 * @param {any} file the json file
 * @param {any} arr the array to push to
 */
async function doubleJSON(file, arr) {
	await $.getJSON(`${file}.json`, function (data) {
		$.each(data, function (dataCategory) {
			$.each(data[dataCategory], async function (dataItem) {
				await arr.push(data[dataCategory][dataItem]);
			});
		});
	});
}
/**
 * This will get a single nested item in a json file
 *
 * @param {any} file The json file
 * @param {any} arr the array to push to
 */
async function singleJSON(file, arr) {
	await $.getJSON(`${file}.json`, function (data) {
		$.each(data, async function (dataItem) {
			await arr.push(data[dataItem]);
		});
	});
}
if ($('#generateChars').length) {
	$('#generateChars').on('change', async function () {
		const value = String($(this).val());
		const min = parseInt($(this).prop('min'));
		const max = parseInt($(this).prop('max'));
		if (isNaN(parseInt(value))) {
			$(this).val(min);
		}
		else if (parseInt(value) < min) {
			$(this).val(min);
		}
		else if (parseInt(value) > max) {
			$(this).val(max);
		}
	});
}
$('document').ready(async function () {
	try {
		if ($('#pregenNameRadio').prop('checked') == true) {
			$('#firstNameText').prop('disabled', true);
			$('#lastNameText').prop('disabled', true);
		}
		await doubleJSON('fnames', firstNames);
		await doubleJSON('lnames', lastNames);
		await $.getJSON('locations.json', function (countries) {
			$.each(countries, async function (country) {
				await charCountries.push(country);
				$.each(countries[country], async function (city) {
					const name = `${String(country)} - ${countries[country][city]}`;
					await charLocations.push(name);
					const newCity = {
						country: country,
						city: countries[country][city],
					};
					await charCities.push(newCity);
				});
			});
		});
		await $.getJSON('raceandspecies.json', function (races) {
			$.each(races, async function (race) {
				await charRaces.push(race);
				$.each(races[race], async function (species) {
					const char = {
						race: race,
						species: races[race][species],
					};
					await charSpecies.push(char);
				});
			});
		});
		await singleJSON('genders', charGenders);
		await singleJSON('sexes', charSexes);
		await singleJSON('sexualities', charSexualities);
		for (const race of charRaces) {
			$('#customRacePickerDrop').append(`<a class="dropdown-item" href="#">${race}</a>`);
		}
		for (const spec of charSpecies) {
			$('#customSpeciesPickerDrop').append(`<a class="dropdown-item" href="#">${spec.species}</a>`);
		}
		for (const sex of charSexes) {
			$('#customSexPickerDrop').append(`<a class="dropdown-item" href="#">${sex}</a>`);
		}
		for (const sexuality of charSexualities) {
			$('#customSexualityPickerDrop').append(`<a class="dropdown-item" href="#">${sexuality}</a>`);
		}
		for (const gender of charGenders) {
			$('#customGenderPickerDrop').append(`<a class="dropdown-item" href="#">${gender}</a>`);
		}
		for (const country of charCountries) {
			$('#customCountryPickerDrop').append(`<a class="dropdown-item" href="#">${country}</a>`);
		}
		for (const city of charCities) {
			$('#customCityPicker').append(`<option class="dropdown-item" name="ass" value="${city.city}">${city.city}</option>`);
		}
		await console.log('website ready');
	}
	catch (err) {
		alert(`DOCUMENT READY ERROR: ${err}`);
		return console.error(err);
	}
});
$('#customCountryInput').on('change', async function () {
	const text = $(this).val();
	await $('#customCityPickerDrop').empty();
	for (const city of charCities) {
		if (city.country == text) {
			await $('#customCityPicker').append(`<option class="dropdown-item" value="${city.city}">${city.city}</option>`);
		}
	}
});
$('#customCityPicker').on('change', function () {
	if ($('#customCityInput').prop('disabled') == false) {
		$('#customCityInput').val($(this).val());
	}
});
$('#customRaceInput').on('change', async function () {
	const text = $(this).val();
	await $('#customSpeciesPickerDrop').empty();
	for (const spec of charSpecies) {
		if (spec.race == text) {
			await $('#customSpeciesPickerDrop').append(`<a class="dropdown-item" href="#">${spec.species}</a>`);
		}
	}
});
$('input[name="nameRadioButtons"]').click(async function () {
	try {
		if ($(this).is(':checked')) {
			if (this.id == 'pregenNameRadio') {
				$('#firstNameText').prop('disabled', true);
				$('#lastNameText').prop('disabled', true);
			}
			if (this.id == 'firstNameRadio') {
				$('#firstNameText').prop('disabled', false);
				$('#lastNameText').prop('disabled', true);
			}
			if (this.id == 'lastNameRadio') {
				$('#firstNameText').prop('disabled', true);
				$('#lastNameText').prop('disabled', false);
			}
			if (this.id == 'bothNameRadio') {
				$('#firstNameText').prop('disabled', false);
				$('#lastNameText').prop('disabled', false);
			}
		}
	}
	catch (err) {
		alert(`NAME=NAMERADIOBUTTONS ERROR: ${err}`);
		return console.error(err);
	}
});
$('input[name="customCharThings"]').click(async function () {
	try {
		if ($(this).is(':checked')) {
			$(`#${this.id}Input`).prop('disabled', false);
		}
		else {
			$(`#${this.id}Input`).prop('disabled', true);
		}
	}
	catch (err) {
		alert(`NAME=CUSTOMCHARTHING ERROR: ${err}`);
		return console.error(err);
	}
});
$('#buttonGenerate').click(async function () {
	try {
		await $('#characterRender').empty();
		let charsToGenerate = 1;
		if ($('#generateChars').length) {
			charsToGenerate = parseInt(String($('#generateChars').val()));
		}
		if ($('#buttonGenerate').hasClass('btn-danger')) {
			$('#buttonGenerate')
				.removeClass('btn-danger')
				.text('GENERATE')
				.addClass('btn-outline-info');
		}
		for (let x = 0; x < charsToGenerate; x++) {
			let firstName, lastName, name;
			await $('#characterRender').append(
				// `<table class="table table-bordered table-info" id="row-${x}">
				// 	<tr>
				// 		<td>
				// 			<label>Character Name</label><div id="charName-${x}"></div>
				// 		</td>
				// 		<td>
				// 			<label>Character Location</label><div id="charLocation-${x}"></div>
				// 		</td>
				// 	</tr>
				// 	<tr>
				// 		<td>
				// 			<label>Character Gender</label><div id="charGender-${x}"></div>
				// 		</td>
				// 		<td>
				// 			<label>Character Race</label><div id="charRace-${x}"></div>
				// 		</td>
				// 	</tr>
				// 	<tr>
				// 		<td>
				// 			<label>Character Sexuality</label><div id="charSexuality-${x}"></div>
				// 		</td>
				// 		<td>
				// 			<label>Character Sex</label><div id="charSex-${x}"></div>
				// 		</td>
				// 	</tr>
				// </table>`);
				`<div class="row" id="row-${x}">
				<div class="col">
					<div class="row">
						<div class="col">
							<label>Character Name</label><div id="charName-${x}"></div>
						</div>
						<div class="col">
							<label>Character Location</label><div id="charLocation-${x}"></div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<label>Character Gender</label><div id="charGender-${x}"></div>
						</div>
						<div class="col">
							<label>Character Race</label><div id="charRace-${x}"></div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<label>Character Sexuality</label><div id="charSexuality-${x}"></div>
						</div>
						<div class="col">
							<label>Character Sex</label><div id="charSex-${x}"></div>
						</div>
					</div>
				</div>
			</div>`);
			if ($('#pregenNameRadio').prop('checked') == true) {
				name = `${randomArray(firstNames)} ${randomArray(lastNames)}`;
			}
			if ($('#bothNameRadio').prop('checked') == true) {
				if ($('#firstNameText').val() != '' && $('#lastNameText').val() != '') {
					firstName = $('#firstNameText').val();
					lastName = $('#lastNameText').val();
					name = `${firstName} ${lastName}`;
				}
				else {
					$('#buttonGenerate')
						.removeClass('btn-outline-info')
						.addClass('btn-danger')
						.text('Name fields are empty');
					return;
				}
			}
			if ($('#firstNameRadio').prop('checked') == true) {
				if ($('#firstNameText').val() != '') {
					firstName = $('#firstNameText').val();
					name = `${firstName} ${randomArray(lastNames)}`;
				}
				else {
					$('#buttonGenerate')
						.removeClass('btn-outline-info')
						.addClass('btn-danger')
						.text('Name fields are empty');
					return;
				}
			}
			if ($('#lastNameRadio').prop('checked') == true) {
				if ($('#lastNameText').val() != '') {
					lastName = $('#lastNameText').val();
					name = `${randomArray(firstNames)} ${lastName}`;
				}
				else {
					$('#buttonGenerate')
						.removeClass('btn-outline-info')
						.addClass('btn-danger')
						.text('Name fields are empty');
					return;
				}
			}
			if ($('#customGenderInput').val() == '' || $('#customGender').prop('checked') == false) {
				await $(`#charGender-${x}`).text(randomArray(charGenders));
			}
			else {
				await $(`#charGender-${x}`).text(capitalize(String($('#customGenderInput').val())));
			}
			if ($('#customRaceInput').val() == '' || $('#customRace').prop('checked') == false) {
				await $(`#charRace-${x}`).text(randomArray(charRaces));
			}
			else {
				await $(`#charRace-${x}`).text(capitalize(String($('#customRaceInput').val())));
			}
			if ($('#customSexInput').val() == '' || $('#customSex').prop('checked') == false) {
				await $(`#charSex-${x}`).text(randomArray(charSexes));
			}
			else {
				await $(`#charSex-${x}`).text(capitalize(String($('#customSexInput').val())));
			}
			if ($('#customSexualityInput').val() == '' || $('#customSexuality').prop('checked') == false) {
				await $(`#charSexuality-${x}`).text(randomArray(charSexualities));
			}
			else {
				await $(`#charSexuality-${x}`).text(capitalize(String($('#customSexualityInput').val())));
			}
			const location = await randomArray(charLocations);
			$(`#charLocation-${x}`).text(location);
			$(`#charName-${x}`).text(name);
			if (!charNames.includes(name)) {
				charNames.push(name);
			}
			const newChar = await {
				name: name,
				location: location,
			};
			chars.push(newChar);
		}
		$('#generatedUniqueNames').text(charNames.length);
		return;
	}
	catch (err) {
		alert(`BUTTON GENERATE ERROR: ${err}`);
		return console.error(err);
	}
});
$('#listNames').on('click', async function () {
	$('#nameList').text('');
	for (const name of charNames) {
		$('#nameList').append(`${name}<br/>`);
	}
});
$('#buttonCount').click(function () {
	let nameCombos = 0,
		charCombos = 0,
		locationCombos = 0,
		sexualityCombos = 0,
		raceCombos = 0;
	for (const country of charCountries) {
		for (const city of charCities) {
			locationCombos++;
		}
	}
	for (const race of charRaces) {
		for (const spec of charSpecies) {
			raceCombos++;
		}
	}
	for (const gender of charGenders) {
		for (const sex of charSexes) {
			for (const sexuality of charSexualities) {
				sexualityCombos++;
			}
		}
	}
	for (const fName of firstNames) {
		for (const lName of lastNames) {
			nameCombos++;
		}
	}
	charCombos = nameCombos * raceCombos * sexualityCombos * locationCombos;
	if ($('#possibleNames').length > 0) {
		$('#possibleNames').text(nameCombos);
	}
	if ($('#possibleChars').length > 0) {
		$('#possibleChars').text(charCombos);
	}
	if ($('#possibleRaces').length > 0) {
		$('#possibleRaces').text(raceCombos);
	}
	if ($('#possibleLocations').length > 0) {
		$('#possibleLocations').text(locationCombos);
	}
	if ($('#possibleSexualities').length > 0) {
		$('#possibleSexualities').text(sexualityCombos);
	}
});