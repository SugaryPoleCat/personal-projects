'use strict';

// THIS can be used to generate names for hte random name generator.
// then we just give THAT app, adress to this JSON, that way everytime users open the webpage, the names dont have to be generated over nad over.
// Either this file or create one for it.


const fs = require('fs');

async function run() {
    try {
        if (process.argv.includes('-g')) {
            // This processes all names.
            const names = await fs.readFileSync(__dirname + '/names.json');
            const namesJSON = JSON.parse(names);
            // console.info('[', new Date().toUTCString(), ']\nnamesJSON\n', namesJSON);

            // this proceses last name exlcusive names
            const lastNames = await fs.readFileSync(__dirname + '/lastnames.json');
            const lastNamesJSON = JSON.parse(lastNames);
            // console.info('[', new Date().toUTCString(), ']\nlastNamesJSON\n', lastNamesJSON);

            // What we have to do, is make names. Name = name + name BUT ALSO name = name + last name;
            // We must also check if there are duplicates.


            const namesArray = [],
                lastNamesArray = [],
                completeNamesArray = [];
            // you cant use OF in json to iterate, BUT you can use IN to iterate JSON
            for (const name in namesJSON) {
                // this will just give category names.
                // console.info('[', new Date().toUTCString(), ']\nname\n', name);

                // this outputs the entire categorty
                // console.log('[', new Date().toUTCString(), `]\n${name}`, namesJSON[name]);

                for (const firstName in namesJSON[name]) {
                    // This just gives index
                    // console.info('[', new Date().toUTCString(), ']\nfirstName\n', firstName);

                    // This outputs the name.
                    // console.log('[', new Date().toUTCString(), `]\n${firstName}`, namesJSON[name][firstName]);

                    namesArray.push(namesJSON[name][firstName]);
                }
            }
            for (const name in lastNamesJSON) {
                for (const lastName in lastNamesJSON[name]) {
                    lastNamesArray.push(lastNamesJSON[name][lastName]);
                }
            }

            for (const name of namesArray) {
                // console.info('[', new Date().toUTCString(), ']\nname\n', name);
                for (const lname of namesArray) {
                    if (name != lname) {
                        const generatedName = name + ' ' + lname;
                        completeNamesArray.push(generatedName);
                    }
                }
                for (const lname of lastNamesArray) {
                    let generatedName;
                    // do {
                    //     generatedName = name + ' ' + lname;
                    // } while (!completeNamesArray.includes(generatedName));
                    generatedName = name + ' ' + lname;
                    // console.log('[', new Date().toUTCString(), ']\nwe are pushing a name');
                    completeNamesArray.push(generatedName);
                }
            }

            // console.info('[', new Date().toUTCString(), ']\nnamesArray\n', namesArray);
            // console.info('[', new Date().toUTCString(), ']\nlastNamesArray\n', lastNamesArray);

            // console.info('[', new Date().toUTCString(), ']\ncompleteNamesArray\n', completeNamesArray);
            // console.info('[', new Date().toUTCString(), ']\ncompleteNamesArray.length\n', completeNamesArray.length);

            await fs.writeFile(__dirname + '/completeNamesArray.json', JSON.stringify(completeNamesArray, '', 4), async (err) => {
                if (err) {
                    throw new Error(err);
                }
                console.log('[', new Date().toUTCString(), ']\nFile written.');
            });


            // get a random name
        }
        // console.log('[', new Date().toUTCString(), ']\n', completeNamesArray[Math.floor(Math.random() * completeNamesArray.length)]);
        const namesFile = await fs.readFileSync(__dirname + '/completeNamesArray.json');
        const namesFileArray = JSON.parse(namesFile);
        console.log('[', new Date().toUTCString(), ']\n', namesFileArray[Math.floor(Math.random() * namesFileArray.length)]);
    }
    catch (err) {
        console.error('[', new Date().toUTCString(), ']\nSomething went wrong \n', err);
        return;
    }
}

run();